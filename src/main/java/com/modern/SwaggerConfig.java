package com.modern;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Collections;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("UserApi")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.modern"))
                //.paths(PathSelectors.any())
                .paths(PathSelectors.ant("/api/**"))
                .build()
                .apiInfo(apiInfo())
                .useDefaultResponseMessages(false)
                .globalResponseMessage(RequestMethod.GET, getArrayList());
    }

    @Bean
    public Docket mapi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("adminApi")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.modern"))
                .paths(PathSelectors.ant("/mapi/**"))
                .build()
                .apiInfo(apiInfo())
                .useDefaultResponseMessages(false)
                .globalResponseMessage(RequestMethod.GET, getArrayList());
    }

    private ArrayList<ResponseMessage> getArrayList(){
        ArrayList<ResponseMessage> lists = new ArrayList<ResponseMessage>();

        lists.add(new ResponseMessageBuilder().code(403).message("권한없음").responseModel(new ModelRef("Forbbiden")).build());
        lists.add(new ResponseMessageBuilder().code(404).message("API를 찾을수 없음").responseModel(new ModelRef("Not Found")).build());
        lists.add(new ResponseMessageBuilder().code(500).message("서버에러").responseModel(new ModelRef("Error")).build());

        return lists;
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "modern",
                "modern API",
                "1.0",
                "",
                new Contact("투도스", "www.todos.co.kr", "hello@todos.co.kr"),
                "License of API", "API license URL", Collections.emptyList());
    }
}
