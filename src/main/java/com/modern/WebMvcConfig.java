package com.modern;

import com.modern.common.session.AuthInterceptor;
import com.modern.common.util.RequestBodyXSSFileter;
import com.navercorp.lucy.security.xss.servletfilter.XssEscapeServletFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Autowired
    private AuthInterceptor authInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authInterceptor)
                .addPathPatterns("/page/**")
                .excludePathPatterns("/page/membership/**")
                .excludePathPatterns("/page/tds/**")
                .excludePathPatterns("/page/about/**")
                .excludePathPatterns("/page/culture/**")
                .excludePathPatterns("/page/portfolio/**")
                .excludePathPatterns("/page/contact/**")
                .excludePathPatterns("/page/admin/login");
    }

    @Bean
    public FilterRegistrationBean getRequestBodyXSSFileterRegistrationBean() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new RequestBodyXSSFileter());
        registrationBean.addUrlPatterns("/*");
        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean getFilterRegistrationBean(){
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new XssEscapeServletFilter());

        registrationBean.setOrder(1);
        registrationBean.addUrlPatterns("/*");    //filter를 거칠 url patterns
        return registrationBean;
    }

}
