package com.modern.admin.board.controller;

import com.modern.admin.board.dto.req.*;
import com.modern.admin.board.service.AdminBoardService;
import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.session.SessionCheck;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/mapi/board")
public class AdminBoardController {

    @Autowired
    private AdminBoardService adminBoardService;

    @ApiOperation(value = "게시판 목록")
    @GetMapping
    public ResponseHandler<?> selectBoard(SelectBoardReq req) {
        ResponseHandler<?> result = adminBoardService.selectBoard(req);
        return  result;
    }

    @ApiOperation(value = "게시판 상세")
    @GetMapping(value="/detail")
    public ResponseHandler<?> selectBoardDetail(SelectBoardDetailReq req) {
        ResponseHandler<?> result = adminBoardService.selectBoardDetail(req);
        return  result;
    }

    @ApiOperation(value = "게시판 쓰기")
    @PostMapping
    @SessionCheck
    public ResponseHandler<?> insertBoard(InsertBoardReq req) {
        ResponseHandler<?> result = adminBoardService.insertBoard(req);
        return  result;
    }

    @ApiOperation(value = "게시판 수정")
    @PostMapping(value="/update")
    @SessionCheck
    public ResponseHandler<?> updateBoard(UpdateBoardReq req) {
        ResponseHandler<?> result = adminBoardService.updateBoard(req);
        return  result;
    }

    @ApiOperation(value = "게시판 삭제")
    @DeleteMapping
    public ResponseHandler<?> deleteBoard(@RequestBody(required = false) DeleteBoardReq req) {
        ResponseHandler<?> result = adminBoardService.deleteBoard(req);
        return  result;
    }
}
