package com.modern.admin.board.dao;

import com.modern.admin.board.dto.req.*;
import com.modern.admin.board.dto.res.BoardDetailRes;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface AdminBoardDao {

    void insertBoard(InsertBoardReq req);

    List selectBoard(SelectBoardReq req);

    int selectBoardCnt(SelectBoardReq req);

    void updateBoard(UpdateBoardReq req);

    void deleteBoard(DeleteBoardReq req);

    BoardDetailRes selectBoardDetail(SelectBoardDetailReq req);
}
