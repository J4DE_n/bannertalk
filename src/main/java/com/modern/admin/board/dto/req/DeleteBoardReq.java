package com.modern.admin.board.dto.req;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value ="게시판 삭제")
public class DeleteBoardReq {
    private int boardSeq;
    private List boardSeqList;
}
