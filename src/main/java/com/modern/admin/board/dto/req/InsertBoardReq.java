package com.modern.admin.board.dto.req;

import com.modern.common.dto.req.CommonReq;
import lombok.Data;

import java.io.Serializable;

@Data
public class InsertBoardReq extends CommonReq implements Serializable{
    private String boardType;
    private String title;
    private String summary;
    private String contents;
    private Integer thumbnailFileGrpSeq;
    private Integer attachFileGrpSeq;
    private String linkUrl;
    private String linkPath;
    private String showYn;
    private Integer creId;
    private Integer updId;
    private String startDt;
}
