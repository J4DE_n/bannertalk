package com.modern.admin.board.dto.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@ApiModel(value ="게시판 상세")
public class SelectBoardDetailReq {
    @ApiParam(value = "게시판 시퀀스", required = true)
    @NotNull
    private int boardSeq;
}
