package com.modern.admin.board.dto.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SelectBoardReq {
    private String boardType;
    private String title;
    private String summary;
    private String contents;
    private Integer thumbnailFileGrpSeq;
    private Integer attachFileGrpNo;
    private String linkPath;
    private String showYn;
    private Integer creId;
    private Integer updId;

    /* *
    *
    *  검색 조건
    *
    *  searchCon = T --> 제목만
    *  searchCon = C --> 내용만
    *  searchCon = S --> 기관명만
    *  searchCon = TC --> 제목 + 내용
    *  searchCon = A --> 제목 + 기관명 + 내용
    *
    * */
    private String searchCon; // 검색 조건
    private String searchText; // 검색어

    private String startDate;
    private String endDate;

    @ApiParam(value = "페이지 조회 개수", required = true)
    private int currentPage;

    @ApiParam(value = "노출 게시물 개수", required = true)
    @NotNull
    private int cntPerPage;

    @JsonIgnore
    @ApiParam(value = "시작 번호")
    private int startRow;

    public void setStartRow() {
        this.startRow = (this.currentPage - 1) * this.cntPerPage;
    }
}
