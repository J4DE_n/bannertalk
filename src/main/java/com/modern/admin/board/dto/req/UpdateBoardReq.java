package com.modern.admin.board.dto.req;

import com.modern.common.dto.req.CommonReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;

@Data
@ApiModel(value ="게시 수정")
public class UpdateBoardReq extends CommonReq implements Serializable {

    @ApiParam(value = "공지사항 시퀀스", required = true)
    @NotNull
    private Integer boardSeq;

    @ApiParam(value = "보드타입")
    private String boardType;

    @ApiParam(value = "제목")
    private String title;

    @ApiParam(value = "부제목")
    private String summary;

    @ApiParam(value = "내용")
    private String contents;

    @ApiParam(value = "썸네일 파일 그룹")
    private Integer thumbnailFileGrpSeq;

    @ApiParam(value = "썸네일 파일 그룹")
    private Integer attachFileGrpSeq;

    @ApiParam(value = "링크")
    private String linkPath;

    @ApiParam(value = "연결 링크")
    private String linkUrl;

    @ApiParam(value = "삭제여부")
    private String showYn;

    @ApiParam(value = "수정자")
    private int updId;

    @ApiParam(value = "에디터 삭제 이미지", hidden = true)
    private ArrayList<String> editorDelImg;

    @ApiParam(value = "등록일")
    private String startDt;

}
