package com.modern.admin.board.dto.res;

import lombok.Data;

@Data
public class BoardDetailRes {
    private String title;
    private String contents;
    private String summary;
    private String linkUrl;
    private String boardType;
    private String startDt;
}
