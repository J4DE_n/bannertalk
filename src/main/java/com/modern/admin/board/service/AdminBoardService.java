package com.modern.admin.board.service;

import com.modern.admin.board.dao.AdminBoardDao;
import com.modern.admin.board.dto.req.*;
import com.modern.admin.board.dto.res.BoardDetailRes;
import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.file.config.ConfigFile;
import com.modern.common.file.dto.model.FileDeleteInfo;
import com.modern.common.file.dto.req.FileDeleteReq;
import com.modern.common.file.service.FileService;
import com.modern.common.type.ReturnType;
import com.modern.common.util.CommonUtil;
import com.modern.common.util.CookieUtil;
import com.modern.membership.dto.res.SessionRes;
import com.modern.membership.service.JwtService;
import com.modern.membership.service.MembershipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AdminBoardService {

    @Autowired
    private AdminBoardDao adminBoardDao;
    @Autowired
    private MembershipService membershipService;
    @Autowired
    JwtService jwtService;
    @Autowired
    private ConfigFile configFile;

    @Autowired
    private FileService fileService;

    public ResponseHandler<?> insertBoard(InsertBoardReq req) {
        ResponseHandler<?> result = new ResponseHandler<>();

        try{
            req.setCreId(req.getUserSeq());
            adminBoardDao.insertBoard(req);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }

        return result;
    }

    public ResponseHandler<?> selectBoard(SelectBoardReq req) {
        ResponseHandler<Map> result = new ResponseHandler<>();

        try{
            req.setStartRow();

            List list = adminBoardDao.selectBoard(req);
            int cnt = adminBoardDao.selectBoardCnt(req);

            Map map = new HashMap();
            map.put("list",list);
            map.put("cnt",cnt);

            result.setData(map);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }

        return result;
    }

    public ResponseHandler<?> updateBoard(UpdateBoardReq req) {
        ResponseHandler<?> result = new ResponseHandler<>();

        try{
            req.setUpdId(req.getUserSeq());
            adminBoardDao.updateBoard(req);


            if(CommonUtil.isNotEmpty(req.getEditorDelImg())){

                List<FileDeleteInfo> fileDelInfoList = new ArrayList<>();

                ArrayList<String> imgs = req.getEditorDelImg();
                for(String nm : imgs){
                    FileDeleteInfo fileDeleteInfo = new FileDeleteInfo();
                    fileDeleteInfo.setFileName(nm);
                    fileDelInfoList.add(fileDeleteInfo);
                }

                FileDeleteReq fileDeleteReq = new FileDeleteReq();
                fileDeleteReq.setCategory(configFile.getSelectCategory9());
                fileDeleteReq.setIsSaveInfos(false);
                fileDeleteReq.setFileDeleteInfos(fileDelInfoList);

                fileService.deleteFiles(fileDeleteReq);
            }


            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }

        return result;
    }

    public ResponseHandler<?> deleteBoard(DeleteBoardReq req) {
        ResponseHandler<?> result = new ResponseHandler<>();
        try{
            for(int index=0; index < req.getBoardSeqList().size(); index++){

                req.setBoardSeq((Integer.parseInt((String)req.getBoardSeqList().get(index))));
                adminBoardDao.deleteBoard(req);
            }
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }

        return result;
    }

    public ResponseHandler<?> selectBoardDetail(SelectBoardDetailReq req) {
        ResponseHandler<BoardDetailRes> result = new ResponseHandler<>();

        try{
            /* 게시판 상세 */
            BoardDetailRes res = adminBoardDao.selectBoardDetail(req);

            result.setData(res);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }

        return result;
    }
}
