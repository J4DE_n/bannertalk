package com.modern.admin.category.dao;
import com.modern.admin.category.dto.model.ACategoryVO;
import com.modern.admin.category.dto.req.ACategoryListReq;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ACategoryDao
{
    List<ACategoryVO> selectCategoryList(ACategoryListReq req);
}
