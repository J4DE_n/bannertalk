package com.modern.admin.category.dto.model;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="카테고리 VO")
public class ACategoryVO implements Serializable {
    private Integer categorySeq;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String categoryType;
    private String pCategorySeq;
    private String categoryNm;
    private String sortNo;
}