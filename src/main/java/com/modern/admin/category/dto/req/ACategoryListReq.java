package com.modern.admin.category.dto.req;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="카테고리 조회")
public class ACategoryListReq implements Serializable {
    @ApiParam(value = "부모카테고리 시퀀스")
    private String pCategorySeq;

    @ApiParam(value = "카테고리 타입 - 공통코드", hidden = true)
    private String categoryType;

    @ApiParam(value = "하위 카테고리 모두 조회 여부")
    private Boolean isPCategoryAll = false;
}