package com.modern.admin.category.dto.res;

import com.modern.admin.category.dto.model.ACategoryVO;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "카테고리 조회 목록")
public class ACategoryListRes {
    private List<ACategoryVO> list;
}
