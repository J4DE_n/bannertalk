package com.modern.admin.category.service;

import com.modern.admin.category.dao.ACategoryDao;
import com.modern.admin.category.dto.model.ACategoryVO;
import com.modern.admin.category.dto.req.ACategoryListReq;
import com.modern.admin.category.dto.res.ACategoryListRes;
import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.type.ReturnType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * QNA를 위한 클래스
 */
@Service
public class ACategoryService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ACategoryDao aCategoryDao;

    /* 카테고리 목록 조회 */
    public ResponseHandler<ACategoryListRes> selectCategory(ACategoryListReq req) {
        final ResponseHandler<ACategoryListRes> result = new ResponseHandler<>();
        try {
            List<ACategoryVO> list = aCategoryDao.selectCategoryList(req);
            ACategoryListRes res = new ACategoryListRes();
            res.setList(list);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
            result.setData(res);
        } catch (Exception e){
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }
        return result;
    }
}

