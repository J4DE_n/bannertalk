package com.modern.admin.change.controller;

import com.modern.admin.change.dto.req.*;
import com.modern.admin.change.dto.res.*;

import com.modern.admin.change.service.AChangeService;
import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.file.config.ConfigFile;
import com.modern.common.file.service.FileService;
import com.modern.common.session.SessionCheck;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;


/**
 *  멤버쉽 Rest Controller class
 *
 *  관리자를 제어하기 위한 REST API가 정의 되어 있다.
 *
 *
 */
@RestController
@RequestMapping(value="/mapi/change" ) //모듈명 정의
@Api(value = "AChangeController", description = "고객 변화 API")
public class AChangeController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	AChangeService aChangeService;

	@Autowired
    FileService fileService;

	@Autowired
    ConfigFile configFile;

	/**
	 * 고객 변화 목록
	 */
	@ApiOperation(value = "고객 변화 목록")
	@GetMapping
	public ResponseHandler<ChangeListRes> list(@Valid ChangeListReq req) {
		ResponseHandler<ChangeListRes> result = aChangeService.selectChange(req);
		return  result;
	}
	
	/**
	 *  고객 변화 등록
	 */
	@ApiOperation(value = "고객 변화 등록")
	@PostMapping
	@SessionCheck
	public ResponseHandler<?> save(ChangeInsertReq req, @RequestParam("attach") MultipartFile[] files) {
		ResponseHandler<?> result = aChangeService.insertChange(req, files);
		return result;
	}

	/**
	 * 고객 변화 상세
	 */
	@ApiOperation(value = "고객 변화 상세")
	@GetMapping(value="/item")
	public ResponseHandler<ChangeDetailRes> info(ChangeDetailReq req) {
		ResponseHandler<ChangeDetailRes> result = aChangeService.selectChangeDetail(req);
		return result;
	}

	/**
	 * 고객 변화 수정
	 */
	@ApiOperation(value = "공지사항 수정")
	@PostMapping(value="/edit")
	@SessionCheck
	public ResponseHandler<?> update(ChangeUpdateReq req, @RequestParam("attach") MultipartFile[] files) {
		ResponseHandler<?> result = aChangeService.updateChange(req, files);
		return  result;
	}

	/**
	 * 고객 변화 삭제
	 */
	@ApiOperation(value = "고객 변화 삭제")
	@DeleteMapping
	@SessionCheck
	public ResponseHandler<?> delete(@Valid @RequestBody(required = false) ChangeDeleteReq req) {
		ResponseHandler<?> result = aChangeService.deleteChange(req);
		return  result;
	}
}
