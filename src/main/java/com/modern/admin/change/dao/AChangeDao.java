package com.modern.admin.change.dao;
import com.modern.admin.change.dto.model.ChangeListVO;
import com.modern.admin.change.dto.req.*;
import com.modern.admin.change.dto.res.ChangeDetailRes;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface AChangeDao
{
    void insertChange(ChangeInsertReq req);

    void updateChange(Map<String, Object> params);

    ChangeDetailRes selectChangeDetail(Map<String, Object> params);

    List<ChangeListVO> selectChange(Map<String, Object> params);

    int selectChangeCnt(Map<String, Object> params);

    void deleteChange(ChangeDeleteReq req);
}
