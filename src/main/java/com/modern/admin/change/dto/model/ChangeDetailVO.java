package com.modern.admin.change.dto.model;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="고객 변화 상세 VO")
public class ChangeDetailVO {
    private int chgSeq;
    private String writeDt;
    private String displayYn;
    private String title;
    private String summary;
    private String tagNm;
    private Integer befFileGrpSeq;
    private Integer aftFileGrpSeq;
    private String contents;
    private String delYn;
    private int creId;
    private String creDt;
    private int updId;
    private String updDt;
}