package com.modern.admin.change.dto.model;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="고객 변화 목록 VO")
public class ChangeListVO implements Serializable {
    private int rowNum;
    private int chgSeq;
    private String userNm;
    private String userId;
    private String title;
    private String contents;
    private String displayYn;
    private String writeDt;
    private String delYn;
    private int creId;
    private String creDt;
}


