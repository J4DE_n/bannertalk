package com.modern.admin.change.dto.req;
import com.modern.common.dto.req.CommonReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(value ="고객 변화 삭제")
public class ChangeDeleteReq extends CommonReq implements Serializable {
    @ApiParam(value = "고객 변화 시퀀스")
    private Integer chgSeq;

    @ApiParam(value = "고객 변화 시퀀스 리스트")
    private List chgSeqList;
}