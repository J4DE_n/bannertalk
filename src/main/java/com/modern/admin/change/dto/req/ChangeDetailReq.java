package com.modern.admin.change.dto.req;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel(value ="고객 변화 상세")
public class ChangeDetailReq implements Serializable {
    @ApiParam(value = "고객 변화 시퀀스", required = true)
    @NotNull
    private Integer chgSeq;
}