package com.modern.admin.change.dto.req;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.modern.common.dto.req.CommonReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;

@Data
@ApiModel(value ="고객 변화 등록")
public class ChangeInsertReq extends CommonReq implements Serializable {

    @ApiParam(value = "등록일")
    private String writeDt;

    @ApiParam(value = "게시 여부")
    private String displayYn;

    @ApiParam(value = "고객 변화 제목", required = true)
    @NotNull
    private String title;

    @ApiParam(value = "요약 설명")
    private String summary;

    @ApiParam(value = "태그")
    private String tagNm;

    @ApiParam(value = "상세 내용")
    private String contents;

    @ApiParam(value = "에디터 삭제 이미지", hidden = true)
    private ArrayList<String> editorDelImg;

    @ApiParam(value = "시스템 작성일")
    private String creDt;

    @ApiParam(value = "비포 썸네일 시퀀스", hidden = true)
    @JsonIgnore
    private Integer befFileGrpSeq;

    @ApiParam(value = "애프터 썸네일 시퀀스", hidden = true)
    @JsonIgnore
    private Integer aftFileGrpSeq;

    @ApiParam(value = "비포,애프터 썸네일")
    private MultipartFile attach;
}