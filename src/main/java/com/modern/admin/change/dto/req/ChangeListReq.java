package com.modern.admin.change.dto.req;
import com.modern.common.dto.req.PagingReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import java.io.Serializable;

@Data
@ApiModel(value ="고객 변화 목록")
public class ChangeListReq extends PagingReq implements Serializable {

    @ApiParam(value = "검색어")
    private String searchText;

    @ApiParam(value = "시작일")
    private String startDate;

    @ApiParam(value = "종료일")
    private String endDate;

    @ApiParam(value = "게시 여부")
    private String displayYn;
}