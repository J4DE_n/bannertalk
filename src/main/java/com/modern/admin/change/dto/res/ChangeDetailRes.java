package com.modern.admin.change.dto.res;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;

@Data
@ApiModel(value ="고객 변화 상세")
public class ChangeDetailRes implements Serializable {
    @ApiParam(value = "고객 변화 시퀀스", required = true)
    @NotNull
    private Integer chgSeq;

    @ApiParam(value = "등록일")
    private String writeDt;

    @ApiParam(value = "게시 여부")
    private String displayYn;

    @ApiParam(value = "고객 변화 제목", required = true)
    @NotNull
    private String title;

    @ApiParam(value = "요약 설명")
    private String summary;

    @ApiParam(value = "태그")
    private String tagNm;

    @ApiParam(value = "상세 내용")
    private String contents;

    @ApiParam(value = "에디터 삭제 이미지", hidden = true)
    private ArrayList<String> editorDelImg;

    @ApiParam(value = "시스템 수정일")
    private String updDt;

    @ApiParam(value = "비포 썸네일 파일 그룹 시퀀스")
    private int befFileGrpSeq;

    @ApiParam(value = "애프터 썸네일 파일 그룹 시퀀스")
    private int aftFileGrpSeq;

    @ApiParam(value = "오리지널 파일명")
    private Integer orgFileNm;

    @ApiParam(value = "시스템 파일명")
    private Integer sysFileNm;

    @ApiParam(value = "포스트 썸네일 시퀀스")
    private int fileSeq;

    @ApiParam(value = "포스트 썸네일 경로")
    private String filePath;

    @ApiParam(value = "삭제 여부")
    private String delYn;
}