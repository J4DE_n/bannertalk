package com.modern.admin.change.dto.res;

import com.modern.admin.change.dto.model.ChangeListVO;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "고객 변화 목록 정보")
public class ChangeListRes {
    private List<ChangeListVO> changeList;

    /*현재 페이지*/
    private int currentPage;
    private int totalCnt;
}
