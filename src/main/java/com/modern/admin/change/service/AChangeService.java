package com.modern.admin.change.service;

import com.modern.admin.change.dao.AChangeDao;
import com.modern.admin.change.dto.model.ChangeListVO;
import com.modern.admin.change.dto.req.*;
import com.modern.admin.change.dto.res.*;
import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.file.config.ConfigFile;
import com.modern.common.file.dto.model.FileDeleteInfo;
import com.modern.common.file.dto.req.FileDeleteReq;
import com.modern.common.file.dto.req.FileSaveReq;
import com.modern.common.file.dto.res.FileInfoRes;
import com.modern.common.file.service.FileService;
import com.modern.common.type.ReturnType;
import com.modern.common.util.CommonUtil;
import com.modern.common.util.ConvertUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Service
public class AChangeService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    AChangeDao aChangeDao;

    @Autowired
    private ConfigFile configFile;

    @Autowired
    private FileService fileService;

    /**
     * 고객 변화 목록
     */
    public ResponseHandler<ChangeListRes> selectChange(ChangeListReq req) {
        logger.info("selectChange[req]" + req);
        final ResponseHandler<ChangeListRes> result = new ResponseHandler<>();
        req.setStartRow();
        Map<String, Object> params = ConvertUtil.convertObjectToMap(req);

        try {
            List<ChangeListVO> list = aChangeDao.selectChange(params);
            int totalCnt = aChangeDao.selectChangeCnt(params);
            ChangeListRes changeListRes = new ChangeListRes();
            changeListRes.setChangeList(list);
            changeListRes.setTotalCnt(totalCnt);
            if(totalCnt == 0) {
                result.setReturnCode(ReturnType.RTN_TYPE_NO_DATA);
                logger.info("selectChange[result]" + result);
                return result;
            }
            result.setData(changeListRes);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
            logger.info("selectChange[result]" + result);
        } catch (Exception e){
            logger.error("selectChange[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }
        return result;
    }

    /**
     * 고객 변화 등록
     */
    public ResponseHandler<?> insertChange(ChangeInsertReq req, MultipartFile[] files) {
        final ResponseHandler<?> result = new ResponseHandler<>();
        try {

            if(!files[0].isEmpty()){
                FileSaveReq befFileSaveReq = new FileSaveReq();
                FileSaveReq aftFileSaveReq = new FileSaveReq();

                befFileSaveReq.setCategory(configFile.getSelectCategory4());
                aftFileSaveReq.setCategory(configFile.getSelectCategory4());

                befFileSaveReq.setIsSaveInfos(true);
                aftFileSaveReq.setIsSaveInfos(true);

                befFileSaveReq.setUserSeq(req.getUserSeq());
                aftFileSaveReq.setUserSeq(req.getUserSeq());

                FileInfoRes befFileInfoRes = fileService.storeFiles(files, befFileSaveReq);
                FileInfoRes aftFileInfoRes = fileService.storeFiles(files, aftFileSaveReq);

                req.setBefFileGrpSeq(befFileInfoRes.getFileGrpSeq());
                req.setAftFileGrpSeq(aftFileInfoRes.getFileGrpSeq());
            }
            aChangeDao.insertChange(req);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
            logger.info("insertChange[result]" + result);
        } catch (Exception e) {
            logger.error("insertChange[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 고객 변화 상세
     */
    public ResponseHandler<ChangeDetailRes> selectChangeDetail(ChangeDetailReq req) {
        logger.info("changeDetail[req]" + req);
        final ResponseHandler<ChangeDetailRes> result = new ResponseHandler<>();
        Map<String, Object> params = ConvertUtil.convertObjectToMap(req);
        try{
            ChangeDetailRes changeDetailRes = aChangeDao.selectChangeDetail(params);
            result.setData(changeDetailRes);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
            logger.info("changeDetail[result]" + result);
        } catch (Exception e){
            logger.error("changeDetail[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }
        return result;
    }

    /**
     * 고객 변화 수정
     */
    public ResponseHandler<?> updateChange(ChangeUpdateReq req, MultipartFile[] files) {
        final ResponseHandler<?> result = new ResponseHandler<>();
        try {
            if(!files[0].isEmpty()) {
                Map<String, Object> params = ConvertUtil.convertObjectToMap(req);
                ChangeDetailRes changeDetailRes = aChangeDao.selectChangeDetail(params);
                if(CommonUtil.isNotEmpty(changeDetailRes.getChgSeq())){
                    if(changeDetailRes.getFileSeq() > 0 ){
                        List<FileDeleteInfo> fileDelInfoList = new ArrayList<>();
                        FileDeleteInfo fileDeleteInfo = new FileDeleteInfo();
                        fileDeleteInfo.setFileSeq(changeDetailRes.getFileSeq());
                        fileDelInfoList.add(fileDeleteInfo);
                        FileDeleteReq fileDeleteReq = new FileDeleteReq();
                        fileDeleteReq.setCategory(configFile.getSelectCategory4());
                        fileDeleteReq.setFileGrpSeq(changeDetailRes.getBefFileGrpSeq());
                        fileDeleteReq.setFileGrpSeq(changeDetailRes.getAftFileGrpSeq());
                        fileDeleteReq.setFileDeleteInfos(fileDelInfoList);
                        fileDeleteReq.setUserSeq(req.getUserSeq());
                        fileService.deleteFiles(fileDeleteReq);
                    }
                }
                FileSaveReq befFileSaveReq = new FileSaveReq();
                FileSaveReq aftFileSaveReq = new FileSaveReq();

                befFileSaveReq.setCategory(configFile.getSelectCategory4());
                aftFileSaveReq.setCategory(configFile.getSelectCategory4());

                befFileSaveReq.setIsSaveInfos(true);
                aftFileSaveReq.setIsSaveInfos(true);

                befFileSaveReq.setUserSeq(req.getUserSeq());
                aftFileSaveReq.setUserSeq(req.getUserSeq());

                if(changeDetailRes.getBefFileGrpSeq() > 0){
                    befFileSaveReq.setFileGrpSeq(changeDetailRes.getBefFileGrpSeq());
                }

                if(changeDetailRes.getAftFileGrpSeq() > 0){
                    aftFileSaveReq.setFileGrpSeq(changeDetailRes.getAftFileGrpSeq());
                }

                FileInfoRes befFileInfoRes = fileService.storeFiles(files, befFileSaveReq);
                FileInfoRes aftFileInfoRes = fileService.storeFiles(files, aftFileSaveReq);

                req.setBefFileGrpSeq(befFileInfoRes.getFileGrpSeq());
                req.setAftFileGrpSeq(aftFileInfoRes.getFileGrpSeq());
            }
            Map<String, Object> params = ConvertUtil.convertObjectToMap(req);
            params.put("userSeq", req.getUserSeq());
            aChangeDao.updateChange(params);
            aChangeDao.updateChange(params);

            //3. 에디터 삭제된 이미지 물리 삭제
            if(CommonUtil.isNotEmpty(req.getEditorDelImg())){
                List<FileDeleteInfo> fileDelInfoList = new ArrayList<>();

                ArrayList<String> imgs = req.getEditorDelImg();
                for(String nm : imgs){
                    FileDeleteInfo fileDeleteInfo = new FileDeleteInfo();
                    fileDeleteInfo.setFileName(nm);
                    fileDelInfoList.add(fileDeleteInfo);
                }

                FileDeleteReq fileDeleteReq = new FileDeleteReq();
                fileDeleteReq.setCategory(configFile.getSelectCategory9());
                fileDeleteReq.setIsSaveInfos(false);
                fileDeleteReq.setFileDeleteInfos(fileDelInfoList);

                fileService.deleteFiles(fileDeleteReq);
            }
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 고객 변화 삭제
     */
    public ResponseHandler<?> deleteChange(ChangeDeleteReq req) {
        final ResponseHandler<?> result = new ResponseHandler<>();
        try {
            /* 다중 선택 삭제 */
            for(int index=0; index < req.getChgSeqList().size(); index++){
                req.setChgSeq(Integer.parseInt((String) req.getChgSeqList().get(index)));
                aChangeDao.deleteChange(req);
            }
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }
        return result;
    }
}

