package com.modern.admin.classUser.dao;

import com.modern.admin.classUser.dto.model.AClassUserVO;
import com.modern.admin.classUser.dto.res.AClassUserListRes;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface AClassUserDao{
    List<AClassUserVO> selectClassUser();
}
