package com.modern.admin.classUser.dto.model;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="클래스 유저 VO")
public class AClassUserVO implements Serializable {

    private Integer paySeq;
    private int userSeq;
    private String classCd;
    private String classEndDt;
    private String productSeq;
    private String tel;
    private String delYn;
    private String creId;
    private String creDt;

}