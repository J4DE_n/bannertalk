package com.modern.admin.classUser.dto.req;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="클래스 유저 조회")
public class AClassUserListReq{
    @ApiParam(value = "부모카테고리 시퀀스")
    private String pCategorySeq;
}