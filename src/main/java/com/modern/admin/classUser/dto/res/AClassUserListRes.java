package com.modern.admin.classUser.dto.res;

import com.modern.admin.classUser.dto.model.AClassUserVO;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "카테고리 조회 목록")
public class AClassUserListRes {
    private List<AClassUserVO> list;
}
