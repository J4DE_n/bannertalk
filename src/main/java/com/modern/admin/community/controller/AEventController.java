package com.modern.admin.community.controller;

import com.modern.admin.community.dto.req.*;
import com.modern.admin.community.dto.res.EventInfoRes;
import com.modern.admin.community.dto.res.EventListRes;
import com.modern.admin.community.service.AEventService;

import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.file.config.ConfigFile;
import com.modern.common.file.service.FileService;
import com.modern.common.session.SessionCheck;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


/**
 *  관리자 Event 관리 Rest Controller class
 *
 *  관리자를 제어하기 위한 REST API가 정의 되어 있다.
 */
@RestController
@RequestMapping(value="/mapi/event" ) //모듈명 정의
@Api(value = "AEventController", description = "이벤트 API")
public class AEventController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	AEventService aEventService;

	@Autowired
	FileService fileService;

	@Autowired
	ConfigFile configFile;

	/**
	 * 이벤트 목록
	 *
	 * @param : 사용자에서 받아온 저장값
	 * @return : ReturnType
	 */
	@ApiOperation(value = "이벤트 목록")
	@GetMapping
	public ResponseHandler<EventListRes> list(EventListReq req) {
		ResponseHandler<EventListRes> result = aEventService.selectEvent(req);

		return  result;
	}
	
	/**
	 *  이벤트 저장
	 */
	@ApiOperation(value = "이벤트 등록")
	@PostMapping
	@SessionCheck
	public ResponseHandler<?> save(EventInsertReq req, @RequestParam("attach") MultipartFile[] files) {
		ResponseHandler<?> result = aEventService.insertEvent(req, files);
		return result;
	}

	/**
	 * 이벤트 수정
	 *
	 * @param : 사용자에서 받아온 저장값
	 * @return : ReturnType
	 */
	@ApiOperation(value = "이벤트 수정")
	@PostMapping(value="/edit")
	@SessionCheck
	public ResponseHandler<?> update(EventUpdateReq req, @RequestParam("attach") MultipartFile[] files) {
		ResponseHandler<?> result = aEventService.updateEvent(req, files);
		return  result;
	}

	/**
	 * 이벤트 수정
	 *
	 * @param : 사용자에서 받아온 저장값
	 * @return : ReturnType
	 */
	@ApiOperation(value = "이벤트 노출여부만 수정")
	@PostMapping(value="/update")
	@SessionCheck
	public ResponseHandler<?> updateShowYn(@RequestBody(required = false) EventShowYnReq req) {
		ResponseHandler<?> result = aEventService.updateEventShowYn(req);
		return  result;
	}

	/**
	 * 이벤트 삭제
	 *
	 * @param : 사용자에서 받아온 저장값
	 * @return : ReturnType
	 */
	@ApiOperation(value = "이벤트 삭제")
	@DeleteMapping
	@SessionCheck
	public ResponseHandler<?> delete(@RequestBody(required = false) EventDeleteReq req) {
		ResponseHandler<?> result = aEventService.deleteEvent(req);
		return  result;
	}

	/**
	 * 이벤트 상세
	 *
	 * @param : 사용자에서 받아온 저장값
	 * @return : ReturnType
	 */
	@ApiOperation(value = "이벤트 상세")
	@GetMapping(value="/item")
	public ResponseHandler<EventInfoRes> info(EventInfoReq req) {
		ResponseHandler<EventInfoRes> result = aEventService.selectEventInfo(req);
		return  result;
	}

}
