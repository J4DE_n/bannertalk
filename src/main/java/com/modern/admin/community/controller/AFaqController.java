package com.modern.admin.community.controller;

import com.modern.admin.category.dto.req.ACategoryListReq;
import com.modern.admin.category.service.ACategoryService;
import com.modern.admin.community.dto.req.*;
import com.modern.admin.community.dto.res.FaqInfoRes;
import com.modern.admin.community.dto.res.FaqListRes;
import com.modern.admin.community.service.AFaqService;

import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.session.SessionCheck;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 *  Admin Faq Rest Controller class
 *
 *  사용자를 제어하기 위한 REST API가 정의 되어 있다.
 */
@RestController
@RequestMapping(value="/mapi/faq" ) //모듈명 정의
@Api(value = "FaqController", description = "FAQ 관련 API")
public class AFaqController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final String CATEGORY_TYPE = "CATEGORY_FAQ";

	@Autowired
	AFaqService AFaqService;

	@Autowired
	ACategoryService aCategoryService;

	/**
	 * FAQ 카테고리
	 *
	 * @param : FAQ 카테고리 조회
	 * @return : ResponseHandler
	 */
	@ApiOperation(value = "FAQ 카테고리 조회")
	@GetMapping(value = "/category")
	public ResponseHandler<?> category(ACategoryListReq req) {
		req.setCategoryType(CATEGORY_TYPE);
		ResponseHandler<?> result = aCategoryService.selectCategory(req);
		return  result;
	}

	/**
	 * FAQ 목록
	 *
	 *
	 * @param : 사용자에서 받아온 저장값
	 * @return : ReturnType
	 */
	@ApiOperation(value = "FAQ 목록")
	@GetMapping
	public ResponseHandler<FaqListRes> list(FaqListReq req) {
		ResponseHandler<FaqListRes> result = AFaqService.selectFaq(req);
		return  result;
	}
	
	/**
	 *  FAQ 저장
	 */
	@ApiOperation(value = "FAQ 등록")
	@PostMapping
	@SessionCheck
	public ResponseHandler<?> save(FaqInsertReq req) {
		ResponseHandler<?> result = AFaqService.insertFaq(req);
		return result;
	}

	/**
	 * FAQ 수정
	 *
	 *
	 * @param : 사용자에서 받아온 저장값
	 * @return : ReturnType
	 */
	@ApiOperation(value = "FAQ 수정")
	@PostMapping(value="/edit")
	@SessionCheck
	public ResponseHandler<?> update(FaqUpdateReq req) {
		ResponseHandler<?> result = AFaqService.updateFaq(req);
		return  result;
	}

	/**
	 * FAQ 삭제
	 *
	 *
	 * @param : 사용자에서 받아온 저장값
	 * @return : ReturnType
	 */
	@ApiOperation(value = "FAQ 삭제")
	@DeleteMapping
	@SessionCheck
	public ResponseHandler<?> delete(@RequestBody(required = false) FaqDeleteReq req) {
		ResponseHandler<?> result = AFaqService.deleteFaq(req);
		return  result;
	}


	/**
	 * FAQ 상세
	 *
	 *
	 * @param : 사용자에서 받아온 저장값
	 * @return : ReturnType
	 */
	@ApiOperation(value = "FAQ 상세")
	@GetMapping(value="/item")
	public ResponseHandler<FaqInfoRes> info(FaqInfoReq req) {
		ResponseHandler<FaqInfoRes> result = AFaqService.selectFaqInfo(req);
		return  result;
	}

}
