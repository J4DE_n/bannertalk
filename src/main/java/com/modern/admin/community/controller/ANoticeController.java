package com.modern.admin.community.controller;

import com.modern.admin.community.dto.req.*;
import com.modern.admin.community.dto.res.NoticeInfoRes;
import com.modern.admin.community.dto.res.NoticeListRes;
import com.modern.admin.community.service.ANoticeService;
import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.file.config.ConfigFile;
import com.modern.common.file.dto.req.FileSaveReq;
import com.modern.common.file.dto.res.FileInfoRes;
import com.modern.common.file.service.FileService;
import com.modern.common.session.SessionCheck;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;


/**
 *  멤버쉽 Rest Controller class
 *
 *  사용자를 제어하기 위한 REST API가 정의 되어 있다.
 *
 *
 */
@RestController
@RequestMapping(value="/mapi/notice" ) //모듈명 정의
@Api(value = "NoticeController", description = "공지사항 API")
public class ANoticeController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	ANoticeService aNoticeService;

	@Autowired
	FileService fileService;

	@Autowired
	ConfigFile configFile;

	/**
	 * 공지사항 목록
	 *
	 *
	 * @param : 사용자에서 받아온 저장값
	 * @return : ReturnType
	 */
	@ApiOperation(value = "공지사항 목록")
	@GetMapping
	public ResponseHandler<NoticeListRes> list(NoticeListReq req) {
		ResponseHandler<NoticeListRes> result = aNoticeService.selectNotice(req);

		return  result;
	}
	
	/**
	 *  공지사항 저장
	 */
	@ApiOperation(value = "공지사항 등록")
	@PostMapping
	@SessionCheck
	public ResponseHandler<?> save(NoticeInsertReq req) {
		ResponseHandler<?> result = aNoticeService.insertNotice(req);
		return result;
	}

	/**
	 * 공지사항 수정
	 *
	 *
	 * @param : 사용자에서 받아온 저장값
	 * @return : ReturnType
	 */
	@ApiOperation(value = "공지사항 수정")
	@PostMapping(value="/edit")
	@SessionCheck
	public ResponseHandler<?> update(NoticeUpdateReq req) {
		ResponseHandler<?> result = aNoticeService.updateNotice(req);
		return  result;
	}

	/**
	 * 공지사항 수정
	 *
	 *
	 * @param : 사용자에서 받아온 저장값
	 * @return : ReturnType
	 */
	@ApiOperation(value = "공지사항 수정")
	@PostMapping(value="/update")
	@SessionCheck
	public ResponseHandler<?> update(@RequestBody(required = false) NoticeShowYnReq req) {
		ResponseHandler<?> result = aNoticeService.updateNoticeShowYn(req);
		return  result;
	}

	/**
	 * 공지사항 삭제
	 *
	 * @param : 사용자에서 받아온 저장값
	 * @return : ReturnType
	 */
	@ApiOperation(value = "공지사항 삭제")
	@DeleteMapping
	@SessionCheck
	public ResponseHandler<?> delete(@Valid @RequestBody(required = false) final NoticeDeleteReq req) {
		ResponseHandler<?> result = aNoticeService.deleteNotice(req);
		return  result;
	}


	/**
	 * 공지사항 상세
	 *
	 * @param : 사용자에서 받아온 저장값
	 * @return : ReturnType
	 */
	@ApiOperation(value = "공지사항 상세")
	@GetMapping(value="/item")
	public ResponseHandler<NoticeInfoRes> info(NoticeInfoReq req) {
		ResponseHandler<NoticeInfoRes> result = aNoticeService.selectNoticeInfo(req);
		return  result;
	}


}
