package com.modern.admin.community.controller;

import com.modern.admin.community.dto.req.*;
import com.modern.admin.community.dto.res.FaqInfoRes;
import com.modern.admin.community.dto.res.FaqListRes;
import com.modern.admin.community.dto.res.QnaInfoRes;
import com.modern.admin.community.dto.res.QnaListRes;
import com.modern.admin.community.service.AFaqService;
import com.modern.admin.community.service.AQnaService;
import com.modern.common.dto.res.ResponseHandler;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 *  멤버쉽 Rest Controller class
 *
 *  사용자를 제어하기 위한 REST API가 정의 되어 있다.
 *
 *
 */
@RestController
@RequestMapping(value="/mapi/qna" ) //모듈명 정의
@Api(value = "FaqController", description = "일대일 문의 관리자 API")
public class AQNAController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	AQnaService aQnaService;

	/**
	 * QNA 카테고리
	 *
	 *
	 * @param : QNA 카테고리 조회
	 * @return : ResponseHandler
	 */
	@ApiOperation(value = "QNA 카테고리 조회")
	@GetMapping(value = "/category")
	public ResponseHandler<?> category() {
		ResponseHandler<?> result = aQnaService.selectCategory();

		return  result;
	}


	/**
	 * 일대일 문의 목록
	 *
	 *
	 * @param : 사용자에서 받아온 저장값
	 * @return : ReturnType
	 */
	@ApiOperation(value = "일대일 문의 목록")
	@GetMapping
	public ResponseHandler<QnaListRes> list(QnaListReq req) {
		ResponseHandler<QnaListRes> result = aQnaService.selectQna(req);

		return  result;
	}

	/**
	 * 일대일 상세
	 *
	 *
	 * @param : 사용자에서 받아온 저장값
	 * @return : ReturnType
	 */
	@ApiOperation(value = "일대일 문의 상세")
	@GetMapping(value="/item")
	public ResponseHandler<QnaInfoRes> info(QnaInfoReq req) {
		ResponseHandler<QnaInfoRes> result = aQnaService.selectQnaInfo(req);
		return  result;
	}

}
