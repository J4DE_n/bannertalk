package com.modern.admin.community.dao;
import com.modern.admin.community.dto.model.EventVO;
import com.modern.admin.community.dto.req.EventDeleteReq;
import com.modern.admin.community.dto.req.EventInsertReq;
import com.modern.admin.community.dto.req.EventShowYnReq;
import com.modern.admin.community.dto.res.EventInfoRes;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface AEventDao
{
    void insertEvent(EventInsertReq req);

    void updateEvent(Map<String, Object> params);

    EventInfoRes selectEventInfo(Map<String, Object> params);

    List<EventVO> selectEvent(Map<String, Object> params);

    int selectEventCnt(Map<String, Object> params);

    void updateEventShowYn(EventShowYnReq req);

    void deleteEvent(EventDeleteReq req);

    String selectMaxEventSeq();
}
