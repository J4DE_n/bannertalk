package com.modern.admin.community.dao;
import com.modern.admin.community.dto.model.FaqVO;
import com.modern.admin.community.dto.res.FaqInfoRes;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface AFaqDao
{
    void insertFaq(Map<String, Object> params);

    void updateFaq(Map<String, Object> params);

    FaqInfoRes selectFaqInfo(Map<String, Object> params);

    List<FaqVO> selectFaq(Map<String, Object> params);

    int selectFaqCnt(Map<String, Object> params);

    void deleteFaq(Map<String, Object> params);
}
