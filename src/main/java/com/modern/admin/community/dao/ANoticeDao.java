package com.modern.admin.community.dao;
import com.modern.admin.community.dto.model.NoticeVO;
import com.modern.admin.community.dto.req.NoticeDeleteReq;
import com.modern.admin.community.dto.req.NoticeInsertReq;
import com.modern.admin.community.dto.req.NoticeShowYnReq;
import com.modern.admin.community.dto.res.NoticeInfoRes;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface ANoticeDao
{
    void insertNotice(NoticeInsertReq req);

    void updateNotice(Map<String, Object> params);

    NoticeInfoRes selectNoticeInfo(Map<String, Object> params);

    List<NoticeVO> selectNotice(Map<String, Object> params);

    int selectNoticeCnt(Map<String, Object> params);

    void updateNoticeShowYn(NoticeShowYnReq req);

    void deleteNotice(NoticeDeleteReq req);
}
