package com.modern.admin.community.dao;
import com.modern.admin.community.dto.model.NoticeVO;
import com.modern.admin.community.dto.model.QnaVO;
import com.modern.admin.community.dto.res.NoticeInfoRes;
import com.modern.admin.community.dto.res.QnaInfoRes;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface AQnaDao
{
    QnaInfoRes selectQnaInfo(Map<String, Object> params);

    List<QnaVO> selectQna(Map<String, Object> params);

    int selectQnaCnt(Map<String, Object> params);
}
