package com.modern.admin.community.dto.model;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="이벤트")
public class EventVO implements Serializable {
    private int rownum;
    private String eventSeq;
    private String eventNm;
    private String classCd;
    private String classCdNm;
    private String introInfo;
    private String guideInfo;
    private String startDt;
    private String endDt;
    private Integer thumbnailFileGrpSeq;
    private String viewCnt;
    private String showYn;
    private String delYn;
    private String userId;
    private String userName;
    private int creId;
    private String creDt;
    private int updId;
    private String updDt;
}
