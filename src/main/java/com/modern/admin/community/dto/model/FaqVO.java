package com.modern.admin.community.dto.model;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="공지사항")
public class FaqVO implements Serializable {
    private Integer rownum;
    private int faqSeq;
    private int categorySeq;
    private String categoryNm;
    private String title;
    private String contents;
    private Integer sortNo;
    private String userId;
    private String userName;
    private String delYn;
    private int creId;
    private String creDt;
}
