package com.modern.admin.community.dto.model;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="공지사항")
public class NoticeVO implements Serializable {
    private int rownum;
    private int noticeSeq;
    private String title;
    private String summary;
    private String contents;
    private String thumbnailFileGrpNo;
    private String viewCnt;
    private String showYn;
    private String userName;
    private String userId;
    private String delYn;
    private int creId;
    private String startDt;
    private String creDt;
}
