package com.modern.admin.community.dto.model;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="공지사항")
public class QnaVO implements Serializable {
    private Integer rownum;
    private int qnaSeq;
    private String reqTel;
    private String reqNm;
    private String reqEmail;
    private String category;
    private String categoryNm;
    private String title;
    private String contents;
    private Integer attachFileGrpSeq;
    private String delYn;
    private int creId;
    private String creDt;
}