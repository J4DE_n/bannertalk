package com.modern.admin.community.dto.req;
import com.modern.common.dto.req.CommonReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@ApiModel(value ="이벤트 삭제")
public class EventDeleteReq extends CommonReq implements Serializable {

    @ApiParam(value = "이벤트 시퀀스")
    private String eventSeq;

    @ApiParam(value = "이벤트 시퀀스 리스트")
    private List eventSeqList;

}
