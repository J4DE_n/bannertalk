package com.modern.admin.community.dto.req;
import com.modern.common.dto.req.CommonReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel(value ="이벤트 상세")
public class EventInfoReq extends CommonReq implements Serializable {

    @ApiParam(value = "이벤트 시퀀스", required = true)
    @NotNull
    private String eventSeq;

}
