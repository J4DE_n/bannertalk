package com.modern.admin.community.dto.req;
import com.modern.common.dto.req.CommonReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;

@Data
@ApiModel(value ="이벤트 등록")
public class EventInsertReq extends CommonReq implements Serializable {

    @ApiParam(value = "이벤트 시퀀스")
    private String eventSeq;

    @ApiParam(value = "제목")
    private String eventNm;

    @ApiParam(value = "도입부")
    private String inventInfo;

    @ApiParam(value = "하단부")
    private String guideInfo;

    @ApiParam(value = "대표이미지")
    private MultipartFile attach;

    @ApiParam(value = "썸네일 이미지 ")
    @Ignore
    private Integer thumbnailFileGrpSeq;

    @ApiParam(value = "생성일")
    private String showYn;

    @ApiParam(value = "에디터 삭제 이미지", hidden = true)
    private ArrayList<String> editorDelImg;

    @ApiParam(value = "생성일")
    private String startDt;

    @ApiParam(value = "시스템 생성일")
    private String creDt;
}
