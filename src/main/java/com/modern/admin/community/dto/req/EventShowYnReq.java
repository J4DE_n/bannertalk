package com.modern.admin.community.dto.req;
import com.modern.common.dto.req.CommonReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;

@Data
@ApiModel(value ="이벤트 수정")
public class EventShowYnReq extends CommonReq implements Serializable {

    @ApiParam(value = "이벤트 시퀀스", required = true)
    @NotNull
    private String eventSeq;

    @ApiParam(value = "노출여부")
    private String showYn;

}
