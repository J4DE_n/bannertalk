package com.modern.admin.community.dto.req;
import com.modern.common.dto.req.CommonReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;

@Data
@ApiModel(value ="이벤트 수정")
public class EventUpdateReq extends CommonReq implements Serializable {

    @ApiParam(value = "이벤트 시퀀스", required = true)
    @NotNull
    private String eventSeq;

    @ApiParam(value = "제목")
    private String eventNm;

    @ApiParam(value = "도입부")
    private String introInfo;

    @ApiParam(value = "하단부")
    private String guideInfo;

    @ApiParam(value = "삭제여부")
    private String delYn;

    @ApiParam(value = "노출여부")
    private String showYn;

    @ApiParam(value = "작성일")
    private String startDt;

    @ApiParam(value = "시스템 작성일")
    private String creDt;

    @ApiParam(value = "썸네일 파일 그룹")
    private Integer thumbnailFileGrpSeq;

    @ApiParam(value = "에디터 삭제 이미지", hidden = true)
    private ArrayList<String> editorDelImg;

}
