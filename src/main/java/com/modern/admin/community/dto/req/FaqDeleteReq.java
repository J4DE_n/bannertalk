package com.modern.admin.community.dto.req;
import com.modern.common.dto.req.CommonReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(value ="FAQ 삭제")
public class FaqDeleteReq extends CommonReq implements Serializable {

    @ApiParam(value = "faq 시퀀스")
    private int faqSeq;

    @ApiParam(value = "faq 시퀀스 리스트")
    private List faqSeqList;

}
