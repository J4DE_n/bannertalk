package com.modern.admin.community.dto.req;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel(value ="블로그 상세")
public class FaqInfoReq implements Serializable {
    @ApiParam(value = "블로그 시퀀스", required = true)
    @NotNull
    private int faqSeq;
}
