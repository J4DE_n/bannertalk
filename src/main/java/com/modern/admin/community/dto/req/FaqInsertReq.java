package com.modern.admin.community.dto.req;
import com.modern.common.dto.req.CommonReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel(value ="자주묻는 질문 등록")
public class FaqInsertReq extends CommonReq {
    @ApiParam(value = "카테고리 고유 시퀀스", required = true)
    @NotNull
    private String categorySeq;

    @ApiParam(value = "제목", required = true)
    @NotNull
    private String title;

    @ApiParam(value = "내용")
    private String contents;

    @ApiParam(value = "정렬번호")
    private int sortNo;
}
