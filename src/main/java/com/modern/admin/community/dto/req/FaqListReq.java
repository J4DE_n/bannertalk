package com.modern.admin.community.dto.req;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel(value ="FAQ 목록")
public class FaqListReq implements Serializable {
    @ApiParam(value = "페이지 조회 개수", required = true)
    private int currentPage;

    @ApiParam(value = "노출 게시물 개수", required = true)
    @NotNull
    private int cntPerPage;

    @ApiParam(value = "검색조건 검색단어")
    private String searchText;


    @ApiParam(value = "검색조건 카테고리 고유 시퀀스")
    private Integer categorySeq;

    /* *
     *  검색 조건
     *
     *  searchCon = T --> 제목만
     *  searchCon = C --> 내용만
     *  searchCon = TC --> 제목 + 내용
     *
     * */
    @ApiParam(value = "검색조건")
    private String searchCon;

    @ApiParam(value = "검색조건 카테고리")
    private String searchCategory;

    @Ignore
    @ApiParam(value = "시작 번호")
    private int startRow;

    public void setStartRow() {
        this.startRow = (this.currentPage - 1) * this.cntPerPage;
    }
}
