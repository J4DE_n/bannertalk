package com.modern.admin.community.dto.req;
import com.modern.common.dto.req.CommonReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel(value ="FAQ 수정")
public class FaqUpdateReq extends CommonReq implements Serializable {
    @ApiParam(value = "faq 시퀀스", required = true)
    @NotNull
    private int faqSeq;

    @ApiParam(value = "카테고리 고유 시퀀스")
    private int categorySeq;

    @ApiParam(value = "제목")
    private String title;

    @ApiParam(value = "내용")
    private String contents;

    @ApiParam(value = "정렬번호")
    private int sortNo;

    @ApiParam(value = "삭제여부")
    private String delYn;
}
