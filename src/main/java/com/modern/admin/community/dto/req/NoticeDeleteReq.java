package com.modern.admin.community.dto.req;
import com.modern.common.dto.req.CommonReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(value ="이벤트 삭제")
public class NoticeDeleteReq extends CommonReq implements Serializable {

    @ApiParam(value = "공지사항ㄷ 시퀀스")
    private int noticeSeq;

    @ApiParam(value = "공지사항 시퀀스 리스트")
    private List noticeSeqList;

}
