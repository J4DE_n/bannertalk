package com.modern.admin.community.dto.req;
import com.modern.common.dto.req.CommonReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;

@Data
@ApiModel(value ="공지사항 등록")
public class NoticeInsertReq extends CommonReq {
    @ApiParam(value = "제목", required = true)
    @NotNull
    private String title;

    @ApiParam(value = "요약")
    private String summary;

    @ApiParam(value = "노출여부")
    private String showYn;

    @ApiParam(value = "내용", required = true)
    @NotNull
    private String contents;

    @ApiParam(value = "썸네일 이미지 ", hidden = true)
    @Ignore
    private Integer fileGrpSeq;

    @ApiParam(value = "에디터 삭제 이미지", hidden = true)
    private ArrayList<String> editorDelImg;

    @ApiParam(value = "입력 작성일")
    private String startDt;

}
