package com.modern.admin.community.dto.req;
import com.modern.common.dto.req.CommonReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;

@Data
@ApiModel(value ="공지사항 수정")
public class NoticeUpdateReq extends CommonReq implements Serializable {
    @ApiParam(value = "공지사항 시퀀스", required = true)
    @NotNull
    private int noticeSeq;

    @ApiParam(value = "제목", required = true)
    private String title;

    @ApiParam(value = "부제목")
    private String summary;

    @ApiParam(value = "내용", required = true)
    private String contents;

    @ApiParam(value = "삭제여부", required = true)
    private String delYn;

    @ApiParam(value = "썸네일 파일 그룹", required = true)
    private int fileGrpSeq;

    @ApiParam(value = "에디터 삭제 이미지", hidden = true)
    private ArrayList<String> editorDelImg;

    @ApiParam(value = "노출여부")
    private String showYn;

    @ApiParam(value = "작성일")
    private String startDt;

    @ApiParam(value = "시스템 작성일")
    private String creDt;
}
