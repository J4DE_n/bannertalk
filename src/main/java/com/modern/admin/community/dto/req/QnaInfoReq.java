package com.modern.admin.community.dto.req;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel(value ="qna 상세")
public class QnaInfoReq implements Serializable {
    @ApiParam(value = "qna 시퀀스", required = true)
    @NotNull
    private int qnaSeq;
}
