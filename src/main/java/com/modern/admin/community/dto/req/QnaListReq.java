package com.modern.admin.community.dto.req;
import com.modern.common.dto.req.PagingReq;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel(value ="QNA 목록")
public class QnaListReq extends PagingReq implements Serializable {
    @ApiParam(value = "검색조건 검색단어")
    private String searchText;

    @ApiParam(value = "검색조건 카테고리")
    private String searchCategory;
}
