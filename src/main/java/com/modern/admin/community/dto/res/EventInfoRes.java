package com.modern.admin.community.dto.res;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="이벤트 상세")
public class EventInfoRes implements Serializable {

    @ApiParam(value = "이벤트 시퀀스", required = true)
    private String eventSeq;

    @ApiParam(value = "클래스 코드")
    private String classCd;

    @ApiParam(value = "이벤트 제목")
    private String eventNm;

    @ApiParam(value = "상단부")
    private String introInfo;

    @ApiParam(value = "하단부")
    private String guideInfo;

    @ApiParam(value = "이벤트 시작일")
    private String startDt;

    @ApiParam(value = "이벤트 종료일")
    private String endDt;

    @ApiParam(value = "노출여부")
    private String showYn;

    @ApiParam(value = "썸네일")
    private String filePath;

    @ApiParam(value = "썸네일 seq")
    @Ignore
    private int fileSeq;

    @ApiParam(value = "파일 그룹 seq")
    private int thumbnailFileGrpSeq;

    @ApiParam(value = "오리지널 썸네일 시스템 명")
    private String OrgFileNm;

    @ApiParam(value = "썸네일 시스템 명")
    private String SysFileNm;

    @ApiParam(value = "삭제여부")
    private String delYn;

    @ApiParam(value = "삭제여부")
    private String creDt;
}
