package com.modern.admin.community.dto.res;

import com.modern.admin.community.dto.model.EventVO;
import com.modern.admin.community.dto.model.NoticeVO;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "이벤트 목록 정보")
public class EventListRes {

    private List<EventVO> eventList;

    /** 현재페이지 */
    private int currentPage;
    private int totalCnt;
}
