package com.modern.admin.community.dto.res;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="faq 상세")
public class FaqInfoRes implements Serializable {
    @ApiParam(value = "faq 시퀀스")
    private int faqSeq;

    @ApiParam(value = "카테고리 고유 시퀀스")
    private String categorySeq;

    @ApiParam(value = "제목")
    private String title;

    @ApiParam(value = "내용")
    private String contents;

    @ApiParam(value = "정렬번호")
    private String sortNo;

    @ApiParam(value = "등록일")
    private int creDt;

    @ApiParam(value = "삭제여부")
    private String delYn;
}
