package com.modern.admin.community.dto.res;

import com.modern.admin.community.dto.model.FaqVO;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "faq 목록 정보")
public class FaqListRes {

    private List<FaqVO> list;

    /** 현재페이지 */
    private int currentPage;
    private int totalCnt;
}
