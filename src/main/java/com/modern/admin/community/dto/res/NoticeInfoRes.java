package com.modern.admin.community.dto.res;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="공지사항 상세")
public class NoticeInfoRes implements Serializable {
    @ApiParam(value = "블로그 시퀀스", required = true)
    private int noticeSeq;

    @ApiParam(value = "제목", required = true)
    private String title;

    @ApiParam(value = "요약")
    private String summary;

    @ApiParam(value = "썸네일")
    private String filePath;

    @ApiParam(value = "썸네일 seq")
    @Ignore
    private int fileSeq;

    @ApiParam(value = "파일 그룹 seq")
    @Ignore
    private int fileGrpSeq;

    @ApiParam(value = "썸네일 시스템 명")
    @Ignore
    private String SysFileNm;

    @ApiParam(value = "내용", required = true)
    private String contents;

    @ApiParam(value = "작성일")
    private String startDt;

    @ApiParam(value = "시스템 작성일")
    private String creDt;

    @ApiParam(value = "노출 여부")
    private String showYn;

    @ApiParam(value = "삭제여부", required = true)
    private String delYn;
}
