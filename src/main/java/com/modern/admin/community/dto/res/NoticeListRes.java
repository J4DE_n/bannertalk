package com.modern.admin.community.dto.res;

import com.modern.admin.community.dto.model.NoticeVO;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "공지사항 목록 정보")
public class NoticeListRes {

    private List<NoticeVO> noticeList;

    /** 현재페이지 */
    private int currentPage;
    private int totalCnt;
}
