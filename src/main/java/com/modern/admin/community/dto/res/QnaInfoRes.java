package com.modern.admin.community.dto.res;
import com.modern.common.file.dto.model.FileInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(value ="Qna 상세")
public class QnaInfoRes implements Serializable {
    @ApiParam(value = "QNA 시퀀스")
    private int qnaSeq;

    @ApiParam(value = "요청자 전화번호")
    private String reqTel;

    @ApiParam(value = "요청자 이름")
    private String reqNm;

    @ApiParam(value = "요청자 이메일")
    private String reqEmail;

    @ApiParam(value = "카테고리")
    @Ignore
    private int category;

    @ApiParam(value = "카테고리 명")
    @Ignore
    private String categoryNm;

    @ApiParam(value = "내용")
    @Ignore
    private String contents;

    @ApiParam(value = "첨부파일 시퀀스")
    @Ignore
    private Integer attachFileGrpSeq;

    @ApiParam(value = "등록일")
    private String creDt;

    @ApiParam(value = "첨부파일목록")
    private List<FileInfo> fileList;


}
