package com.modern.admin.community.dto.res;

import com.modern.admin.community.dto.model.NoticeVO;
import com.modern.admin.community.dto.model.QnaVO;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "Qna 목록 정보")
public class QnaListRes {

    private List<QnaVO> list;

    /** 현재페이지 */
    private int currentPage;
    private int totalCnt;
}
