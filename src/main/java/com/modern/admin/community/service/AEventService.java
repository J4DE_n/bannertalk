package com.modern.admin.community.service;

import com.modern.admin.community.dao.AEventDao;
import com.modern.admin.community.dto.model.EventVO;
import com.modern.admin.community.dto.req.*;
import com.modern.admin.community.dto.res.EventInfoRes;
import com.modern.admin.community.dto.res.EventListRes;
import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.file.config.ConfigFile;
import com.modern.common.file.dto.model.FileDeleteInfo;
import com.modern.common.file.dto.req.FileDeleteReq;
import com.modern.common.file.dto.req.FileSaveReq;
import com.modern.common.file.dto.res.FileInfoRes;
import com.modern.common.file.service.FileService;
import com.modern.common.type.ReturnType;
import com.modern.common.util.CommonUtil;
import com.modern.common.util.ConvertUtil;
import com.modern.membership.service.MembershipService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * 입력폼 저장을 위한 클래스
 *
 *
 */
@Service
public class AEventService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    AEventDao aEventDao;

    @Autowired
    private ConfigFile configFile;

    @Autowired
    private FileService fileService;

    @Autowired
    private MembershipService membershipService;


    /* 공지사항 저장 */
    public ResponseHandler<?> insertEvent(EventInsertReq req, MultipartFile[] files) {
        final ResponseHandler<?> result = new ResponseHandler<>();
        try {
            if(!files[0].isEmpty()) {
                //1. 파일 저장
                FileSaveReq fileSaveReq = new FileSaveReq();
                fileSaveReq.setCategory(configFile.getSelectCategory9());
                fileSaveReq.setIsSaveInfos(true);
                fileSaveReq.setUserSeq(req.getUserSeq());
                FileInfoRes fileInfoRes = fileService.storeFiles(files, fileSaveReq);
                req.setThumbnailFileGrpSeq(fileInfoRes.getFileGrpSeq());
            }

            // EVENT_SEQ 생성 MAX+1
            int eventSeq = Integer.parseInt(aEventDao.selectMaxEventSeq());
            req.setEventSeq(String.valueOf(eventSeq));

            //2. 정보저장
            aEventDao.insertEvent(req);

            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }
        return result;
    }

    /* 공지사항 수정 */
    public ResponseHandler<?> updateEvent(EventUpdateReq req, MultipartFile[] files) {
        final ResponseHandler<?> result = new ResponseHandler<>();

        try {
            // 1. 썸네일 수정일 경우
            if(!files[0].isEmpty()) {
                Map<String, Object> params = ConvertUtil.convertObjectToMap(req);
                EventInfoRes infoRes = aEventDao.selectEventInfo(params);

                if(CommonUtil.isNotEmpty(infoRes.getEventSeq())){

                    if(infoRes.getFileSeq() > 0 ){ //썸네일 등록된 경우
                        //1-2. 수정일 경우 물리 파일 삭제
                        List<FileDeleteInfo> fileDelInfoList = new ArrayList<>();
                        FileDeleteInfo fileDeleteInfo = new FileDeleteInfo();
                        fileDeleteInfo.setFileSeq(infoRes.getFileSeq());
                        fileDelInfoList.add(fileDeleteInfo);

                        FileDeleteReq fileDeleteReq = new FileDeleteReq();
                        fileDeleteReq.setCategory(configFile.getSelectCategory1());
                        fileDeleteReq.setFileGrpSeq(infoRes.getThumbnailFileGrpSeq());
                        fileDeleteReq.setFileDeleteInfos(fileDelInfoList);
                        fileDeleteReq.setUserSeq(req.getUserSeq());

                        fileService.deleteFiles(fileDeleteReq);
                    }
                }

                //1-3. 썸네일 이미지 등록
                FileSaveReq fileSaveReq = new FileSaveReq();
                fileSaveReq.setCategory(configFile.getSelectCategory1()); //카테고리
                fileSaveReq.setIsSaveInfos(true); //파일 테이블 저장
                fileSaveReq.setUserSeq(req.getUserSeq());
                if(infoRes.getThumbnailFileGrpSeq() > 0){
                    fileSaveReq.setFileGrpSeq(infoRes.getThumbnailFileGrpSeq()); //파일 그룹 seq
                }
                FileInfoRes fileInfoRes = fileService.storeFiles(files, fileSaveReq); //파일 저장 로직
                req.setThumbnailFileGrpSeq(fileInfoRes.getFileGrpSeq());
            }


            //2. 공지사항 수정 정보 업데이트
            Map<String, Object> params = ConvertUtil.convertObjectToMap(req);
            params.put("userSeq", req.getUserSeq());
            aEventDao.updateEvent(params);

            //3. 에디터 삭제된 이미지 물리 삭제
            if(CommonUtil.isNotEmpty(req.getEditorDelImg())){

                List<FileDeleteInfo> fileDelInfoList = new ArrayList<>();

                ArrayList<String> imgs = req.getEditorDelImg();
                for(String nm : imgs){
                    FileDeleteInfo fileDeleteInfo = new FileDeleteInfo();
                    fileDeleteInfo.setFileName(nm);
                    fileDelInfoList.add(fileDeleteInfo);
                }

                FileDeleteReq fileDeleteReq = new FileDeleteReq();
                fileDeleteReq.setCategory(configFile.getSelectCategory9());
                fileDeleteReq.setIsSaveInfos(false);
                fileDeleteReq.setFileDeleteInfos(fileDelInfoList);

                fileService.deleteFiles(fileDeleteReq);
            }

            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }
        return result;
    }

    /* 공지사항 삭제 */
    public ResponseHandler<?> deleteEvent(EventDeleteReq req) {
        final ResponseHandler<?> result = new ResponseHandler<>();
        try {


            /* 다중 선택 삭제 */
            for(int index=0; index < req.getEventSeqList().size(); index++){
                req.setEventSeq((String) req.getEventSeqList().get(index));
                aEventDao.deleteEvent(req);
            }


            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }
        return result;
    }

    /* 블로그 상세 */
    public ResponseHandler<EventInfoRes> selectEventInfo(EventInfoReq req) {
        final ResponseHandler<EventInfoRes> result = new ResponseHandler<>();

        Map<String, Object> params = ConvertUtil.convertObjectToMap(req);
        try{
            EventInfoRes EventInfoRes = aEventDao.selectEventInfo(params);
            result.setData(EventInfoRes);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e){
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }
        return result;
    }

    /* 블로그 목록 */
    public ResponseHandler<EventListRes> selectEvent(EventListReq req) {
        final ResponseHandler<EventListRes> result = new ResponseHandler<>();

        req.setStartRow();
        Map<String, Object> params = ConvertUtil.convertObjectToMap(req);

        try {
            List<EventVO> list = aEventDao.selectEvent(params);
            int totalCnt = aEventDao.selectEventCnt(params);

            EventListRes EventListRes = new EventListRes();
            EventListRes.setEventList(list);
            EventListRes.setTotalCnt(totalCnt);

            result.setData(EventListRes);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e){
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }
        return result;
    }

    public ResponseHandler<?> updateEventShowYn(EventShowYnReq req) {
        final ResponseHandler<?> result = new ResponseHandler<>();

        try {
            aEventDao.updateEventShowYn(req);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }

        return result;
    }
}

