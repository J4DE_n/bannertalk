package com.modern.admin.community.service;

import com.modern.admin.community.dao.AFaqDao;
import com.modern.admin.community.dto.model.FaqVO;
import com.modern.admin.community.dto.req.*;
import com.modern.admin.community.dto.res.FaqInfoRes;
import com.modern.admin.community.dto.res.FaqListRes;
import com.modern.comCode.dto.req.ComCodeReq;
import com.modern.comCode.service.ComService;
import com.modern.common.config.ConfigCommon;
import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.type.ReturnType;
import com.modern.common.util.ConvertUtil;
import com.modern.membership.service.MembershipService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 *
 * FAQ를 위한 클래스
 *
 *
 */
@Service
public class AFaqService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    AFaqDao AFaqDao;

    @Autowired
    private ComService comService;

    @Autowired
    private ConfigCommon configCommon;

    /* FAQ 저장 */
    public ResponseHandler<?> insertFaq(FaqInsertReq req) {
        final ResponseHandler<?> result = new ResponseHandler<>();
        try {
            Map<String, Object> params = ConvertUtil.convertObjectToMap(req);
            params.put("userSeq", req.getUserSeq());
            AFaqDao.insertFaq(params);

            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }
        return result;
    }

    /* FAQ 수정 */
    public ResponseHandler<?> updateFaq(FaqUpdateReq req) {
        final ResponseHandler<?> result = new ResponseHandler<>();
        try {
            //2. FAQ 수정 정보 업데이트
            Map<String, Object> params = ConvertUtil.convertObjectToMap(req);
            params.put("userSeq", req.getUserSeq());
            AFaqDao.updateFaq(params);

            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }
        return result;
    }

    /* FAQ 삭제 */
    public ResponseHandler<?> deleteFaq(FaqDeleteReq req) {
        final ResponseHandler<?> result = new ResponseHandler<>();
        try {

            Map<String, Object> params = ConvertUtil.convertObjectToMap(req);
            params.put("userSeq", req.getUserSeq());

            /* 다중 선택 삭제 */
            for(int index=0; index < req.getFaqSeqList().size(); index++){
                params.put("faqSeq", Integer.parseInt((String)req.getFaqSeqList().get(index)));
                AFaqDao.deleteFaq(params);
            }

            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }
        return result;
    }

    /* FAQ 상세 */
    public ResponseHandler<FaqInfoRes> selectFaqInfo(FaqInfoReq req) {
        final ResponseHandler<FaqInfoRes> result = new ResponseHandler<>();

        Map<String, Object> params = ConvertUtil.convertObjectToMap(req);
        try{
            FaqInfoRes FaqInfoRes = AFaqDao.selectFaqInfo(params);
            result.setData(FaqInfoRes);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e){
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }
        return result;
    }

    /* FAQ 목록 */
    public ResponseHandler<FaqListRes> selectFaq(FaqListReq req) {
        final ResponseHandler<FaqListRes> result = new ResponseHandler<>();

        req.setStartRow();
        Map<String, Object> params = ConvertUtil.convertObjectToMap(req);

        try {
            List<FaqVO> list = AFaqDao.selectFaq(params);
            int totalCnt = AFaqDao.selectFaqCnt(params);

            FaqListRes FaqListRes = new FaqListRes();
            FaqListRes.setList(list);
            FaqListRes.setTotalCnt(totalCnt);

            result.setData(FaqListRes);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e){
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }
        return result;
    }


    public ResponseHandler<?> selectCategory() {

        ComCodeReq comCodeReq = new ComCodeReq();
        String[] mstCd = {configCommon.getMst().getFaq()};
        comCodeReq.setMstCdArr(mstCd);
        return comService.selectComCodeList(comCodeReq);
    }
}

