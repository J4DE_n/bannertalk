package com.modern.admin.community.service;

import com.modern.admin.community.dao.AQnaDao;
import com.modern.admin.community.dto.model.QnaVO;
import com.modern.admin.community.dto.req.QnaInfoReq;
import com.modern.admin.community.dto.req.QnaListReq;
import com.modern.admin.community.dto.res.QnaInfoRes;
import com.modern.admin.community.dto.res.QnaListRes;
import com.modern.comCode.dto.req.ComCodeReq;
import com.modern.comCode.service.ComService;
import com.modern.common.config.ConfigCommon;
import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.file.dto.model.FileInfo;
import com.modern.common.file.dto.req.FileListReq;
import com.modern.common.file.service.FileService;
import com.modern.common.type.ReturnType;
import com.modern.common.util.ConvertUtil;
import com.modern.membership.service.MembershipService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 *
 * QNA를 위한 클래스
 *
 *
 */
@Service
public class AQnaService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    AQnaDao aQnaDao;

    @Autowired
    private MembershipService membershipService;

    @Autowired
    private ComService comService;

    @Autowired
    private ConfigCommon configCommon;


    @Autowired
    private FileService fileService;

    /* 일대일 문의 상세 */
    public ResponseHandler<QnaInfoRes> selectQnaInfo(QnaInfoReq req) {
        final ResponseHandler<QnaInfoRes> result = new ResponseHandler<>();

        Map<String, Object> params = ConvertUtil.convertObjectToMap(req);
        try{

            //상세정보
            QnaInfoRes qnaInfoRes = aQnaDao.selectQnaInfo(params);

            //파일 정보
            FileListReq fileListReq = new FileListReq();
            fileListReq.setFileGrpSeq(qnaInfoRes.getAttachFileGrpSeq());
            List<FileInfo> fileList = fileService.getFileList(fileListReq);
            qnaInfoRes.setFileList(fileList);

            result.setData(qnaInfoRes);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e){
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }
        return result;
    }

    /* 일대일 문의 목록 */
    public ResponseHandler<QnaListRes> selectQna(QnaListReq req) {
        final ResponseHandler<QnaListRes> result = new ResponseHandler<>();

        req.setStartRow();
        Map<String, Object> params = ConvertUtil.convertObjectToMap(req);

        try {
            List<QnaVO> list = aQnaDao.selectQna(params);
            int totalCnt = aQnaDao.selectQnaCnt(params);

            QnaListRes qnaListRes = new QnaListRes();
            qnaListRes.setList(list);
            qnaListRes.setTotalCnt(totalCnt);

            result.setData(qnaListRes);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e){
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }
        return result;
    }

    public ResponseHandler<?> selectCategory() {

        ComCodeReq comCodeReq = new ComCodeReq();
        String[] mstCd = {configCommon.getMst().getQna()};
        comCodeReq.setMstCdArr(mstCd);
        return comService.selectComCodeList(comCodeReq);
    }

}

