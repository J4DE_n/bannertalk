package com.modern.admin.manager.controller;

import com.modern.admin.manager.dto.req.*;
import com.modern.admin.manager.dto.res.ManagerDetailRes;
import com.modern.admin.manager.dto.res.ManagerForMatchingListRes;
import com.modern.admin.manager.dto.res.ManagerListRes;
import com.modern.admin.manager.dto.res.MatchingUserListRes;
import com.modern.admin.manager.service.AManagerService;
import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.session.SessionManagerCheck;
import com.modern.membership.dto.req.ChangePasswordByCodeReq;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;


/**
 *  멤버쉽 Rest Controller class
 *
 *  사용자를 제어하기 위한 REST API가 정의 되어 있다.
 *
 *
 */


@RestController
@RequestMapping(value="/mapi/manager" ) //모듈명 정의
@Api(value = "AManagerController", description = "관리자 API")
public class AManagerController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	AManagerService aManagerService;

	/**
	 *  관리자(컨설턴트) 목록
	 *
	 *
	 * @param : 사용자에서 받아온 저장값
	 * @return : ReturnType
	 */
	@ApiOperation(value = "컨설턴트 목록")
	@GetMapping
	public ResponseHandler<ManagerListRes> list(@Valid ManagerListReq req) {
		ResponseHandler<ManagerListRes> result =aManagerService.selectManager(req);

		return  result;
	}
	
	/**
	 *  관리자(컨설턴트) 저장
	 */
	@ApiOperation(value = "컨설턴트 등록")
	@PostMapping
	@SessionManagerCheck
	public ResponseHandler<?> save(@Valid ManagerInsertReq req, HttpServletRequest request, HttpServletResponse response) {
		ResponseHandler<?> result = aManagerService.insertManager(req, request, response);
		return result;
	}

	/**
	 *  관리자(컨설턴트) 비밀번호 변경
	 */
	@ApiOperation(value = "관리자 비밀번호 변경")
	@PutMapping(value="/pwd")
	public ResponseHandler<?> chgPwdByVerifyKey(@Valid @RequestBody(required=false) ChangePasswordByCodeReq req) {
		ResponseHandler<?> result = aManagerService.updateManagerPassword(req);
		return result;
	}

	/**
	 *  관리자(컨설턴트) 수정
	 */
	@ApiOperation(value = "컨설턴트 수정")
	@PostMapping(value = "/edit")
	@SessionManagerCheck
	public ResponseHandler<?> update(@Valid ManagerUpdateReq req) {
		ResponseHandler<?> result = aManagerService.updateManager(req);
		return result;
	}

	/**
	 * 관리자(컨설턴트) 상세
	 *
	 * @param : 사용자에서 받아온 저장값
	 * @return : ReturnType
	 */
	@ApiOperation(value = "컨설턴트 상세")
	@GetMapping(value="/item")
	public ResponseHandler<ManagerDetailRes> info(ManagerDetailReq req) {
		ResponseHandler<ManagerDetailRes> result = aManagerService.selectManagerDetail(req);
		return  result;
	}

	/**
	 * 컨설턴트 삭제
	 *
	 * @param : 사용자에서 받아온 저장값
	 * @return : ReturnType
	 */
	@ApiOperation(value = "컨설턴트 삭제")
	@DeleteMapping
	@SessionManagerCheck
	public ResponseHandler<?> delete(@Valid @RequestBody(required = false) final ManagerDeleteReq req) {
		ResponseHandler<?> result = aManagerService.deleteManager(req);
		return  result;
	}

	/**
	 *  컨설턴트 매칭 회원 조회
	 *
	 * @param : MatchingUserListReq
	 * @return : ResponseHandler<MatchingUserListRes>
	 */
	@ApiOperation(value = "컨설턴트 매칭 회원 조회")
	@GetMapping(value = "/matching/user")
	public ResponseHandler<MatchingUserListRes> selectMatchingUser(@Valid MatchingUserListReq req) {
		ResponseHandler<MatchingUserListRes> result = aManagerService.selectMatchingUser(req);

		return  result;
	}

	/**
	 * 고객관리 컨설턴트 목록
	 *
	 * @return :
	 */
	@ApiOperation(value = "고객관리 컨설턴트 목록")
	@GetMapping(value = "/matching")
	public ResponseHandler<ManagerForMatchingListRes> selectManagerForMatching(@Valid ManagerForMatchingReq req) {
		ResponseHandler<ManagerForMatchingListRes> result = aManagerService.selectManagerForMatching(req);

		return  result;
	}

}
