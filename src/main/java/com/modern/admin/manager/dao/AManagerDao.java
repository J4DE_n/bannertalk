package com.modern.admin.manager.dao;

import com.modern.admin.manager.dto.model.ManagerMatchingVO;
import com.modern.admin.manager.dto.model.MatchingVO;
import com.modern.admin.manager.dto.req.MatchingUserListReq;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import javax.validation.Valid;
import java.util.List;

@Repository
@Mapper
public interface AManagerDao
{
    List<ManagerMatchingVO> selectMatchingUser(@Valid MatchingUserListReq req) throws Exception;
    int selectMatchingUserCnt(@Valid MatchingUserListReq req) throws Exception;

    List<MatchingVO> selectManagerForMatching(@Valid MatchingVO req) throws Exception;
}
