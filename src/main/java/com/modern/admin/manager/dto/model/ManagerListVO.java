package com.modern.admin.manager.dto.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@ApiModel(value ="컨설턴트 VO")
public class ManagerListVO implements Serializable {
    private int rowNum;
    private int userSeq;
    private int auth;
    private String pendingYn;
    private String userId;
    private String userNm;
    private String gender;
    private String birthday;
    private String tel;
    private int matchingCnt;
    private String delYn;
    private Date creDt;
    private Date loginLasttime;
}