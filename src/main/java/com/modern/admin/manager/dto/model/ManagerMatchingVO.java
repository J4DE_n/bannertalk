package com.modern.admin.manager.dto.model;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@ApiModel(value ="컨설턴트 목록 VO")
public class ManagerMatchingVO implements Serializable {
    private Integer managerUserSeq;
    private Integer userSeq;
    private String userId;
    private String userNm;
    private String matchYn;
    private Date matchDt;
    private Date creDt;
}