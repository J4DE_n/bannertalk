package com.modern.admin.manager.dto.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="컨설턴트 리스트 VO")
public class ManagerVO implements Serializable {
    private int userSeq;
    private String pendingYn;
    private String userId;
    private String userNm;
    private String gender;
    private String rangeService;
    private String rangeTarget;
    private String region;
    private String birthday;
    private String tel;
    private int matchingCnt;
}