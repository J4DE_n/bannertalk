package com.modern.admin.manager.dto.req;

import com.modern.common.dto.req.ManagerCommonReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="컨설턴트 삭제")
public class ManagerDeleteReq extends ManagerCommonReq implements Serializable {
    @ApiParam(value = "컨설턴트 시퀀스 리스트")
    private Integer[] userSeqList;
}