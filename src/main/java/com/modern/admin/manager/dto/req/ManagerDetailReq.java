package com.modern.admin.manager.dto.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel(value ="컨설턴트 상세")
public class ManagerDetailReq implements Serializable {
    @ApiParam(value = "컨설턴트 시퀀스", required = true)
    @NotNull
    private int userSeq;
}