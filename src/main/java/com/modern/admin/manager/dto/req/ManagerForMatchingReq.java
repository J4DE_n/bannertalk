package com.modern.admin.manager.dto.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.modern.common.dto.req.ManagerPagingReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="매칭을 위한 컨설턴트 목록 조회")
public class ManagerForMatchingReq extends ManagerPagingReq implements Serializable {
    @ApiParam(value = "회원 번호", hidden = true)
    @JsonIgnore
    private Integer userSeq;

}