package com.modern.admin.manager.dto.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.modern.common.dto.req.ManagerPagingReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="컨설턴트 목록")
public class ManagerListReq extends ManagerPagingReq implements Serializable {

    @ApiParam(value = "검색어")
    private String searchText;

    @ApiParam(value = "권한")
    private Integer auth;

    @ApiParam(value = "성별")
    private String gender;

    @ApiParam(value = "사용자 권한", hidden = true)
    @JsonIgnore
    private Integer userAuth;

}