package com.modern.admin.manager.dto.req;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.modern.common.dto.req.ManagerCommonReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel(value ="컨설턴트 등록")
public class ManagerUpdateReq extends ManagerCommonReq implements Serializable {
    @ApiParam(value = "유저 시퀀스", required = true)
    @NotNull
    private Integer userSeq;
    @ApiParam(value = "아이디", required = true)
    private String userId;
    @ApiParam(value = "권한", hidden = true)
    private Integer auth;
    @ApiParam(value = "이름", required = true)
    private String userNm;
    @ApiParam(value = "영어이름")
    private String engNm;
    @ApiParam(value = "전화번호", required = true)
    private String tel;
    @ApiParam(value = "생일", required = true)
    private String birthday;
    @ApiParam(value = "성별", required = true)
    private String gender;
    @ApiParam(value = "카카오아이디", required = true)
    private String kakaoId;
    @ApiParam(value = "소개")
    private String intro;
    @ApiParam(value = "인사말")
    private String greeting;
    @ApiParam(value = "프로필 이미지 물리 파일")
    private MultipartFile file;
    @ApiParam(value = "프로필 이미지")
    @JsonIgnore
    private Integer fileGrpSeq;
    @ApiParam(value = "브랜딩1")
    private String branding1;
    @ApiParam(value = "브랜딩2")
    private String branding2;
    @ApiParam(value = "브랜딩3")
    private String branding3;
    @ApiParam(value = "주요이력")
    private String career;
    @ApiParam(value = "관리대상")
    private String rangeTarget;
    @ApiParam(value = "관리항목")
    private String rangeService;
    @ApiParam(value = "활동지역")
    private String region;
    @ApiParam(value = "가능언어")
    private String language;
    @ApiParam(value = "제약여부")
    private String pendingYn;
    @ApiParam(value = "제약사유")
    private String pendingReason;
}