package com.modern.admin.manager.dto.req;

import com.modern.common.dto.req.ManagerPagingReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="컨설턴트 매칭 유저 목록")
public class MatchingUserListReq extends ManagerPagingReq implements Serializable {
    @ApiParam(value = "권한")
    private Integer managerUserSeq;
}