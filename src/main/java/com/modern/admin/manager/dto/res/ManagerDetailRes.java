package com.modern.admin.manager.dto.res;

import com.modern.comCode.dto.model.AddrItemVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(value = "컨설턴트 상세")
public class ManagerDetailRes implements Serializable {
    @ApiParam(value = "컨설턴트 시퀀스", required = true)
    @NotNull
    private Integer userSeq;

    @ApiParam(value = "권한")
    private int auth;

    @ApiParam(value = "아이디")
    private String userId;

    @ApiParam(value = "이름")
    private String userNm;

    @ApiParam(value = "영어 이름")
    private String engNm;

    @ApiParam(value = "성별")
    private String gender;

    @ApiParam(value = "생년월일")
    private String birthday;

    @ApiParam(value = "전화번호")
    private String tel;

    @ApiParam(value = "카카오 아이디")
    private String kakaoId;

    @ApiParam(value = "한 줄 소개")
    private String intro;

    @ApiParam(value = "간단한 인사말")
    private String greeting;

    @ApiParam(value = "강점 분야1")
    private String branding1;

    @ApiParam(value = "강점 분야2")
    private String branding2;

    @ApiParam(value = "강점 분야3")
    private String branding3;

    @ApiParam(value = "주요 이력")
    private String career;

    @ApiParam(value = "가능 언어")
    private String language;

    @ApiParam(value = "가능 대상")
    private String rangeTarget;

    @ApiParam(value = "가능 서비스")
    private String rangeService;

    @ApiParam(value = "활동지역")
    private String region;

    @ApiParam(value = "제한여부")
    private String pendingYn;

    @ApiParam(value = "제한사유")
    private String pendingReason;

    @ApiParam(value = "시스템 작성일")
    private String creDt;

    @ApiParam(value = "가능 지역")
    private List<AddrItemVO> addrList;

    @ApiParam(value = "프로필 파일 경로")
    private String filePath;

    @ApiParam(value = "프로필 파일 이름")
    private String orgFileNm;

    @ApiParam(value = "프로필 파일 고유번호")
    private String fileSeq;
}
