package com.modern.admin.manager.dto.res;

import com.modern.admin.manager.dto.model.MatchingVO;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "컨설턴트 목록")
public class ManagerForMatchingListRes {
    private List<MatchingVO> list;
}
