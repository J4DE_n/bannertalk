package com.modern.admin.manager.dto.res;

import com.modern.admin.manager.dto.model.ManagerListVO;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "컨설턴트 목록")
public class ManagerListRes {
    private List<ManagerListVO> managerList;

    /*현재 페이지*/
    private int currentPage;
    private int totalCnt;
}
