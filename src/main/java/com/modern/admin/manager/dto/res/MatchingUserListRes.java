package com.modern.admin.manager.dto.res;

import com.modern.admin.manager.dto.model.ManagerMatchingVO;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "매칭 유저 목록")
public class MatchingUserListRes {
    private List<ManagerMatchingVO> list;

    /*현재 페이지*/
    private int currentPage;
    private int totalCnt;
}
