package com.modern.admin.manager.service;

import com.modern.admin.manager.dao.AManagerDao;
import com.modern.admin.manager.dto.model.ManagerListVO;
import com.modern.admin.manager.dto.model.ManagerMatchingVO;
import com.modern.admin.manager.dto.model.MatchingVO;
import com.modern.admin.manager.dto.req.*;
import com.modern.admin.manager.dto.res.ManagerDetailRes;
import com.modern.admin.manager.dto.res.ManagerForMatchingListRes;
import com.modern.admin.manager.dto.res.ManagerListRes;
import com.modern.admin.manager.dto.res.MatchingUserListRes;
import com.modern.comCode.dto.req.AddrItemReq;
import com.modern.comCode.dto.res.AddrItemRes;
import com.modern.comCode.service.ComService;
import com.modern.common.config.ConfigCommon;
import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.type.ReturnType;
import com.modern.common.util.ComEncDecUtil;
import com.modern.common.util.CommonUtil;
import com.modern.membership.config.ConfigMembership;
import com.modern.membership.dao.ManagerDao;
import com.modern.membership.dao.MembershipDao;
import com.modern.membership.dto.model.ManagerInfoVO;
import com.modern.membership.dto.model.UserInfoVO;
import com.modern.membership.dto.req.ChangePasswordByCodeReq;
import com.modern.membership.dto.req.UserInfoReq;
import com.modern.membership.service.ManagerService;
import com.modern.membership.service.MembershipService;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;


@Service
public class AManagerService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ManagerDao managerDao;

    @Autowired
    private AManagerDao aManagerDao;

    @Autowired
    private ManagerService managerService;

    @Autowired
    private MembershipService membershipService;

    @Autowired
    private ComService comService;

    @Autowired
    private ConfigMembership configMembership;

    @Autowired
    private MembershipDao membershipDao;

    @Autowired
    private ConfigCommon configCommon;



    /**
     * 컨설턴트 등록 및 초대
     * @param req
     * @return
     */
    public ResponseHandler<?> insertManager(ManagerInsertReq req, HttpServletRequest request, HttpServletResponse response) {
        final ResponseHandler<?> result = new ResponseHandler<>();
        try {
            //1. 컨설턴트 기본 정보 저장
            ManagerInfoVO managerInfoVO = new ManagerInfoVO();
            BeanUtils.copyProperties(managerInfoVO, req);
            managerInfoVO.setCertificateYn("N");
            ManagerInfoVO memberInfo = managerService.insertManagerInfo(managerInfoVO);

            if(!memberInfo.getCode().equals(ReturnType.RTN_TYPE_OK)){
                result.setReturnCode(memberInfo.getCode());
                return result;
            }

            //2. 비밀번호 설정 메일 발송
            UserInfoVO userInfoVO = new UserInfoVO();
            userInfoVO.setUserId(req.getUserId());
            userInfoVO.setUserSeq(memberInfo.getUserSeq());

            ReturnType mailReturnType = managerService.sendMailManagerPwdSetting(userInfoVO);
            if(!mailReturnType.equals(ReturnType.RTN_TYPE_OK)){
                result.setReturnCode(ReturnType.RTN_TYPE_EMAIL_SENDING_NG);
                return result;
            }

            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            logger.error("[insertManager Exception]", e);
        }
        return result;
    }

    /**
     * 컨설턴트 수정
     * @param req
     * @return
     */
    public ResponseHandler<?> updateManager(@Valid ManagerUpdateReq req) {
        final ResponseHandler<?> result = new ResponseHandler<>();
        try {
            //1. 컨설턴트 기본 정보 업데이트
            ManagerInfoVO managerInfoVO = new ManagerInfoVO();
            BeanUtils.copyProperties(managerInfoVO, req);
            ManagerInfoVO memberInfo = managerService.updateManagerInfo(managerInfoVO);

            if(!memberInfo.getCode().equals(ReturnType.RTN_TYPE_OK)){
                result.setReturnCode(memberInfo.getCode());
                return result;
            }

            //2. 제약유저 업데이트
            UserInfoVO userInfoVO = new UserInfoVO();
            userInfoVO.setPendingYn(req.getPendingYn());
            userInfoVO.setPendingReason(req.getPendingReason());
            userInfoVO.setUserSeq(req.getUserSeq());
            userInfoVO.setAuth(req.getAuth());
            membershipService.UpdateUser(userInfoVO);

            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            logger.error("[insertManager Exception]", e);
        }
        return result;
    }

    /**
     * 컨설턴트 비밀번호 변경
     * @param req
     * @return
     */
    public ResponseHandler<?> updateManagerPassword(@Valid ChangePasswordByCodeReq req) {
        final ResponseHandler<?> result = new ResponseHandler<>();
        try {
            ResponseHandler<?> updPwd = membershipService.changePwdByVerifyKey(req);
            if(updPwd.getCode().equals(ReturnType.RTN_TYPE_OK.getValue())){

                UserInfoReq userReq = new UserInfoReq();
                String userId = ComEncDecUtil.getDecrypted(req.getUserId(), configCommon.getAes128Key());
                userReq.setUserId(userId);
                userReq.setAuth(configMembership.getSelectAuthorityUserNo());
                userReq.setIsManagerAuth(req.getIsManagerAuth());
                UserInfoVO userInfoVO = membershipDao.selectUser(userReq);

                if(CommonUtil.isEmpty(userInfoVO.getCertificateYn()) || !userInfoVO.getCertificateYn().equals("Y")){
                    UserInfoVO updReq = new UserInfoVO();
                    updReq.setVerifyPwdKey("");
                    updReq.setCertificateYn("Y");
                    updReq.setUserSeq(userInfoVO.getUserSeq());
                    ResponseHandler<?> updateUser = membershipService.UpdateUser(updReq);
                    if(updateUser.getCode().equals(ReturnType.RTN_TYPE_OK.getValue())){
                        result.setReturnCode(ReturnType.RTN_TYPE_OK);
                    } else {
                        result.setReturnCode(ReturnType.RTN_TYPE_NG);
                    }
                    return result;
                }

                result.setReturnCode(ReturnType.RTN_TYPE_OK);
            } else {
                result.setReturnCode(ReturnType.RTN_TYPE_NG);
            }
        } catch (Exception e) {
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            logger.error("[insertManager Exception]", e);
        }
        return result;
    }

    /**
     * 컨설턴트 목록
     * @param req
     * @return
     */
    public ResponseHandler<ManagerListRes> selectManager(ManagerListReq req) {
        logger.info("selectManager[req]" + req);
        final ResponseHandler<ManagerListRes> result = new ResponseHandler<>();
        try {
            req.setStartRow();
            req.setUserAuth(configMembership.getSelectAuthorityUserNo());
            List<ManagerListVO> list = managerDao.selectManager(req);
            int totalCnt = managerDao.selectManagerCnt(req);
            ManagerListRes managerListRes = new ManagerListRes();
            managerListRes.setManagerList(list);
            managerListRes.setTotalCnt(totalCnt);
            if(totalCnt == 0) {
                result.setReturnCode(ReturnType.RTN_TYPE_NO_DATA);
                logger.info("selectManager[result]" + result);
                return result;
            }
            result.setData(managerListRes);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
            logger.info("selectManager[result]" + result);
        } catch (Exception e){
            logger.error("selectManager[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }
        return result;
    }

    /**
     * 컨설턴트 상세
     * @param req
     * @return
     */
    public ResponseHandler<ManagerDetailRes> selectManagerDetail(ManagerDetailReq req) {
        logger.info("managerDetail[req]" + req);
        final ResponseHandler<ManagerDetailRes> result = new ResponseHandler<>();
        ManagerDetailRes res = new ManagerDetailRes();

        try{
            //매니저 정보
            ManagerInfoVO managerInfoVO = new ManagerInfoVO();
            managerInfoVO.setUserSeq(req.getUserSeq());
            ManagerInfoVO info = managerDao.selectManagerInfo(managerInfoVO);

            if(info != null){
                BeanUtils.copyProperties(res, info);
                result.setData(res);
                result.setReturnCode(ReturnType.RTN_TYPE_OK);
            } else {
                result.setReturnCode(ReturnType.RTN_TYPE_NO_DATA);
            }
            logger.info("managerDetail[result]" + result);
        } catch (Exception e){
            logger.error("managerDetail[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }
        return result;
    }

    /**
     * 컨설턴트 삭제
     * @param req
     * @return
     */
    public ResponseHandler<?> deleteManager(ManagerDeleteReq req) {
        final ResponseHandler<?> result = new ResponseHandler<>();
        try {
            if(req.getUserSeqList().length > 0){
                for(Integer seq : req.getUserSeqList()){
                    UserInfoVO userInfoVO = new UserInfoVO();
                    userInfoVO.setUserSeq(seq);
                    userInfoVO.setDelYn("Y");
                    userInfoVO.setUpdId(req.getSessionSeq());
                    membershipDao.updateUser(userInfoVO); //user 업데이트

                    ManagerInfoVO managerInfoVO = new ManagerInfoVO();
                    managerInfoVO.setUserSeq(seq);
                    managerInfoVO.setDelYn("Y");
                    managerInfoVO.setSessionSeq(req.getSessionSeq());
                    managerDao.updateManagerDelYn(managerInfoVO); //관리자 업데이트
                }
            }
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 매칭 회원 정보
     * @param req
     * @return
     */
    public ResponseHandler<MatchingUserListRes> selectMatchingUser(@Valid MatchingUserListReq req) {
        final ResponseHandler<MatchingUserListRes> result = new ResponseHandler<>();
        try {
            req.setStartRow();
            List<ManagerMatchingVO> list = aManagerDao.selectMatchingUser(req);
            int totalCnt = aManagerDao.selectMatchingUserCnt(req);
            MatchingUserListRes res = new MatchingUserListRes();

            if(list.size() == 0){
                result.setReturnCode(ReturnType.RTN_TYPE_NO_DATA);
            } else {
                res.setList(list);
                result.setReturnCode(ReturnType.RTN_TYPE_OK);
            }
            res.setTotalCnt(totalCnt);
            result.setData(res);
        } catch (Exception e) {
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            logger.error("[selectMatchingUser Exception]", e);
        }

        return result;
    }

    /**
     * 매칭을 위한 컨설턴트 조회
     * @param req
     * @return
     */
    public ResponseHandler<ManagerForMatchingListRes> selectManagerForMatching(@Valid ManagerForMatchingReq req) {
        final ResponseHandler<ManagerForMatchingListRes> result = new ResponseHandler<>();
        try {
            MatchingVO matchingVO = new MatchingVO();
            BeanUtils.copyProperties(matchingVO, req);
            List<MatchingVO> list = aManagerDao.selectManagerForMatching(matchingVO);
            ManagerForMatchingListRes res = new ManagerForMatchingListRes();

            if(list.size() == 0){
                result.setReturnCode(ReturnType.RTN_TYPE_NO_DATA);
            } else {
                res.setList(list);
                result.setReturnCode(ReturnType.RTN_TYPE_OK);
            }
            result.setData(res);
        } catch (Exception e) {
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            logger.error("[selectMatchingUser Exception]", e);
        }

        return result;
    }
}

