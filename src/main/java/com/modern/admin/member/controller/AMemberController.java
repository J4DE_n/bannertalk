package com.modern.admin.member.controller;

import com.modern.admin.member.dto.req.MemberListReq;
import com.modern.admin.member.dto.res.MemberListRes;
import com.modern.admin.member.service.AMemberService;
import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.file.config.ConfigFile;
import com.modern.common.file.service.FileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


/**
 *  멤버쉽 Rest Controller class
 *
 *  사용자를 제어하기 위한 REST API가 정의 되어 있다.
 *
 *
 */
@RestController
@RequestMapping(value="/mapi/member" ) //모듈명 정의
@Api(value = "AMemberController", description = "고객 API")
public class AMemberController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	AMemberService aMemberService;

	@Autowired
    FileService fileService;

	@Autowired
    ConfigFile configFile;

	/**
	 * 사용자 목록
	 *
	 *
	 * @param : 사용자에서 받아온 저장값
	 * @return : ReturnType
	 */
	@ApiOperation(value = "사용자 목록")
	@GetMapping
	public ResponseHandler<MemberListRes> list(@Valid MemberListReq req) { //valid 추가
		ResponseHandler<MemberListRes> result = aMemberService.selectMember(req);

		return  result;
	}
	
	/**
	 *  사용자 저장
	 */
	/*@ApiOperation(value = "고객 변화 등록")
	@PostMapping
	@SessionCheck
	public ResponseHandler<?> save(ChangeInsertReq req) {
		ResponseHandler<?> result = changeService.insertChange(req);
		return result;
	}*/



	/**
	 * 사용자 삭제
	 *
	 * @param : 사용자에서 받아온 저장값
	 * @return : ReturnType
	 */
	/*@ApiOperation(value = "고객 변화 삭제")
	@DeleteMapping
	@SessionCheck
	public ResponseHandler<?> delete(@Valid @RequestBody(required = false) final ChangeDeleteReq req) {
		ResponseHandler<?> result = changeService.deleteChange(req);
		return  result;
	}*/


	/**
	 * 고객 변화 상세
	 *
	 * @param : 사용자에서 받아온 저장값
	 * @return : ReturnType
	 */
	/*@ApiOperation(value = "고객 변화 상세")
	@GetMapping(value="/item")
	public ResponseHandler<ChangeInfoRes> info(ChangeInfoReq req) {
		ResponseHandler<ChangeInfoRes> result = changeService.selectChangeInfo(req);
		return  result;
	}*/
}
