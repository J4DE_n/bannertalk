package com.modern.admin.member.dao;

import com.modern.admin.member.dto.model.MemberListVO;
import com.modern.admin.member.dto.req.MemberListReq;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface AMemberDao
{
    /*void insertChange(ChangeInsertReq req);
    void insertChange(ChangeInsertReq req);

    void updateChange(Map<String, Object> params);

    ChangeInfoRes selectChangeInfo(ChangeInfoReq req);

    List<ChangeVO> selectChange(ChangeListReq req);

    int selectChangeCnt(ChangeListReq req);

    void deleteChange(ChangeDeleteReq req);*/

    List<MemberListVO> selectMember(MemberListReq req);

    int selectMemberCnt(MemberListReq req);

    /*MemberInfoRes selectMemberInfo(MemberInfoReq req);*/

}
