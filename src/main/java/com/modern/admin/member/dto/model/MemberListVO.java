package com.modern.admin.member.dto.model;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="사용자 리스트 VO")
public class MemberListVO implements Serializable {
    private int rowNum;
    private int userSeq;
    private int auth;
    private String userNm;
    private String userId;
    private String kakaoIdExistYn;
    private String kakaoId;
    private String birthday;
    private String addrMajor;
    private String addrMinor;
    private String gender;
    private String creDt;
}