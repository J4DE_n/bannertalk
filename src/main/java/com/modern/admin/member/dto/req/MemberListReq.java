package com.modern.admin.member.dto.req;

import com.modern.common.dto.req.PagingReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="사용자 목록")
public class MemberListReq extends PagingReq implements Serializable {

    @ApiParam(value = "검색어")
    private String searchText;

    @ApiParam(value = "광역시도")
    private String addrMajor;

    @ApiParam(value = "군구")
    private String addrMinor;

    @ApiParam(value = "성별")
    private String gender;

    @ApiParam(value = "시작일")
    private String startDt;

    @ApiParam(value = "종료일")
    private String endDt;


}