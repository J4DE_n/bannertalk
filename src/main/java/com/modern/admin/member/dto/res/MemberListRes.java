package com.modern.admin.member.dto.res;

import com.modern.admin.member.dto.model.MemberListVO;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "사용자 목록 정보")
public class MemberListRes {
    private List<MemberListVO> memberList; //요기에 담는거죠

    /*현재 페이지*/
    private int currentPage;
    private int totalCnt;
}
