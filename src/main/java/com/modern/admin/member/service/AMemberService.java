package com.modern.admin.member.service;

import com.modern.admin.member.dao.AMemberDao;
import com.modern.admin.member.dto.model.MemberListVO;
import com.modern.admin.member.dto.req.MemberListReq;
import com.modern.admin.member.dto.res.MemberListRes;
import com.modern.comCode.service.ComService;
import com.modern.common.config.ConfigCommon;
import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.type.ReturnType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class AMemberService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    AMemberDao aMemberDao;

    @Autowired
    private ComService comService;

    @Autowired
    private ConfigCommon configCommon;

    /**
     * 고객 변화 등록
     * @param req
     * @return
     */
    /*public ResponseHandler<?> insertChange(ChangeInsertReq req, MultipartFile[] files) {
        final ResponseHandler<?> result = new ResponseHandler<>();
        try {


            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }
        return result;
    }*/


/*
    *//* 고객 변화 저장 *//*
    public ResponseHandler<?> insertChange(ChangeInsertReq req) {
        final ResponseHandler<?> result = new ResponseHandler<>();
        try {
            changeDao.insertChange(req);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }
        return result;
    }

    *//* 고객 변화 삭제 *//*
    public ResponseHandler<?> deleteChange(ChangeDeleteReq req) {
        final ResponseHandler<?> result = new ResponseHandler<>();
        try {

            *//* 다중 선택 삭제 *//*
            for(int index=0; index < req.getChangeSeqList().size(); index++){
                req.setChgSeq(Integer.parseInt((String) req.getChangeSeqList().get(index)));
                changeDao.deleteChange(req);
            }

            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }
        return result;
    }*/

    /**
     * 사용자 목록
     * @param req
     * @return
     */
    public ResponseHandler<MemberListRes> selectMember(MemberListReq req) {
        logger.info("selectMember[req]" + req);
        final ResponseHandler<MemberListRes> result = new ResponseHandler<>();
        req.setStartRow();
        try {
            List<MemberListVO> list = aMemberDao.selectMember(req);
            int totalCnt = aMemberDao.selectMemberCnt(req);
            MemberListRes memberListRes = new MemberListRes();
            memberListRes.setMemberList(list);
            memberListRes.setTotalCnt(totalCnt);
            if(totalCnt == 0) {
                result.setReturnCode(ReturnType.RTN_TYPE_NO_DATA);
                logger.info("selectMember[result]" + result);
                return result;
            }
            result.setData(memberListRes);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
            logger.info("selectMember[result]" + result);
        } catch (Exception e){
            logger.error("selectMember[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }
        return result;
    }


    /**
     * 컨설턴트 상세
     * @param req
     * @return
     */
    /*public ResponseHandler<MemberInfoRes> selectMemberInfo(ManagerInfoReq req) {
        logger.info("managerInfo[req]" + req);
        final ResponseHandler<ManagerInfoRes> result = new ResponseHandler<>();

        try{
            ManagerInfoRes managerInfoRes = managerDao.selectManagerInfo(req);
            result.setData(managerInfoRes);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
            logger.info("managerInfo[result]" + result);
        } catch (Exception e){
            logger.error("managerInfo[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }
        return result;
    }*/
}

