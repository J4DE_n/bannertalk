/*
package com.modern.admin.membership.controller;

import com.modern.admin.membership.service.ALoginService;
import com.modern.common.dto.res.ResponseHandler;
import com.modern.membership.dto.req.LoginReq;
import com.modern.membership.service.JwtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@RestController
public class ALoginController {

    @Autowired
	ALoginService aLoginService;

    @Autowired
    JwtService jwtService;

	@PostMapping(value = "/logout")
	public ResponseHandler logout(HttpServletResponse response) {
		ResponseHandler result = aLoginService.logout(response);

		return result;
	}

	@PostMapping(value = "/admin/login")
	public ResponseHandler adminLoginOauth(@Valid @RequestBody(required = false) final LoginReq req, HttpServletResponse response, HttpSession session) {
		ResponseHandler result = aLoginService.adminLoginOauth(req, response, session);

		return result;
	}
}
*/
