/*
package com.modern.admin.membership.service;

import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.password.PasswordHandler;
import com.modern.common.type.ReturnType;
import com.modern.common.util.ComStaticUtil;
import com.modern.common.util.CommonUtil;
import com.modern.common.util.CookieUtil;
import com.modern.common.util.SmsUtil;
import com.modern.membership.config.ConfigMembership;
import com.modern.membership.dao.MembershipDao;
import com.modern.membership.dto.model.UserInfoVO;
import com.modern.membership.dto.req.LoginReq;
import com.modern.membership.dto.req.UserInfoReq;
import com.modern.membership.dto.res.SessionRes;
import com.modern.membership.service.JwtService;
import com.modern.membership.service.MembershipService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@Service
public class ALoginService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    MembershipDao membershipDao;

    @Autowired
    MembershipService membershipService;

    @Autowired
    ConfigMembership configMembership;

    @Autowired
    PasswordHandler passwordHandler;

    @Autowired
    JwtService jwtService;

    public ReturnType updateOauth(HttpServletRequest request, HttpServletResponse response) {
        try {

            //1. 현재 세션정보 조회
            ResponseHandler<SessionRes> sessionInfo = membershipService.getSessionInfo(request, response);
            int userSeq = sessionInfo.getData().getUserSeq();
            //2. 자동저장 여부 쿠키 조회
            String forever = CookieUtil.getCookie(request,"TFO" );

            //3. 유저정보 조회
            UserInfoReq infoReq = new UserInfoReq();
            infoReq.setUserSeq(userSeq);
            UserInfoVO userInfoVO = membershipDao.selectSessionInfo(infoReq);
            if (CommonUtil.isNotEmpty(userInfoVO)) {
                //4. 쿠키 삭제
                jwtService.removeSession(response);
                int expire = 0;
                if((CommonUtil.isNotEmpty(forever) && "Y".equals(forever))) {
                    expire = configMembership.getCookie().getExpire();
                    CookieUtil.setCookie(response, "TFO", forever, ComStaticUtil.requestDomain, expire);
                } else {
                    expire = -1;
                }

                //5. 쿠키 다시 굽기
                Map<String, Object> map = new HashMap<>();
                map.put("userSeq", userInfoVO.getUserSeq());
                map.put("userId", userInfoVO.getUserId());
                //map.put("userName", userInfoVO.getUserName());
                map.put("authority", userInfoVO.getAuth());
                String jwtString = jwtService.createJwtBuilder(map);

                //쿠키 저장
                jwtService.setJwtCookie(jwtString, response, expire, userInfoVO.getUserSeq());
                return ReturnType.RTN_TYPE_OK;
            } else {
                return ReturnType.RTN_TYPE_MEMBERSSHIP_USER_EXIST_NG;
            }
        } catch(Exception e) {
            logger.error("updateOauth [Exception]", e);
            return ReturnType.RTN_TYPE_NG;
        }
    }

    public ResponseHandler<SessionRes> loginOauth(LoginReq req, HttpServletResponse response) {
        ResponseHandler<SessionRes> result = new ResponseHandler();
        SessionRes sessionRes = new SessionRes();
        try {

            UserInfoReq userReq = new UserInfoReq();
            userReq.setUserId(req.getUserId());
            UserInfoVO userInfoVO = membershipDao.selectSessionInfo(userReq);
            if(ObjectUtils.isEmpty(userInfoVO)){
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_USER_NO_EXIST_NG);
                return result;
            }

            String pwd = req.getUserPwd();
            String encPassword = userInfoVO.getUserPwd();

            if(!passwordHandler.matches(pwd, encPassword)) {
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_PASSWORD_ENC_NG);
                return result;
            }

            if(!"Y".equals(userInfoVO.getCertificateYn())) {
                sessionRes.setUserId(userInfoVO.getUserId());
                result.setData(sessionRes);
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_EMAIL_UNVERIFY);
                return result;
            }

            Map<String, Object> map = new HashMap<>();
            map.put("userSeq", userInfoVO.getUserSeq());
            map.put("userId", userInfoVO.getUserId());
            //map.put("userName", userInfoVO.getUserName());
            map.put("authority", userInfoVO.getAuth());
            //map.put("classCd", userInfoVO.getClassCd());
            //map.put("myRecommendCd", userInfoVO.getMyRecommendCd());
            //map.put("tel", userInfoVO.getTel());

            String jwtString = jwtService.createJwtBuilder(map);

            int expire = 0;
            if((CommonUtil.isNotEmpty(req.getForever()) && "Y".equals(req.getForever()))) {
                expire = configMembership.getCookie().getExpire();
                CookieUtil.setCookie(response, "TFO", req.getForever(), ComStaticUtil.requestDomain, expire);
            } else {
                expire = -1;
            }

            //쿠키 저장
            jwtService.setJwtCookie(jwtString, response, expire, userInfoVO.getUserSeq());

            //BeanUtils.copyProperties(sessionRes, userInfoVO);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);

        } catch(Exception e) {
            logger.error("loginOauth[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            return result;
        }

        return result;
    }

    public ResponseHandler logout(HttpServletResponse response) {
        ResponseHandler result = new ResponseHandler();
        try {

            jwtService.removeSession(response);

            result.setReturnCode(ReturnType.RTN_TYPE_OK);

        } catch(Exception e) {
            logger.error("logout[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            return result;
        }

        return result;
    }

    public ResponseHandler adminLoginOauth(@Valid LoginReq req, HttpServletResponse response, HttpSession session) {
        ResponseHandler result = new ResponseHandler();
        try {

            UserInfoReq userReq = new UserInfoReq();
            userReq.setUserId(req.getUserId());
            UserInfoVO userInfoVO = membershipDao.selectUserInfo(userReq);

            if(ObjectUtils.isEmpty(userInfoVO)){
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_USER_NO_EXIST_NG);
                return result;
            }

            String pwd = req.getUserPwd();
            String encPassword = userInfoVO.getUserPwd();
            if(!passwordHandler.matches(pwd, encPassword)) {
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_PASSWORD_ENC_NG);
                return result;
            }

            int auth = userInfoVO.getAuth();

            Map<String, Object> map = new HashMap<>();
            map.put("userSeq", userInfoVO.getUserSeq());
            map.put("userId", userInfoVO.getUserId());
            //map.put("userName", userInfoVO.getUserName());
            map.put("authority", auth );
            //map.put("myRecommendCd", userInfoVO.getMyRecommendCd());
            //map.put("tel", userInfoVO.getTel());

            //System.out.println("authPro : "+ configMembership.getSelectAuthoritySuperAdminNo());
            //System.out.println("authAdmin : "+ configMembership.getSelectAuthorityAdminNo());
            //System.out.println("authCont : "+ configMembership.getSelectAuthorityContentsAdminNo());
            //System.out.println("authUser : "+ configMembership.getSelectAuthorityUserNo());
            //System.out.println("auth : "+ userInfoVO.getAuthority() );

            if( auth <= configMembership.getSelectAuthorityContentsAdminNo() ){
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_AUTHORITY_NG);
                return result;
            }

            String jwtString = jwtService.createJwtBuilder(map);

            int expire = 0;
            if((CommonUtil.isNotEmpty(req.getForever()) && "Y".equals(req.getForever()))) {
                expire = configMembership.getCookie().getExpire();
                CookieUtil.setCookie(response, "TFO", req.getForever(), ComStaticUtil.requestDomain, expire);
            } else {
                expire = -1;
            }

            //쿠키 저장
            jwtService.setJwtCookie(jwtString, response, expire, userInfoVO.getUserSeq());

            */
/* 관리자 로그인시 문자 보내기 위한 토큰 생성*//*

            SmsUtil.getTokenSignal();

            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch(Exception e) {
            logger.error("loginOauth[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            return result;
        }

        return result;
    }
}
*/
