package com.modern.admin.myAccount.controller;

import com.modern.admin.myAccount.dto.req.UpdateProfileReq;
import com.modern.admin.myAccount.dto.req.UpdatePwReq;
import com.modern.admin.myAccount.service.MyAccountService;
import com.modern.common.dto.req.CommonReq;
import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.file.config.ConfigFile;
import com.modern.common.file.service.FileService;
import com.modern.common.session.SessionCheck;
import com.modern.common.type.ReturnType;
import com.modern.membership.config.ConfigMembership;
import com.modern.membership.dto.req.*;
import com.modern.membership.dto.res.MyInfoRes;
import com.modern.membership.dto.res.SessionRes;
import com.modern.membership.dto.res.UserSignalRes;
import com.modern.membership.service.MembershipService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;


/**
 *  멤버쉽 Rest Controller class
 *
 *  사용자를 제어하기 위한 REST API가 정의 되어 있다.
 *
 *
 */
@RestController
@RequestMapping(value="/myAccount" )
public class MyAccountController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private MyAccountService myAccountService;

	/**
	 * 비밀번호 변경
	 *
	 * @param req
	 * @return
	 */
	@ApiOperation(value = "비밀번호 변경")
	@PostMapping(value = "/updatePw")
	@SessionCheck
	public ResponseHandler<?> updatePw(@Valid @RequestBody(required = false) UpdatePwReq req) {
		ResponseHandler<?> result = myAccountService.updatePw(req);
		return result;
	}

	/**
	 * 프로필 업데이트
	 * @param req
	 * @return
	 */
	@ApiOperation(value="프로필 업데이트")
	@PostMapping(value = "/updateProfile")
	@SessionCheck
	public ResponseHandler<?> updateProfile(@Valid @RequestBody(required=false) UpdateProfileReq req) {
		ResponseHandler<?> result = myAccountService.updateProfile(req);
		return result;
	}
}