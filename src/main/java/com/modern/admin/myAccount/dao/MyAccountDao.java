package com.modern.admin.myAccount.dao;

import com.modern.admin.myAccount.dto.req.UpdateProfileReq;
import com.modern.admin.myAccount.dto.req.UpdatePwReq;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import javax.validation.Valid;

@Repository
@Mapper
public interface MyAccountDao {

    int confirmPw(String pw);

    void updatePw(@Valid UpdatePwReq req);

    void updateProfile(@Valid UpdateProfileReq req);
}
