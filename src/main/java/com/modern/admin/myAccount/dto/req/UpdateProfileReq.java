package com.modern.admin.myAccount.dto.req;

import com.modern.common.dto.req.CommonReq;
import lombok.Data;

@Data
public class UpdateProfileReq extends CommonReq{
    private String tel;
    private String updName;
}
