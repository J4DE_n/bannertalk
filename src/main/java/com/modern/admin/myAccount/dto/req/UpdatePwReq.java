package com.modern.admin.myAccount.dto.req;

import com.modern.common.dto.req.CommonReq;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UpdatePwReq extends CommonReq{
    @NotNull
    private String passWord;
    @NotNull
    private String newPassword;
    private String newPasswordConfirm;
}
