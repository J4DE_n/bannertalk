package com.modern.admin.myAccount.service;

import com.modern.admin.myAccount.dao.MyAccountDao;
import com.modern.admin.myAccount.dto.req.UpdateProfileReq;
import com.modern.admin.myAccount.dto.req.UpdatePwReq;
import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.password.PasswordHandler;
import com.modern.common.type.ReturnType;
import com.modern.membership.dao.MembershipDao;
import com.modern.membership.dto.req.UserInfoReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Valid;

@Service
public class MyAccountService {

    @Autowired
    private MyAccountDao MyAccountDao;
    @Autowired
    private MembershipDao membershipDao;
    @Autowired
    private PasswordHandler passwordHandler;

    public ResponseHandler<?> updatePw(@Valid UpdatePwReq req) {
        final ResponseHandler<?> rs = new ResponseHandler<>();

        try {

            UserInfoReq userReq = new UserInfoReq();
            userReq.setUserId(req.getUserId());
            String encPassword = membershipDao.selectSessionInfo(userReq).getUserPwd();
            String pwd = req.getPassWord();
            if(!passwordHandler.matches(pwd, encPassword)) {
                rs.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_PASSWORD_ENC_NG);
                return rs;
            }

            PasswordHandler ph = new PasswordHandler();
            String enc = ph.encode(req.getNewPassword());
            req.setNewPassword(enc);

            MyAccountDao.updatePw(req);

            rs.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            rs.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }

        return rs;
    }

    public ResponseHandler<?> updateProfile(@Valid UpdateProfileReq req) {
        final ResponseHandler<?> rs = new ResponseHandler<>();

        try {

            MyAccountDao.updateProfile(req);

            rs.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            rs.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }

        return rs;
    }
}
