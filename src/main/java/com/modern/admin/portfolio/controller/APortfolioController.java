package com.modern.admin.portfolio.controller;

import com.modern.admin.portfolio.dto.req.*;
import com.modern.admin.portfolio.dto.res.PortfolioDetailRes;
import com.modern.admin.portfolio.dto.res.PortfolioListRes;
import com.modern.admin.portfolio.service.APortfolioService;
import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.session.SessionManagerCheck;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 *  포토폴리오 Rest Controller class
 *
 *  포토폴리오를 제어하기 위한 REST API가 정의 되어 있다.
 *
 */
@RestController
@RequestMapping(value="/mapi/portfolio" ) //모듈명 정의
@Api(value = "APortfolioController", description = "포트폴리오 API")
public class APortfolioController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private APortfolioService aPortfolioService;

	/**
	 * 포토폴리오 목록
	 */
	@ApiOperation(value = "포토폴리오 목록")
	@GetMapping
	public ResponseHandler<PortfolioListRes> selectPortfolio(@Valid PortfolioListReq req) {
		ResponseHandler<PortfolioListRes> result = aPortfolioService.selectPortfolioList(req);
		return  result;
	}

	/**
	 * 포토폴리오 상세
	 */
	@ApiOperation(value = "포토폴리오 상세")
	@GetMapping(value="/item")
	public ResponseHandler<PortfolioDetailRes> info(PortfolioDetailReq req) {
		ResponseHandler<PortfolioDetailRes> result = aPortfolioService.selectPortfolioDetail(req);
		return result;
	}

	/**
	 * 포토폴리오 등록
	 */
	@ApiOperation(value = "포토폴리오 등록")
	@PostMapping
	@SessionManagerCheck
	public ResponseHandler<?> insertPortfolio(@Valid PortfolioInsertReq req) {
		ResponseHandler<?> result = aPortfolioService.insertPortfolio(req);
		return  result;
	}

	/**
	 * 포토폴리오 수정
	 */
	@ApiOperation(value = "포토폴리오 수정")
	@PostMapping(value="/edit")
	@SessionManagerCheck
	public ResponseHandler<?> update(PortfolioUpdateReq req) {
		ResponseHandler<?> result = aPortfolioService.updatePortfolio(req);
		return  result;
	}

	/**
	 * 포토폴리오 삭제
	 */
	@ApiOperation(value = "포토폴리오 삭제")
	@DeleteMapping
	@SessionManagerCheck
	public ResponseHandler<?> delete(@Valid @RequestBody(required = false) PortfolioDeleteReq req) {
		ResponseHandler<?> result = aPortfolioService.deletePortfolio(req);
		return  result;
	}

}
