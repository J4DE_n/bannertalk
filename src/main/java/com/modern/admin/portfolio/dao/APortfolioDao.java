package com.modern.admin.portfolio.dao;

import com.modern.admin.portfolio.dto.model.PortfolioListVO;
import com.modern.admin.portfolio.dto.req.*;
import com.modern.admin.portfolio.dto.res.PortfolioDetailRes;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface APortfolioDao {
    List<PortfolioListVO> selectPortfolioList(PortfolioListReq req);
    int selectPortfolioListCnt(PortfolioListReq req);
    void insertPortfolio(PortfolioInsertReq req);
    PortfolioDetailRes selectPortfolioDetail(PortfolioDetailReq req);
    void updatePortfolio(PortfolioUpdateReq req);
    void deletePortfolio(PortfolioDeleteReq req);
}
