package com.modern.admin.portfolio.dto.model;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value ="포토폴리오 상세 VO")
public class PortfolioDetailVO {
    private int ptfSeq;             // 포토폴리오 시퀀스
    private String title;           // 제목
    private String summary;         // 설명
    private String displayYn;       // 노출 여부
    private String tnFileGrpSeq;    // 썸네일 파일 그룹 시퀀스
    private String detailFileGrpSeq;// 세부설명 파일 그룹 시퀀스
    private String writeDt;         // 작성일
    private int creId;              // 생성자
    private String creDt;           // 생성일
    private int updId;              // 수정자
    private String updDt;           // 수정일
}