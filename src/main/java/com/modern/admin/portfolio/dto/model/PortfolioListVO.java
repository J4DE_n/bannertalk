package com.modern.admin.portfolio.dto.model;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="포토폴리오 목록 VO")
public class PortfolioListVO implements Serializable {
    private int rowNum;         // 행번호
    private int ptfSeq;         // 포토폴리오 시퀀스
    private String title;       // 제목
    private String writeDt;     // 작성일
    private String displayYn;   // 노출 여부
    private String userId;      // 유저 아이디
    private String userNm;      // 유저 이름
}