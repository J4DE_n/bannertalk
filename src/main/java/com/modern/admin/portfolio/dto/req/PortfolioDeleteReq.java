package com.modern.admin.portfolio.dto.req;
import com.modern.common.dto.req.ManagerCommonReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="포토폴리오 삭제")
public class PortfolioDeleteReq extends ManagerCommonReq implements Serializable {
    @ApiParam(value = "포토폴리오 시퀀스")
    private Integer ptfSeq;

    @ApiParam(value = "포토폴리오 시퀀스 리스트")
    private Integer[] ptfSeqList;
}