package com.modern.admin.portfolio.dto.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel(value ="포토폴리오 상세 요청")
public class PortfolioDetailReq implements Serializable {
    @ApiParam(value = "포토폴리오 시퀀스", required = true)
    @NotNull
    private Integer ptfSeq;
}