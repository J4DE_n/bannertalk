package com.modern.admin.portfolio.dto.req;
import com.modern.common.dto.req.ManagerCommonReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

@Data
@ApiModel(value ="포토폴리오 등록 요청")
public class PortfolioInsertReq extends ManagerCommonReq implements Serializable {
    @ApiParam(value = "제목", required = true)
    private String title;

    @ApiParam(value = "요약 설명", required = true)
    private String summary;

    @ApiParam(value = "노출여부")
    private String displayYn;

    @ApiParam(value = "썸네일 파일 그룹 시퀀스")
    private Integer tnFileGrpSeq;

    @ApiParam(value = "상세설명 파일 그룹 시퀀스")
    private Integer detailFileGrpSeq;

    @ApiParam(value = "시스템 작성일")
    private String writeDt;

    @ApiParam(value = "썸네일 물리 파일")
    private MultipartFile tnFile;

    @ApiParam(value = "세부설명 물리 파일")
    private MultipartFile detailFile;
}