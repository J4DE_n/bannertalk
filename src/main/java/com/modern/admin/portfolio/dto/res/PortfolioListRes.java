package com.modern.admin.portfolio.dto.res;

import com.modern.admin.portfolio.dto.model.PortfolioListVO;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(value ="포토폴리오 목록 응답")
public class PortfolioListRes implements Serializable {
    private List<PortfolioListVO> portfolioList;        // 포토폴리오 목록 리스트

    private int currentPage;                            // 현재 페이지
    private int totalCnt;                               // 총 갯수
}