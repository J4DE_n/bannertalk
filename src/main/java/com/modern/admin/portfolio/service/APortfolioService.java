package com.modern.admin.portfolio.service;

import com.modern.admin.portfolio.dao.APortfolioDao;
import com.modern.admin.portfolio.dto.model.PortfolioListVO;
import com.modern.admin.portfolio.dto.req.*;
import com.modern.admin.portfolio.dto.res.PortfolioDetailRes;
import com.modern.admin.portfolio.dto.res.PortfolioListRes;
import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.file.config.ConfigFile;
import com.modern.common.file.dto.model.FileInfo;
import com.modern.common.file.dto.model.ThumbnailInfo;
import com.modern.common.file.dto.req.FileDeleteReq;
import com.modern.common.file.dto.req.FileListReq;
import com.modern.common.file.dto.req.FileSaveReq;
import com.modern.common.file.dto.res.FileInfoRes;
import com.modern.common.file.service.FileService;
import com.modern.common.type.ReturnType;
import com.modern.common.util.CommonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class APortfolioService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private APortfolioDao aPortfolioDao;

    @Autowired
    private ConfigFile configFile;

    @Autowired
    private FileService fileService;

    /**
     * 포토폴리오 목록
     */
    public ResponseHandler<PortfolioListRes> selectPortfolioList(PortfolioListReq req) {
        logger.info("selectPortfolioList[req]" + req);
        final ResponseHandler<PortfolioListRes> result = new ResponseHandler<>();
        req.setStartRow();

        try {
            List<PortfolioListVO> list = aPortfolioDao.selectPortfolioList(req);
            int totalCtn = aPortfolioDao.selectPortfolioListCnt(req);
            PortfolioListRes portfolioListRes = new PortfolioListRes();
            portfolioListRes.setPortfolioList(list);
            portfolioListRes.setTotalCnt(totalCtn);

            if(totalCtn == 0) {
                result.setReturnCode(ReturnType.RTN_TYPE_NO_DATA);
                logger.info("selectPortfolioList[result]" + result);
                return result;
            }
            result.setData(portfolioListRes);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
            logger.info("selectPortfolioList[result]" + result);
        }catch (Exception e) {
            logger.error("selectPortfolioList[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }
        return result;
    }

    /**
     * 포토폴리오 상세
     */
    public ResponseHandler<PortfolioDetailRes> selectPortfolioDetail(PortfolioDetailReq req) {
        logger.info("selectPortfolioDetail[req]" + req);
        final ResponseHandler<PortfolioDetailRes> result = new ResponseHandler<>();
        FileListReq fileListReq = new FileListReq();
        try{
            PortfolioDetailRes portfolioDetailRes = aPortfolioDao.selectPortfolioDetail(req);

            // Thumnail File Info
            if (CommonUtil.isNotEmpty(portfolioDetailRes.getTnFileGrpSeq())) {
                fileListReq.setFileGrpSeq(portfolioDetailRes.getTnFileGrpSeq());
                List<FileInfo> fileInfo = fileService.getFileList(fileListReq);
                if(fileInfo.size() > 0){
                    portfolioDetailRes.setTnFile(fileInfo.get(0));
                }
            }

            // Detail File Info
            if (CommonUtil.isNotEmpty(portfolioDetailRes.getDetailFileGrpSeq())) {
                fileListReq.setFileGrpSeq(portfolioDetailRes.getDetailFileGrpSeq());
                List<FileInfo> fileInfo = fileService.getFileList(fileListReq);
                if(fileInfo.size() > 0){
                    portfolioDetailRes.setDetailFile(fileInfo.get(0));
                }
            }

            result.setData(portfolioDetailRes);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
            logger.info("selectPortfolioDetail[result]" + result);
        } catch (Exception e){
            logger.error("selectPortfolioDetail[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }
        return result;
    }

    /**
     * 포토폴리오 등록
     */
    public ResponseHandler<FileInfoRes> insertPortfolio(PortfolioInsertReq req) {
        logger.info("insertPortfolio[req]" + req);
        final ResponseHandler<FileInfoRes> result = new ResponseHandler<>();
        try {
            // 메인 썸네일
            if (!req.getTnFile().isEmpty()) {
                FileInfoRes fileInfoRes = this.setProfileImg(req.getTnFile(), req.getSessionSeq(), req.getTnFileGrpSeq());
                if(CommonUtil.isNotEmpty(fileInfoRes) && fileInfoRes.getFileGrpSeq() > 0){
                    req.setTnFileGrpSeq(fileInfoRes.getFileGrpSeq());
                }
            }

            // 상세설명 이미지
            if (!req.getDetailFile().isEmpty()) {
                FileInfoRes fileInfoRes = this.setImg(req.getDetailFile(), req.getSessionSeq(), req.getDetailFileGrpSeq());
                if(CommonUtil.isNotEmpty(fileInfoRes) && fileInfoRes.getFileGrpSeq() > 0){
                    req.setDetailFileGrpSeq(fileInfoRes.getFileGrpSeq());
                }
            }

            aPortfolioDao.insertPortfolio(req);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
            logger.info("insertPortfolio[result]" + result);
        }catch (Exception e) {
            logger.error("insertPortfolio[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }
        return result;
    }

    /**
     * 포토폴리오 수정
     */
    public ResponseHandler<FileInfoRes> updatePortfolio(PortfolioUpdateReq req) {
        final ResponseHandler<FileInfoRes> result = new ResponseHandler<>();
        logger.info("updatePorfolio[req]" + req);
        try{
            FileDeleteReq fileDeleteReq = new FileDeleteReq();
            fileDeleteReq.setCategory(configFile.getSelectCategory1());

            // 메인 썸네일 삭제
            if ("Y".equals(req.getTnFileDelYn())) {
                fileDeleteReq.setFileGrpSeq(req.getTnFileGrpSeq());
                fileService.deleteFiles(fileDeleteReq);
            }

            // 상세설명 이미지 삭제
            if ("Y".equals(req.getDetailFileDelYn())) {
                fileDeleteReq.setFileGrpSeq(req.getDetailFileGrpSeq());
                fileService.deleteFiles(fileDeleteReq);
            }

            // 메인 썸네일
            if (!req.getTnFile().isEmpty()) {
                FileInfoRes fileInfoRes = this.setProfileImg(req.getTnFile(), req.getSessionSeq(), req.getTnFileGrpSeq());
                if(CommonUtil.isNotEmpty(fileInfoRes) && fileInfoRes.getFileGrpSeq() > 0){
                    req.setTnFileGrpSeq(fileInfoRes.getFileGrpSeq());
                }
            }

            // 상세설명 이미지
            if (!req.getDetailFile().isEmpty()) {
                FileInfoRes fileInfoRes = this.setImg(req.getDetailFile(), req.getSessionSeq(), req.getDetailFileGrpSeq());
                if(CommonUtil.isNotEmpty(fileInfoRes) && fileInfoRes.getFileGrpSeq() > 0){
                    req.setDetailFileGrpSeq(fileInfoRes.getFileGrpSeq());
                }
            }

            // 고객 변화 정보 등록
            aPortfolioDao.updatePortfolio(req);

            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            logger.error("updatePorfolio[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 포토폴리오 삭제
     */
    public ResponseHandler<?> deletePortfolio(PortfolioDeleteReq req) {
        logger.info("deletePortfolio[req]" + req);
        final ResponseHandler<?> result = new ResponseHandler<>();
        try {
            if (req.getPtfSeqList().length > 0) {
                for (Integer seq : req.getPtfSeqList()) {
                    req.setPtfSeq(seq);
                    aPortfolioDao.deletePortfolio(req);
                }
            }
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 썸네일 이미지 저장
     *
     */
    public FileInfoRes setProfileImg(MultipartFile file, Integer sessionSeq, Integer fileGrpSeq) throws Exception {
        if (file != null && !file.isEmpty()) {
            ThumbnailInfo thumbnailInfo = new ThumbnailInfo();
            thumbnailInfo.setWidth(555);
            thumbnailInfo.setHeight(555);

            String fileNm = file.getOriginalFilename();
            int pos = fileNm.lastIndexOf(".");
            thumbnailInfo.setExt(fileNm.substring(pos + 1));

            FileSaveReq fileSaveReq = new FileSaveReq();
            fileSaveReq.setCategory(configFile.getSelectCategory1());
            fileSaveReq.setIsSaveInfos(true);
            fileSaveReq.setUserSeq(sessionSeq);
            fileSaveReq.setCheckExtCategory(configFile.getExtCategory1()); //이미지 확장자 체크

            fileSaveReq.setIsThumnail(true);
            fileSaveReq.setThumnailInfo(thumbnailInfo);

            if (fileGrpSeq != null && fileGrpSeq > 0) {
                fileSaveReq.setFileGrpSeq(fileGrpSeq);

                //이전 파일 삭제
                FileDeleteReq deleteReq = new FileDeleteReq();
                deleteReq.setCategory(configFile.getSelectCategory1());
                deleteReq.setFileGrpSeq(fileGrpSeq);
                fileService.deleteFiles(deleteReq);
            }
            return fileService.storeFiles(new MultipartFile[]{file}, fileSaveReq);
        }
        return null;
    }

    /**
     * 일반 이미지 저장
     *
     */
    public FileInfoRes setImg(MultipartFile file, Integer sessionSeq, Integer fileGrpSeq) throws Exception {
        if (file != null && !file.isEmpty()) {
            FileSaveReq fileSaveReq = new FileSaveReq();
            fileSaveReq.setCategory(configFile.getSelectCategory1());
            fileSaveReq.setIsSaveInfos(true);
            fileSaveReq.setCheckExtCategory(configFile.getExtCategory1());
            fileSaveReq.setUserSeq(sessionSeq);

            if (fileGrpSeq != null && fileGrpSeq > 0) {
                fileSaveReq.setFileGrpSeq(fileGrpSeq);

                //이전 파일 삭제
                FileDeleteReq deleteReq = new FileDeleteReq();
                deleteReq.setCategory(configFile.getSelectCategory1());
                deleteReq.setFileGrpSeq(fileGrpSeq);
                fileService.deleteFiles(deleteReq);
            }
            return fileService.storeFiles(new MultipartFile[]{file}, fileSaveReq);
        }
        return null;
    }
}