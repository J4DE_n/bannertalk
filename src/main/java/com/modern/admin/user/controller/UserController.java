package com.modern.admin.user.controller;

import com.modern.admin.user.dto.req.*;
import com.modern.admin.user.service.UserService;
import com.modern.common.dto.res.ResponseHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value="/admin/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    public ResponseHandler<?> get(UserReq req) {
        ResponseHandler<?> result = userService.get(req);
        return  result;
    }

    @GetMapping("/detail")
    public ResponseHandler<?> detail(UserDetailReq req) {
        ResponseHandler<?> result = userService.detail(req);
        return  result;
    }

    @DeleteMapping
    public ResponseHandler<?> delete(@RequestBody(required = false) UserDeleteReq req) {
        ResponseHandler<?> result = userService.delete(req);
        return  result;
    }

    @GetMapping("/recommend")
    public ResponseHandler<?> recommend(UserRecommendReq req) {
        ResponseHandler<?> result = userService.recommend(req);
        return  result;
    }

    @PutMapping
    public ResponseHandler<?> put(@RequestBody UserPutReq req) {
        ResponseHandler<?> result = userService.put(req);
        return  result;
    }

    @PutMapping("/planUserYn")
    public ResponseHandler<?> planUserYn(@RequestBody UserPlanUserYnReq req) {
        ResponseHandler<?> result = userService.planUserYn(req);
        return  result;
    }

}
