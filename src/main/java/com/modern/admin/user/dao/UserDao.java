package com.modern.admin.user.dao;

import com.modern.admin.user.dto.model.UserDetailVO;
import com.modern.admin.user.dto.model.UserRecommendVO;
import com.modern.admin.user.dto.model.UserVO;
import com.modern.admin.user.dto.req.*;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface UserDao {

    List<UserVO> get(UserReq req);

    int getCnt(UserReq req);

    UserDetailVO detail(UserDetailReq req);

    void delete(UserDeleteReq req);

    List<UserRecommendVO> recommend(UserRecommendReq req);

    int recommendCnt(UserRecommendReq req);

    void updateClass(UserPutReq req);

    void updateProfile(UserPutReq req);

    void planUserYn(UserPlanUserYnReq req);
}
