package com.modern.admin.user.dto.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value ="회원관리")
public class UserRecommendVO {
    private String userName;
    private String userSeq;
    private String userId;
    private String creDt;
}
