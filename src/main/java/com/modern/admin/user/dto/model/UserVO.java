package com.modern.admin.user.dto.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value ="회원관리")
public class UserVO {
    private String userSeq;
    private String userName;
    private String userId;
    private String planUserYn;
    private String allRecommendCnt;
    private String payRecommendCnt;
    private String classCd;
    private String tel;
    private String gender;
    private String birthday;
    private String myRecommendCd;
    private String creDt;
}
