package com.modern.admin.user.dto.req;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class UserDeleteReq implements Serializable {
    private List userSeqs;
}
