package com.modern.admin.user.dto.req;

import com.modern.common.dto.req.CommonReq;
import lombok.Data;

import java.io.Serializable;

@Data
public class UserDetailReq extends CommonReq implements Serializable {

}
