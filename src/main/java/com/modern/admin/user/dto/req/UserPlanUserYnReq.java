package com.modern.admin.user.dto.req;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value ="설계사 업데이트")
public class UserPlanUserYnReq {
    private String seq;
    private String planUserYn;
}
