package com.modern.admin.user.dto.req;

import com.modern.common.dto.req.CommonReq;
import lombok.Data;

import java.io.Serializable;

@Data
public class UserPutReq extends CommonReq implements Serializable {
    private String updUserName;
    private String classCd;
    private String tel;
    private String seq;
}
