package com.modern.admin.user.service;

import com.modern.admin.user.dao.UserDao;
import com.modern.admin.user.dto.model.UserDetailVO;
import com.modern.admin.user.dto.model.UserRecommendVO;
import com.modern.admin.user.dto.model.UserVO;
import com.modern.admin.user.dto.req.*;
import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.type.ReturnType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    UserDao userDao;

    public ResponseHandler<?> get(UserReq req) {
        final ResponseHandler<Map> result = new ResponseHandler<>();

        try {

            req.setStartRow();
            List<UserVO> list = userDao.get(req);
            int cnt = userDao.getCnt(req);

            Map map = new HashMap();
            map.put("list",list);
            map.put("cnt",cnt);

            result.setData(map);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e){
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }

        return result;
    }

    public ResponseHandler<?> detail(UserDetailReq req) {
        final ResponseHandler<Map> result = new ResponseHandler<>();

        try {

            UserDetailVO info = userDao.detail(req);
            Map map = new HashMap();
            map.put("info",info);

            result.setData(map);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e){
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }

        return result;
    }

    public ResponseHandler<?> delete(UserDeleteReq req) {
        final ResponseHandler<Map> result = new ResponseHandler<>();

        try {

            System.out.println(req.getUserSeqs());

            userDao.delete(req);

            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e){
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }

        return result;
    }

    public ResponseHandler<?> recommend(UserRecommendReq req) {
        final ResponseHandler<Map> result = new ResponseHandler<>();

        try {
            req.setStartRow();
            List<UserRecommendVO> list =  userDao.recommend(req);
            int cnt =  userDao.recommendCnt(req);

            Map map  = new HashMap();
            map.put("list",list);
            map.put("cnt",cnt);
            result.setData(map);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e){
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }

        return result;
    }

    public ResponseHandler<?> put(UserPutReq req) {
        final ResponseHandler<?> result = new ResponseHandler<>();

        try {

            //userDao.updateClass(req);
            userDao.updateProfile(req);

            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e){
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }

        return result;
    }

    public ResponseHandler<?> planUserYn(UserPlanUserYnReq req) {
        final ResponseHandler<?> result = new ResponseHandler<>();

        try {

            userDao.planUserYn(req);

            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e){
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }

        return result;
    }
}

