package com.modern.comCode.controller;

import com.modern.comCode.dto.req.AddrItemReq;
import com.modern.comCode.dto.req.AddrReq;
import com.modern.comCode.dto.req.ComCodeReq;
import com.modern.comCode.dto.res.AddrItemRes;
import com.modern.comCode.dto.res.AddrRes;
import com.modern.comCode.service.ComService;
import com.modern.common.dto.res.ResponseHandler;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *  Page 컨틀롤러
 *
 *
 */


@RequestMapping(value="/api/com" )
@RestController
public class ComController {

    @Autowired
    ComService comService;
    /*
	 * 공통코드 조회
	 */
    @ApiOperation(value = "공통코드 조회")
    @GetMapping(value="/code")
    @ResponseBody
    public ResponseHandler<?> selectCode(ComCodeReq req) {
        ResponseHandler<?> result = comService.selectComCodeList(req);
        return  result;
    }


    /*
	 * 주소 조회
	 */
    @ApiOperation(value = "주소 조회")
    @GetMapping(value="/addr")
    public ResponseHandler<AddrRes> selectAddr(AddrReq req) {
        ResponseHandler<AddrRes> result = comService.selectAddr(req);
        return  result;
    }

    /*
	 * 주소 항목 조회
	 */
    @ApiOperation(value = "주소 항목 조회")
    @GetMapping(value="/addr/item")
    public ResponseHandler<AddrItemRes> selectAddrItem(AddrItemReq req) {
        ResponseHandler<AddrItemRes> result = comService.selectAddrItem(req);
        return  result;
    }

}


