package com.modern.comCode.dao;

import com.modern.comCode.dto.model.AddrItemVO;
import com.modern.comCode.dto.model.AddrVO;
import com.modern.comCode.dto.model.ComCodeVO;
import com.modern.comCode.dto.req.AddrItemReq;
import com.modern.comCode.dto.req.AddrReq;
import com.modern.comCode.dto.req.ComCodeReq;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import javax.validation.Valid;
import java.util.List;

@Repository
@Mapper
public interface ComDao
{
    /* 공통코드 조회 */
    List<ComCodeVO> selectComDtl(@Valid ComCodeReq req);

    /* 주소 목록 조회 */
    List<AddrVO> selectAddr(AddrReq req) throws Exception;

    /* 주소 항목 목록 조회 */
    List<AddrItemVO> selectAddrItem(AddrItemReq req);
}
