package com.modern.comCode.dto.model;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "주소")
public class AddrItemVO implements Serializable {
    private Integer majorSeq;
    private Integer minorSeq;
    private String addrMajor;
    private String addrMinor;
}
