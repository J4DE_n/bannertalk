package com.modern.comCode.dto.model;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "주소")
public class AddrVO implements Serializable {
    private Integer addrSeq;
    private String addrType;
    private Integer parentAddrSeq;
    private String item;
}
