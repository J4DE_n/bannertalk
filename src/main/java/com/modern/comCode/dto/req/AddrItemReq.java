package com.modern.comCode.dto.req;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "주소목록 조회")
public class AddrItemReq implements Serializable {
    @ApiParam(value = "주소 seq Array")
    private String[] seqList;
}
