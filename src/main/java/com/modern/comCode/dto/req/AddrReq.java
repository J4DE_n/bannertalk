package com.modern.comCode.dto.req;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "주소목록 조회")
public class AddrReq implements Serializable {
    @ApiParam(value = "주소 부모코드")
    private Integer parentAddrSeq;

    @ApiParam(value = "주소 타입")
    private String addrType;
}
