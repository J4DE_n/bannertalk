package com.modern.comCode.dto.res;
import com.modern.comCode.dto.model.AddrItemVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(value = "주소")
public class AddrItemRes implements Serializable {
    @ApiParam(value = "주소항목목록", required = true)
    @NotNull
    private List<AddrItemVO> list;
}
