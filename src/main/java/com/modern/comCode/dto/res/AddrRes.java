package com.modern.comCode.dto.res;
import com.modern.comCode.dto.model.AddrVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(value = "주소")
public class AddrRes implements Serializable {
    @ApiParam(value = "주소목록", required = true)
    @NotNull
    private List<AddrVO> list;
}
