package com.modern.comCode.service;


import com.modern.comCode.dto.req.AddrItemReq;
import com.modern.comCode.dto.req.AddrReq;
import com.modern.comCode.dto.req.ComCodeReq;
import com.modern.comCode.dto.res.AddrItemRes;
import com.modern.comCode.dto.res.AddrRes;
import com.modern.common.dto.res.ResponseHandler;

import javax.validation.Valid;

/**
 *  LoginService를 위한 IF Class
 *  Spring Security사용을 위해 UserDetailsService를 Override해야 한다.
 *
 *
 */
public interface ComService {

    /* 공통코드 조회 */
    ResponseHandler<?> selectComCodeList(@Valid ComCodeReq req);

    ResponseHandler<AddrRes> selectAddr(AddrReq req);

    ResponseHandler<AddrItemRes> selectAddrItem(AddrItemReq req);
}
