package com.modern.comCode.service;

import com.modern.comCode.dao.ComDao;
import com.modern.comCode.dto.model.AddrItemVO;
import com.modern.comCode.dto.model.AddrVO;
import com.modern.comCode.dto.model.ComCodeVO;
import com.modern.comCode.dto.req.AddrItemReq;
import com.modern.comCode.dto.req.AddrReq;
import com.modern.comCode.dto.req.ComCodeReq;
import com.modern.comCode.dto.res.AddrItemRes;
import com.modern.comCode.dto.res.AddrRes;
import com.modern.comCode.dto.res.ComCodeRes;
import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.type.ReturnType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.List;

/**
 *
 * 공통관련 impl
 *
 *
 */
@Service
public class ComServiceImpl implements ComService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ComDao comDao;


    //@Autowired
    //RestTemplageService_ restTemplageService;

    /* 공통코드 조회 */
    @Override
    public ResponseHandler<?> selectComCodeList(@Valid ComCodeReq req) {
        final ResponseHandler<ComCodeRes> result = new ResponseHandler<>();
        try {
            ComCodeRes comCodeRes = new ComCodeRes();
            List<ComCodeVO> comCodeList = comDao.selectComDtl(req);
            comCodeRes.setComCodeList(comCodeList);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
            result.setData(comCodeRes);
        } catch (Exception e){
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public ResponseHandler<AddrRes> selectAddr(AddrReq req) {
        final ResponseHandler<AddrRes> result = new ResponseHandler<>();

        try {
            AddrRes addrRes = new AddrRes();
            List<AddrVO> list = comDao.selectAddr(req);
            addrRes.setList(list);

            result.setReturnCode(ReturnType.RTN_TYPE_OK);
            result.setData(addrRes);
        } catch (Exception e){
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            logger.error("[selectAddr Exception]", e);
        }

        return result;
    }

    @Override
    public ResponseHandler<AddrItemRes> selectAddrItem(AddrItemReq req) {
        final ResponseHandler<AddrItemRes> result = new ResponseHandler<>();

        try {
            AddrItemRes addrRes = new AddrItemRes();
            List<AddrItemVO> list = comDao.selectAddrItem(req);
            addrRes.setList(list);

            result.setReturnCode(ReturnType.RTN_TYPE_OK);
            result.setData(addrRes);
        } catch (Exception e){
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            logger.error("[selectAddrItem Exception]", e);
        }

        return result;
    }
}

