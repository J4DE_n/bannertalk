package com.modern.common.config;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Data
@Configuration
@ConfigurationProperties(prefix="com")
@PropertySource(value = {"classpath:properties/configCommon.properties"}, encoding = "UTF-8")
public class ConfigCommon {

    private Mail mail;
    @Setter @Getter
    public static class Mail {
        private String adminaddress;
    }

    /* 게시판 타입 코드 */
    private Code code;
    @Setter @Getter
    public static class Code {
        private int event;
    }

    private Mst mst;
    @Setter @Getter
    public static class Mst {
        private String board;
        private String faq;
        private String qna;
    }

    private BootPay bootPay;
    @Setter @Getter
    public static class BootPay {
        private String applicationId;
        private String privateKey;
        private String scheme;
        private String host;
        private String getToken;
        private String receipt;
        private String cancel;
    }

    private Koscom koscom;
    @Setter @Getter
    public static class Koscom {
        private String host;
        private String scheme;
        private String key;
    }

    private Deposit deposit;
    @Setter @Getter
    public static class Deposit {
        private String account;
        private String holder;
        private String bankNm;
    }

    private String encodeKey;
    private String aes128Key;
    private String domain;
    private String sendPhone;
}
