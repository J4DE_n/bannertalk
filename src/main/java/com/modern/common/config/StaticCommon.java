package com.modern.common.config;

public class StaticCommon {
    //성별
    public static final String MST_GENDER = "GENDER";

    //상품 타입
    public static final String MST_PRODUCT_TYPE = "PRODUCT_TYPE";

    //결제 수단
    public static final String MST_PAYMENT_METHOD = "PAYMENT_METHOD";

    //결제 상태
    public static final String MST_PAYMENT_STATE = "PAYMENT_STATE";

    //환불 상태
    public static final String MST_REFUND_STATUS = "REFUND_STATUS";


}
