package com.modern.common.dto.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.modern.common.type.ReturnType;
import io.swagger.annotations.ApiParam;
import lombok.Data;

@Data
public class ReturnVO {
    @JsonIgnore
    @ApiParam(value = "메시지", hidden = true)
    String msg;

    @JsonIgnore
    @ApiParam(value = "결과코드", hidden = true)
    ReturnType code;
}
