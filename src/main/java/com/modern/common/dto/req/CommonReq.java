package com.modern.common.dto.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiParam;
import lombok.Data;

@Data
public class CommonReq {
    @JsonIgnore
    @ApiParam(value = "사용자 시퀀스", hidden = true)
    Integer userSeq;

    @JsonIgnore
    @ApiParam(value = "사용자 아이디", hidden = true)
    String userId;

    @JsonIgnore
    @ApiParam(value = "사용자 이름", hidden = true)
    String userName;
}
