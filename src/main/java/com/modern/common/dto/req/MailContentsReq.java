package com.modern.common.dto.req;

import lombok.Data;

@Data
public class MailContentsReq extends CommonReq {
    private String reqTel;
    private String reqNm;
    private String reqEmail;
    private String category;
    private String title;
    private String contents;
}
