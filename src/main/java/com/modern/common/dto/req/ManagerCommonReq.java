package com.modern.common.dto.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiParam;
import lombok.Data;

@Data
public class ManagerCommonReq {
    @JsonIgnore
    @ApiParam(value = "관리자 시퀀스", hidden = true)
    Integer sessionSeq;

    @JsonIgnore
    @ApiParam(value = "관리자 아이디", hidden = true)
    String sessionId;

    @JsonIgnore
    @ApiParam(value = "관리자 이름", hidden = true)
    String sessionName;

    @JsonIgnore
    @ApiParam(value = "관리자 권한", hidden = true)
    Integer sessionAuth;
}
