package com.modern.common.dto.req;

import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnore;

import javax.validation.constraints.NotNull;

@Data
public class ManagerPagingReq extends ManagerCommonReq {
    @ApiParam(value = "현재 페이지 번호", required = true)
    @NotNull
    private int currentPage;

    @ApiParam(value = "노출 게시물 개수", required = true)
    @NotNull
    private int cntPerPage;

    @JsonIgnore
    @ApiParam(value = "시작 번호", hidden = true)
    private int startRow;

    public void setStartRow() {
        this.startRow = (this.currentPage - 1) * this.cntPerPage;
    }
}
