package com.modern.common.file.controller;

import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.file.config.ConfigFile;
import com.modern.common.file.dto.req.AFileEditorReq;
import com.modern.common.file.dto.req.FileDownloadReq;
import com.modern.common.file.dto.req.FileSaveReq;
import com.modern.common.file.dto.req.FileZipDownloadReq;
import com.modern.common.file.dto.res.FileDownloadRes;
import com.modern.common.file.dto.res.FileInfoRes;
import com.modern.common.file.dto.res.FileZipDownloadRes;
import com.modern.common.file.service.FileService;
import com.modern.common.session.SessionManagerCheck;
import com.modern.common.type.ReturnType;
import com.modern.common.util.CommonUtil;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

@RestController
@RequestMapping(value="/api/file" )
public class FileController {

    @Autowired
    FileService fileService;

    @Autowired
    ConfigFile configFile;

    CommonUtil commonUtil = new CommonUtil();

//    @PostMapping(value="/delete")
//    public void delete(@Valid @RequestBody(required=false) final FileDeleteReq req) {
//
//        try {
//            fileService.deleteFiles(req);
//        }
//        catch(Exception e) {
//        }
//    }
//
//    /**
//     * 1개의 파일을 저장 테스트
//     *
//     */
//    @RequestMapping(value="/store" , method = RequestMethod.POST)
//    public void storeFile(@RequestParam("file") MultipartFile file) {
//        // For test
//
//        ResponseHandler<FileInfoRes> result = new ResponseHandler<>() ;
//        FileInfoRes fileInfoRes = new FileInfoRes();  //storeFiles(MultipartFile[] files, FileSaveReq req)
//        FileSaveReq fileSaveReq = new FileSaveReq();
//
//        String url = null;
//        ReturnType rtn ;
//
//        MultipartFile[] files = new MultipartFile[1];
//        files[0] = file;
//        fileSaveReq.setCategory(1);
//        fileSaveReq.setFileGrpSeq(0);
//        fileSaveReq.setGrpNm("test");
//        fileSaveReq.setIsSaveInfos(true);
//
//        List<FileDetailInfo> fileDetailInfos = new  ArrayList<>();
//
//        FileDetailInfo fileDetailInfo = new FileDetailInfo();
//
//        fileDetailInfo.setOrderNo(5);
//        fileDetailInfo.setSubType("111");
//        fileDetailInfo.setType("222");
//
//        fileDetailInfos.add(fileDetailInfo);
//
//        fileSaveReq.setFileDetailInfos(fileDetailInfos);
//
//        try {
//
//            fileInfoRes = fileService.storeFiles(files, fileSaveReq);
//
//        }
//        catch(Exception e) {
//            System.out.println("[StoreFile][Exception] " + e.toString());
//        }
//
//        result.setData(fileInfoRes);
//    }


    @ApiOperation(value="1개 파일 다운로드")
    @GetMapping(value="/download")
    public ResponseEntity<InputStreamResource> downloadFile(FileDownloadReq req){

        FileDownloadRes fileDownloadRes = new FileDownloadRes();

        try {
            fileDownloadRes = fileService.downloadFile(req);
        }
        catch(Exception e) {
            return null;
        }

        return ResponseEntity.ok()
                // Content-Disposition
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileDownloadRes.getOrgFileName())
                // Content-Type
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                // Contet-Length
                .contentLength(fileDownloadRes.getContentLength())
                // Input Stream
                .body(fileDownloadRes.getInputStreamResource());
    }

    @ApiOperation(value="전체 파일 압축 다운로드")
    @GetMapping(value="/download/zip")
    public ResponseEntity<InputStreamResource> downloadZipFile(FileZipDownloadReq req){

        FileZipDownloadRes fileDownloadRes = new FileZipDownloadRes();

        try {
            fileDownloadRes = fileService.downloadZipFile(req);
        }
        catch(Exception e) {
            return null;
        }

        String contentType = "application/zip";
        String zipFileName;

        if(!commonUtil.isExist(req.getZipFileName())) {
            zipFileName = "downloadFiles.zip";
        } else {
            zipFileName = req.getZipFileName() + ".zip";
        }

        InputStream is = new ByteArrayInputStream(fileDownloadRes.getOutputStreamResource().toByteArray());
        InputStreamResource inputStreamResource = new InputStreamResource(is);

        return ResponseEntity.ok()
                // Content-Disposition
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + zipFileName)
                // Content-Type
                .contentType(MediaType.parseMediaType(contentType))
                // Contet-Length
                //.contentLength(???)
                // Input Stream
                .body(inputStreamResource);
                //.body(fileDownloadRes.getOutputStreamResource());
    }


    /**
     *  에디터 파일 업로드
     */
    @ApiOperation(value = "에디터 파일 업로드")
    @PostMapping(value = "/editor")
    @SessionManagerCheck
    public ResponseHandler<FileInfoRes> imgUpload(AFileEditorReq req) {
        final ResponseHandler<FileInfoRes> result = new ResponseHandler<>();
        FileInfoRes fileInfoRes;
        try {

            System.out.println("check ## " + req);

            FileSaveReq fileSaveReq = new FileSaveReq();
            BeanUtils.copyProperties(fileSaveReq, req);

            MultipartFile[] files = {req.getFile()};
            fileInfoRes = fileService.storeFiles(files, fileSaveReq);
            result.setData(fileInfoRes);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            e.printStackTrace();
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }

        return  result;
    }

}
