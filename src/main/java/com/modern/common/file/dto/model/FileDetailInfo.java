package com.modern.common.file.dto.model;

import lombok.Data;

@Data
public class FileDetailInfo {
    int orderNo;
    String type;
    String subType;
}
