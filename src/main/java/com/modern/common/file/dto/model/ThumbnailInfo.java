package com.modern.common.file.dto.model;

import lombok.Data;

@Data
public class ThumbnailInfo {
    int width;
    int height;
    String ext;
}
