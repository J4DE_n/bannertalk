package com.modern.common.file.dto.req;

import com.modern.common.dto.req.ManagerCommonReq;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

@Data
public class FileEditorReq extends ManagerCommonReq {
    @ApiParam(value = "파일 카테고리", required = true)
    @NotNull
    private int category;
    @ApiParam(value = "저장여부")
    private Boolean isSaveInfos = false;
    @ApiParam(value = "확장자체크")
    private int checkExtCategory = 1;
    @ApiParam(value = "업로드 파일")
    private MultipartFile file;
}
