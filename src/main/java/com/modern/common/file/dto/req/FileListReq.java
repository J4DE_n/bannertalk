package com.modern.common.file.dto.req;

import lombok.Data;

@Data
public class FileListReq {

    Integer fileGrpSeq;
    Integer fileSeq;
}
