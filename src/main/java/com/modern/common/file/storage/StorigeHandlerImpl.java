package com.modern.common.file.storage;

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.modern.common.type.ReturnType;
import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.List;

/**
 *  서버 로컬 폴더 제어 클래스
 *
 *  서버 로컬 드라이브 폴더에 파일 저장, 삭제 등의 동작을 실시 한다.
 *
 */
public class StorigeHandlerImpl implements StorigeHandler {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 파일 저장
     *
     *  MultipartFile 형식 파일을 서버 스토리지 내에  저장 한다.
     *
     * @param file
     * @param storagePath
     * @param fileName
     * @return
     * @throws Exception
     */
    public ReturnType store(MultipartFile file, String storagePath , String fileName ) throws Exception {

        String toFileLoc;

        System.out.println("[STORAGE] store");

        if(fileName== null || file ==null)
        {
            return  ReturnType.RTN_TYPE_NG;
        }

        // 디레토리가 없다면 생성
        File dir = new File(storagePath);

        if (!dir.isDirectory()) {
            dir.mkdirs();
        }

        // 파일 생성 및 저장
        File serverFile = new File(storagePath + File.separator + fileName);

        file.transferTo(serverFile);

        System.out.println("[STORAGE] store OK");
        return ReturnType.RTN_TYPE_OK;
    }

    /**
     * 파일 저장
     *
     * Array 형식 파일을 서버 스토리지 내에 저장 한다.
     *
     * @param file
     * @param storagePath
     * @param fileName
     * @return
     * @throws Exception
     */
    public ReturnType store(List<String> file, String storagePath , String fileName ) throws Exception {

        String toFileLoc;

        System.out.println("[STORAGE] store");

        if(fileName== null || file ==null)
        {
            return  ReturnType.RTN_TYPE_NG;
        }

        // 디레토리가 없다면 생성
        File dir = new File(storagePath);

        if (!dir.isDirectory()) {
            dir.mkdirs();
        }

        // 파일 생성 및 저장
        File infoFile = new File(storagePath + File.separator + fileName);

        BufferedWriter outputWriter = null;
        outputWriter = new BufferedWriter(new FileWriter(infoFile));

        for(String info : file) {
            outputWriter.write(info);
            outputWriter.newLine();
        }

        outputWriter.flush();
        outputWriter.close();

        System.out.println("[STORAGE] store OK");
        return ReturnType.RTN_TYPE_OK;
    }


    /**
     * 파일 삭제
     *
     * 서버 스토리지 내 파일을 삭제 한다.
     *
     * @param storagePath
     * @param fileName
     * @return
     * @throws Exception
     */
    public ReturnType delete(String storagePath , String fileName ) throws Exception {
        logger.info("[STORAGE] delete" + storagePath + File.separator + fileName);

        File serverFile = new File(storagePath + File.separator + fileName);

        if( serverFile.exists() ){
            if(!serverFile.delete()) {
                return ReturnType.RTN_TYPE_NG;
            }
            return ReturnType.RTN_TYPE_OK;
        } else {
            logger.error("[STORAGE] file not exisits");
            return ReturnType.RTN_TYPE_NG;
        }
    }

    /**
     * 썸네일 저장
     *
     * 비율에 맞추어 썸네일을 만든다.
     *
     * @param file
     * @param storagePath
     * @param fileName
     * @param width
     * @param height
     * @param ext
     * @return ReturnType
     * @throws Exception
     */
    public ReturnType makeThumbnail(MultipartFile file, String storagePath, String fileName, int width, int height, String ext) throws Exception{
        if(fileName== null || file ==null) {
            return  ReturnType.RTN_TYPE_NG;
        }

        /* orientation */
        int orientation = getOrientation(file.getInputStream());
        BufferedImage originImg = ImageIO.read(file.getInputStream());

        if(orientation == 6){
            originImg = rotateImage(originImg, 90);
        } else if(orientation == 3){
            originImg = rotateImage(originImg, 180);
        } else if(orientation == 8){
            originImg = rotateImage(originImg, 270);
        }

        /* crop */
        int originWidth = originImg.getWidth();
        int originHeight = originImg.getHeight();

        //높이 계산
        int imgW = originWidth;
        int imgH = (imgW * height) / width;

        /*
         * 너비 계산
         * 계산된 높이가 원본보다 높으면 crop할 수 없으므로 높이기준 너비 재계산
         * */
        if(imgH > originHeight){
            imgW = (originHeight * width) / height;
            imgH = originHeight;
        }

        /*
         * crop(원본이미지, x좌표, y좌표, crop너비, crop높이)
         * */
        BufferedImage cropImg = Scalr.crop(originImg, (originWidth-imgW)/2, (originHeight-imgH)/2, imgW, imgH); //crop
        BufferedImage thumbImg = Scalr.resize(cropImg, width, height); //썸네일 생성

        //썸네일 저장
        File serverFile = new File(storagePath + File.separator + fileName);

        ImageIO.write(thumbImg, ext, serverFile);
        return  ReturnType.RTN_TYPE_OK;
    }

    private int getOrientation(InputStream file) throws Exception{
        int orientation =1;
        try {
            Metadata metadata = ImageMetadataReader.readMetadata(file);
            Directory directory = metadata.getFirstDirectoryOfType(ExifIFD0Directory.class);
            if(directory != null){
                orientation = directory.getInt(ExifIFD0Directory.TAG_ORIENTATION);
            }
        } catch (Exception e){
            orientation = 1;
        }

        return orientation;
    }

    public BufferedImage rotateImage(BufferedImage orgImage,int radians) {
        BufferedImage newImage;
        if(radians==90 || radians==270){
            newImage = new BufferedImage(orgImage.getHeight(),orgImage.getWidth(),orgImage.getType());
        } else if (radians==180){
            newImage = new BufferedImage(orgImage.getWidth(),orgImage.getHeight(),orgImage.getType());
        } else{
            return orgImage;
        }
        Graphics2D graphics = (Graphics2D) newImage.getGraphics();
        graphics.rotate(Math. toRadians(radians), newImage.getWidth() / 2, newImage.getHeight() / 2);
        graphics.translate((newImage.getWidth() - orgImage.getWidth()) / 2, (newImage.getHeight() - orgImage.getHeight()) / 2);
        graphics.drawImage(orgImage, 0, 0, orgImage.getWidth(), orgImage.getHeight(), null );

        return newImage;
    }
}
