package com.modern.common.service;

import net.minidev.json.JSONObject;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Map;

/**
 *
 * Resttemplate Service
 *
 *
 */
@Service
public class RestTemplateService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    private Environment environment;


    public String post(URI url, Object params) throws Exception {
        ResponseEntity<String> result = null;
        restTemplate = new RestTemplate(getClientHttpRequestFactory());

        try {

            System.setProperty("sun.net.http.allowRestrictedHeaders", "true");

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            logger.info("restTemplate post [url]" + url);
            logger.info("restTemplate post [params]" + params);
            result = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(params, headers), String.class);
            logger.info("restTemplate [result]" + result);
        } catch (Exception e) {
            logger.error("restTemplate post [Exception]" + e);
            logger.error("restTemplate post [Exception]", e);
            throw new Exception(e);
        }

        return result.getBody();
    }

    /* 가비아 Get Token */
    public String postForGabia(URI url, Object params) throws Exception {
        ResponseEntity<String> result = null;
        restTemplate = new RestTemplate(getClientHttpRequestFactory());

        String[] text = params.toString().replaceAll("}", "").split("=");

        try {

            System.setProperty("sun.net.http.allowRestrictedHeaders", "true");

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.set("cache-control", "no-cache");
            headers.set("Authorization", text[1]);

            logger.info("restTemplate post [url]" + url);
            logger.info("restTemplate post [params]" + params);
            result = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(params, headers), String.class);
            logger.info("restTemplate [result]" + result);
        } catch (Exception e) {
            logger.error("restTemplate post [Exception]" + e);
            logger.error("restTemplate post [Exception]", e);
            throw new Exception(e);
        }

        return result.getBody();
    }


    public JSONObject JsonByAccessToken(URI url,String access_token,String method,Map<String, Object> parmas) throws Exception {
        ResponseEntity<JSONObject> result;

        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization",access_token);
            HttpEntity<Map<String, Object>> entity = new HttpEntity<>(parmas,headers);

            HttpMethod m = HttpMethod.GET;
            if(method.equals("post")){m = HttpMethod.POST;}
            result = restTemplate.exchange(url, m ,entity,JSONObject.class);
            logger.info("restTemplate [result]"+result);
        } catch (Exception e) {
            logger.error("restTemplate [Exception]", e);
            throw new Exception(e);
        }

        return result.getBody();
    }

    public String get(URI url) throws Exception {
        ResponseEntity<String> result = null;
        restTemplate = new RestTemplate(getClientHttpRequestFactory());
        try {
         /*   String[] arr = environment.getActiveProfiles();
            String env=null;
            for(String str : arr) {
                env = str;
            }*/

//            String originUrl="localhost";
//            if(env==null||env.equals("local")){ originUrl="localhost"; }
//            if(env.equals("dev")){ originUrl="172.26.14.127"; }
//            if(env.equals("prod")){ originUrl="172.26.14.127"; }
//            configWallet.getOriginAddr();

            System.setProperty("sun.net.http.allowRestrictedHeaders", "true");

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> entity = new HttpEntity<String>(headers);
            logger.info("restTemplate get [url]" + url);
            result = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
            logger.info("restTemplate get [result]" + result);
        } catch (Exception e) {
            logger.error("restTemplate get [Exception]", e);
            throw new Exception(e);
        }
        return result.getBody();
    }

    private ClientHttpRequestFactory getClientHttpRequestFactory() {
        int timeout = 120000;
        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(timeout)
                .setConnectionRequestTimeout(timeout)
                .setSocketTimeout(timeout)
                .build();
        CloseableHttpClient client = HttpClientBuilder
                .create()
                .setDefaultRequestConfig(config)
                .build();
        return new HttpComponentsClientHttpRequestFactory(client);
    }
}

