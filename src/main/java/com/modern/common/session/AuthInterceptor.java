package com.modern.common.session;

import com.modern.common.type.ReturnType;
import com.modern.common.util.CookieUtil;
import com.modern.membership.dto.res.SessionRes;
import com.modern.membership.service.JwtManagerService;
import com.modern.membership.service.JwtService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class AuthInterceptor extends HandlerInterceptorAdapter {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    JwtService jwtService;

    @Autowired
    JwtManagerService jwtManagerService;

    @Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        try {

            String accessToken = "";
            SessionRes sessionRes = null;
            if(isManager(request)){
                accessToken = jwtManagerService.getAcessToken(request);
                sessionRes = jwtManagerService.getManagerJwtParse(accessToken, request, response);
            } else {
                accessToken = jwtService.getAcessToken(request);
                sessionRes = jwtService.getJwtParse(accessToken, request, response);
            }

            if(sessionRes == null) {
                if(CookieUtil.isAjax(request)) {
                    response.setStatus(ReturnType.RTN_TYPE_SESSION.getValue());
                } else {
                    if(isManager(request)) {
                        jwtManagerService.removeManagerSession(response);
                        response.sendRedirect("/page/admin/login");
                    } else {
                        jwtService.removeSession(response);
                        response.sendRedirect("/");
                    }
                }

                return false;
            } else {
                if(isManager(request)) {
                    if(sessionRes.getAuth() <= 1) { //권한체크
                        response.sendRedirect("/page/admin/login");
                        return false;
                    }
                    if(managerOnly(request)){
                        if(sessionRes.getAuth() <= 17){
                            response.sendRedirect("/page/admin/member/list");
                            return false;
                        }
                    }
                }
            }
        } catch (Exception e) {
            jwtService.removeSession(response);
            logger.error("AuthInterceptor[Exception]", e);
            return false;
        }

        return true;
    }

    /*
    * 관리자 여부
    * */
    private boolean isManager(HttpServletRequest request){
        String servletPath = request.getServletPath();
        if(servletPath.contains("page/admin")){
            return true;
        } else {
            return false;
        }
    }

    private boolean managerOnly(HttpServletRequest request){
        String servletPath = request.getServletPath();
        if(!servletPath.contains("page/admin/member") && !servletPath.contains("page/admin/myInfo")){
            return true;
        } else {
            return false;
        }
    }
}
