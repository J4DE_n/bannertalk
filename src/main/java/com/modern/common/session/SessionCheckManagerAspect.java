package com.modern.common.session;

import com.modern.common.dto.req.ManagerCommonReq;
import com.modern.common.type.ReturnType;
import com.modern.common.util.CommonUtil;
import com.modern.common.util.CookieUtil;
import com.modern.membership.dto.res.SessionRes;
import com.modern.membership.service.JwtManagerService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Aspect
@Component
public class SessionCheckManagerAspect {
    @Autowired
    JwtManagerService jwtManagerService;

    @Around(value="@annotation(sessionManagerCheck)")
    public Object target(ProceedingJoinPoint joinPoint, SessionManagerCheck sessionManagerCheck) throws Throwable {
        Object result = null;

        Object[] params = joinPoint.getArgs();

        if(params[0] != null) {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
            HttpServletResponse response = ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getResponse();

            String accessToken = jwtManagerService.getAcessToken(request);

            String isApp = request.getHeader("isApp");
            if(CommonUtil.isNotEmpty(isApp) && "Y".equals(isApp)) {
                accessToken = request.getHeader("accessToken");
            }

            SessionRes sessionRes = jwtManagerService.getManagerJwtParse(accessToken, request, response);

            if(sessionRes != null) {
                ((ManagerCommonReq)params[0]).setSessionSeq(sessionRes.getUserSeq());
                ((ManagerCommonReq)params[0]).setSessionId(sessionRes.getUserId());
                ((ManagerCommonReq)params[0]).setSessionName(sessionRes.getUserName());
                ((ManagerCommonReq)params[0]).setSessionAuth(sessionRes.getAuth());
            } else {
                if(CookieUtil.isAjax(request)) {
                    jwtManagerService.removeManagerSession(response);
                    response.sendError(ReturnType.RTN_TYPE_SESSION.getValue());
                }
                throw new Exception();
            }
        }

        try {
            result = joinPoint.proceed();
        } catch (Exception e) {
            throw e;
        }

        return result;

    }

}
