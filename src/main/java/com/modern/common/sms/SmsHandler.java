package com.modern.common.sms;

import com.modern.common.config.ConfigCommon;
import com.modern.common.service.RestTemplateService;
import com.modern.common.util.ComEncDecUtil;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.net.URI;

@Component
public class SmsHandler {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ConfigCommon configCommon;

    @Autowired
    public RestTemplateService restTemplateService;

    private static String smsId ="etfsignaloffice"; // id
    private static String apiKey ="489e6c28dd0a192d3a08245ac72d5738"; // apiKey
    private static String tokenUrl ="https://sms.gabia.com/oauth/token"; // 토큰주소

    private static String sendShortUrl ="https://sms.gabia.com/api/send/sms"; // 단문 주소
    private static String sendLongUrl ="https://sms.gabia.com/api/send/lms"; // 장문 주소

    private static String grant_type = "client_credentials";

    private static String autorizationType = "Basic"; // Autorization Type

    /* Get Token */
    public JSONObject getToken() throws Exception {

        JSONObject result =null;

        String authority = "Basic "+ComEncDecUtil.encrytpedBase64(smsId+":"+apiKey);

        try {

            MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
            params.add("Authorization", authority);


            URI uri = new URI("https://sms.gabia.com/oauth/token");

//            URI uri = UriComponentsBuilder.newInstance().scheme("https").host("sms.gabia.com")
//                    .path("/oauth/token")
//                    .query("Authorization="+auth)
//                    .query("grant_type=client_credentials")
//                    .build().encode().toUri();
//
//
//            URI uri = UriComponentsBuilder.newInstance().scheme("https").host("sms.gabia.com")
//            .path("/oauth/token")
//            .query("grant_type=client_credentials")
//            .query("Authorization="+"Basic "+ComEncDecUtil.encrytpedBase64(smsId+":"+apiKey))
//            .build().encode().toUri();



            String accessTokenInfo = restTemplateService.postForGabia(uri, params);

//
//            JSONParser p = new JSONParser(JSONParser.MODE_PERMISSIVE);
//            JSONObject json = (JSONObject) p.parse(accessTokenInfo);
//            String status = json.get("status").toString();
//
//            if(status.equals("200")){
//                result = (JSONObject)json.get("data");
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;

    }

    /* Short sms send */
    public String sendShortSms() throws Exception {
        return null;
    }


    /* Long sms send */
    public String sendLongSms() throws Exception {
        return null;
    }
}
