package com.modern.common.util;


import com.modern.common.dto.req.MailContentsReq;
import com.modern.membership.dto.req.EmailSendReq;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import java.io.StringWriter;

/**
 * Created by MJ on 2018. 8. 23..
 */

public class VelocityUtil {

    public static String getEmailVerifyNumberContents(EmailSendReq req) {
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        velocityEngine.init();

        Template template = velocityEngine.getTemplate("templates/velocity/verifyNumber.vm", "UTF-8");
        VelocityContext velocityContext = new VelocityContext();
        //velocityContext.put("emailVerify", req.getEmailVerifyUrl());

        StringWriter stringWriter = new StringWriter();
        template.merge(velocityContext, stringWriter);

        return stringWriter.toString();

    }

    public static String getEmailVerifyContents(EmailSendReq req, String domain) {
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        velocityEngine.init();

        Template template = velocityEngine.getTemplate("templates/velocity/emailCode.vm", "UTF-8");
        VelocityContext velocityContext = new VelocityContext();
        velocityContext.put("domain", domain);
        velocityContext.put("verifyCode", req.getVerifyCode());
        velocityContext.put("verifyCodeLink", req.getVerifyCodeLink());

        StringWriter stringWriter = new StringWriter();
        template.merge(velocityContext, stringWriter);

        return stringWriter.toString();

    }

    /* 회원가입 완료 폼 */
    public static String getEmailWelcome(String domain) {
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        velocityEngine.init();

        Template template = velocityEngine.getTemplate("templates/velocity/welcome.vm", "UTF-8");
        VelocityContext velocityContext = new VelocityContext();
        velocityContext.put("domain", domain);

        StringWriter stringWriter = new StringWriter();
        template.merge(velocityContext, stringWriter);

        return stringWriter.toString();
    }

    /* 안내폼 */
    public static String getInfoContents(MailContentsReq req, VelocityContext velocityContext) {
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        velocityEngine.init();

        Template template = velocityEngine.getTemplate("templates/velocity/contactus.vm", "UTF-8");

        StringWriter stringWriter = new StringWriter();
        template.merge(velocityContext, stringWriter);

        return stringWriter.toString();

    }

    /* Q&A 문의 폼 */
    public static String getQnaContents(MailContentsReq req, VelocityContext velocityContext) {
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        velocityEngine.init();

        Template template = velocityEngine.getTemplate("templates/velocity/qna.vm", "UTF-8");

        velocityContext.put("title", req.getTitle());
        velocityContext.put("contents", req.getContents());
        velocityContext.put("category", req.getCategory());
        velocityContext.put("reqEmail", req.getReqEmail());
        velocityContext.put("reqNm", req.getReqNm());

        StringWriter stringWriter = new StringWriter();
        template.merge(velocityContext, stringWriter);

        return stringWriter.toString();
    }

}
