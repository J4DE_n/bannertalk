package com.modern.community.controller;

import com.modern.common.dto.res.ResponseHandler;
import com.modern.community.dto.req.NoticeInfoReq;
import com.modern.community.dto.req.NoticeListReq;
import com.modern.community.dto.res.NoticeInfoRes;
import com.modern.community.dto.res.NoticeListRes;
import com.modern.community.service.NoticeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 *  멤버쉽 Rest Controller class
 *
 *  사용자를 제어하기 위한 REST API가 정의 되어 있다.
 *
 *
 */
@RestController
@RequestMapping(value="/notice" ) //모듈명 정의
@Api(value = "NoticeController", description = "공지사항 API")
public class NoticeController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	NoticeService noticeService;

	/**
	 * 공지사항 목록
	 *
	 *
	 * @param : 사용자에서 받아온 저장값
	 * @return : ReturnType
	 */
	@ApiOperation(value = "공지사항 목록")
	@GetMapping
	public ResponseHandler<NoticeListRes> list(NoticeListReq req) {
		ResponseHandler<NoticeListRes> result = noticeService.selectNotice(req);

		return  result;
	}

	/**
	 * 공지사항 상세
	 *
	 *
	 * @param : 사용자에서 받아온 저장값
	 * @return : ReturnType
	 */
	@ApiOperation(value = "공지사항 상세")
	@GetMapping(value="/item")
	public ResponseHandler<NoticeInfoRes> info(NoticeInfoReq req) {
		ResponseHandler<NoticeInfoRes> result = noticeService.selectNoticeInfo(req);
		return  result;
	}
}
