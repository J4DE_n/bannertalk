package com.modern.community.dao;
import com.modern.community.dto.model.NoticeVO;
import com.modern.community.dto.model.PreNextVO;
import com.modern.community.dto.req.NoticeInfoReq;
import com.modern.community.dto.res.NoticeInfoRes;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface NoticeDao
{
    NoticeInfoRes selectNoticeInfo(Map<String, Object> params);

    List<NoticeVO> selectNotice(Map<String, Object> params);

    int selectNoticeCnt(Map<String, Object> params);

    PreNextVO preNextNotice(NoticeInfoReq req);

    void updateNoticeViewCnt(NoticeInfoRes noticeInfoRes);
}
