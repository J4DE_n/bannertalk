package com.modern.community.dto.res;
import com.modern.community.dto.model.PreNextVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="공지사항 상세")
public class NoticeInfoRes implements Serializable {
    @ApiParam(value = "블로그 시퀀스", required = true)
    private int noticeSeq;

    @ApiParam(value = "제목", required = true)
    private String title;

    @ApiParam(value = "요약")
    private String summary;

    @ApiParam(value = "썸네일")
    private String filePath;

    @ApiParam(value = "썸네일 seq")
    @Ignore
    private int fileSeq;

    @ApiParam(value = "파일 그룹 seq")
    @Ignore
    private int fileGrpSeq;

    @ApiParam(value = "썸네일 시스템 명")
    @Ignore
    private String SysFileNm;

    @ApiParam(value = "내용", required = true)
    private String contents;

    @ApiParam(value = "조회수", required = true)
    private int viewCnt;

    @ApiParam(value = "노출여부", required = true)
    private String showYn;

    @ApiParam(value = "삭제여부", required = true)
    private String delYn;

    @ApiParam(value = "등록일", required = true)
    private String startDt;

    @ApiParam(value = "시스템 등록일", required = true)
    private String creDt;


    private PreNextVO prev;
    private PreNextVO next;
}


