package com.modern.community.dto.res;

import com.modern.community.dto.model.NoticeVO;
import com.modern.community.dto.req.NoticeListReq;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "공지사항 목록 정보")
public class NoticeListRes {

    private List<NoticeVO> noticeList;

    private int totalCnt;
    private boolean isEndPage =false;

    public void setIsEndPage(NoticeListReq req, int totalCnt) {
        int endPage = (int) Math.ceil(totalCnt / (double) req.getCntPerPage());
        if(endPage == req.getCurrentPage()){
            this.isEndPage = true;
        }
    }
}
