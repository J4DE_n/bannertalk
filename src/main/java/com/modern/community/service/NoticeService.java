package com.modern.community.service;

import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.type.ReturnType;
import com.modern.common.util.CommonUtil;
import com.modern.common.util.ConvertUtil;
import com.modern.community.dao.NoticeDao;
import com.modern.community.dto.model.NoticeVO;
import com.modern.community.dto.model.PreNextVO;
import com.modern.community.dto.req.NoticeInfoReq;
import com.modern.community.dto.req.NoticeListReq;
import com.modern.community.dto.res.NoticeInfoRes;
import com.modern.community.dto.res.NoticeListRes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 *
 * 입력폼 저장을 위한 클래스
 *
 *
 */
@Service
public class NoticeService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    NoticeDao noticeDao;

    /* 공지사항 상세 */
    public ResponseHandler<NoticeInfoRes> selectNoticeInfo(NoticeInfoReq req) {
        final ResponseHandler<NoticeInfoRes> result = new ResponseHandler<>();

        Map<String, Object> params = ConvertUtil.convertObjectToMap(req);
        try{
            NoticeInfoRes NoticeInfoRes = noticeDao.selectNoticeInfo(params);

            /* 공지사항 조회수 증가 */
            noticeDao.updateNoticeViewCnt(NoticeInfoRes);

            //이전
            req.setType(1);
            PreNextVO prev = noticeDao.preNextNotice(req);

            //다음
            req.setType(2);
            PreNextVO next = noticeDao.preNextNotice(req);

            NoticeInfoRes.setPrev(prev);
            NoticeInfoRes.setNext(next);

            result.setData(NoticeInfoRes);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e){
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }
        return result;
    }

    /* 공지사항 목록 */
    public ResponseHandler<NoticeListRes> selectNotice(NoticeListReq req) {
        final ResponseHandler<NoticeListRes> result = new ResponseHandler<>();

        req.setStartRow();
        Map<String, Object> params = ConvertUtil.convertObjectToMap(req);

        try {
            List<NoticeVO> list = noticeDao.selectNotice(params);
            int totalCnt = noticeDao.selectNoticeCnt(params);

            NoticeListRes NoticeListRes = new NoticeListRes();
            if(CommonUtil.isNotEmpty(list)){
                NoticeListRes.setNoticeList(list);
                NoticeListRes.setTotalCnt(totalCnt);
                NoticeListRes.setIsEndPage(req, totalCnt);
                result.setData(NoticeListRes);
                result.setReturnCode(ReturnType.RTN_TYPE_OK);
            } else {
                result.setReturnCode(ReturnType.RTN_TYPE_NO_DATA);
            }
            NoticeListRes.setIsEndPage(req, totalCnt);

        } catch (Exception e){
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            e.printStackTrace();
        }
        return result;
    }

}

