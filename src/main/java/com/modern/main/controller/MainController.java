package com.modern.main.controller;

import com.modern.common.dto.res.ResponseHandler;
import com.modern.main.service.MainService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Random;


/**
 *  메인 Rest Controller class
 *
 *  사용자를 제어하기 위한 REST API가 정의 되어 있다.
 *
 *
 */
@RestController
@RequestMapping(value="/main" ) //모듈명 정의
@Api(value = "MainController", description = "Main 관련 API")
public class MainController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
    MainService mainService;


	/**
	 * 홈 지수 조회
	 *
	 *
	 * @param : CategoryListReq req
	 * @return : ResponseHandler
	 */
	@ApiOperation(value = "메인 지수 조회")
	@GetMapping(value = "/marketcode/index")
	public ResponseHandler<?> marketcodeIndex() {
		ResponseHandler<?> result = mainService.selectMarketcodeHistory();
		return  result;
	}


	/**
	 * 홈 지수 조회
	 *
	 *
	 * @param : CategoryListReq req
	 * @return : ResponseHandler
	 */
	@ApiOperation(value = "메인 지수 조회")
	@GetMapping(value = "/test")
	public ResponseHandler<?> getRandomEnCapNumber() {
		ResponseHandler<?> result = new ResponseHandler<>();

//		int ran = (int)(Math.random()*4)+1;
//		System.out.println("ran #### " + ran);
//		String name = mainService.selectTest(ran);
//		System.out.println("name #### " + name);
//
//		String rs = "";
//		Random rnd =new Random();
//
//		StringBuffer buf =new StringBuffer();
//
//		for(int i=0;i<4;i++) {
//			if (rnd.nextBoolean()) {
//				buf.append((char) ((int) (rnd.nextInt(26)) + 65));
//			} else {
//				buf.append((rnd.nextInt(10)));
//			}
//		}
//		rs = buf.toString();
//		System.out.println(rs);

		return  result;
	}

}
