package com.modern.main.dao;

import com.modern.main.dto.model.MajorMarketcodeHistoryVO;
import com.modern.main.dto.req.MajorMarketcodeHistoryReq;
import com.modern.main.dto.res.MajorMarketcodeHistoryRes;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface MainDao
{
    List<MajorMarketcodeHistoryVO> selectMarketcodeHistory(MajorMarketcodeHistoryReq req);

    String selectMaxApiReceiveDt();

    String selectTest(int ran);
}
