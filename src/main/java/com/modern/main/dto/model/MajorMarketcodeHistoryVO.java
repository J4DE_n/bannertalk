package com.modern.main.dto.model;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="marketCodeHistory VO")
public class MajorMarketcodeHistoryVO implements Serializable {
    private String marketcode;
    private String marketcodeNm;
    private String trdPrc;
    private String cmpprevddTpCd;
    private String cmpprevddPrc;
    private String accTrdvol;
    private String accTrdval;
    private String isuCnt;
    private String listShrs;
    private String mktcap;
}




