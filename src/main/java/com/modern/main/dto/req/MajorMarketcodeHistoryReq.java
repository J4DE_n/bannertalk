package com.modern.main.dto.req;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel(value ="주요지수")
public class MajorMarketcodeHistoryReq implements Serializable {
    @ApiParam(value = "주요지수 max날짜", hidden=true)
    private String maxDt;
}
