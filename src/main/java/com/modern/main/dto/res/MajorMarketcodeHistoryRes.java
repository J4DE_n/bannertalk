package com.modern.main.dto.res;
import com.modern.main.dto.model.MajorMarketcodeHistoryVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(value ="지수 조회")
public class MajorMarketcodeHistoryRes implements Serializable {
    @ApiParam(value = "지수 결과")
    private List<MajorMarketcodeHistoryVO> list;
}
