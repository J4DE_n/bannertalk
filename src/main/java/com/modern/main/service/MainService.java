package com.modern.main.service;

import com.modern.comCode.service.ComService;
import com.modern.common.config.ConfigCommon;
import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.type.ReturnType;
import com.modern.main.dao.MainDao;
import com.modern.main.dto.req.MajorMarketcodeHistoryReq;
import com.modern.main.dto.res.MajorMarketcodeHistoryRes;
import com.modern.membership.service.MembershipService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * Main을 위한 클래스
 *
 *
 */
@Service
public class MainService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    MainDao mainDao;

    /* 지수 조회 */
    public ResponseHandler<MajorMarketcodeHistoryRes> selectMarketcodeHistory() {
        final ResponseHandler<MajorMarketcodeHistoryRes> result = new ResponseHandler<>();
        try {
            MajorMarketcodeHistoryReq req = new MajorMarketcodeHistoryReq();
            String maxDt = mainDao.selectMaxApiReceiveDt();
            req.setMaxDt(maxDt);
            MajorMarketcodeHistoryRes res = new MajorMarketcodeHistoryRes();
            res.setList(mainDao.selectMarketcodeHistory(req));

            result.setData(res);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }
        return result;
    }

    public String selectTest(int ran) {
        System.out.println("service ran ### " + ran);
        try {
            String name = mainDao.selectTest(ran);
            System.out.println("name ### " + name);
            return name;
        } catch (Exception e) {
            return "error";
        }
    }
}

