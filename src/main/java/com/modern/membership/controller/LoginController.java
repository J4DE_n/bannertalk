package com.modern.membership.controller;

import com.modern.common.dto.res.ResponseHandler;
import com.modern.membership.dto.req.LoginReq;
import com.modern.membership.dto.res.SessionRes;
import com.modern.membership.service.LoginService;
import com.modern.membership.service.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@RestController
public class LoginController {

    @Autowired
    LoginService loginService;

    @Autowired
	ManagerService managerService;

    @PostMapping(value = "/login")
	public ResponseHandler<SessionRes> loginOauth(@Valid @RequestBody(required = false) final LoginReq req, HttpServletResponse response) {
		ResponseHandler<SessionRes> result = loginService.loginOauth(req, response);

		return result;
	}

	@PostMapping(value = "/logout")
	public ResponseHandler logout(HttpServletResponse response) {
		ResponseHandler result = loginService.logout(response);

		return result;
	}

	@PostMapping(value = "/admin/login")
	public ResponseHandler adminLoginOauth(@Valid @RequestBody(required = false) final LoginReq req, HttpServletResponse response) {
    	ResponseHandler result = loginService.adminLoginOauth(req, response);
		return result;
	}

	@PostMapping(value = "/admin/logout")
	public ResponseHandler adminLogout(HttpServletResponse response) {
		ResponseHandler result = loginService.adminLogout(response);

		return result;
	}

	/**
	 * 관리자 세션 조회
	 * @param request
	 * @param response
	 * @return
	 */
	@GetMapping(value = "/session/manager")
	public ResponseHandler<SessionRes> getSessionInfo(HttpServletRequest request, HttpServletResponse response) {
		ResponseHandler<SessionRes> result = managerService.getSessionInfo(request, response);
		return result;
	}

	/**
	 * 관리자 세션 조회2
	 * @param request
	 * @param response
	 * @return
	 */
	@GetMapping(value = "/session/admin")
	public ResponseHandler<SessionRes> getASessionInfo(HttpServletRequest request, HttpServletResponse response) {
		ResponseHandler<SessionRes> result = managerService.getSessionInfo(request, response);
		return result;
	}
}
