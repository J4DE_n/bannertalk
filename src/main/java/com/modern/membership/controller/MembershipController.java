package com.modern.membership.controller;

import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.file.config.ConfigFile;
import com.modern.common.file.service.FileService;
import com.modern.common.session.SessionCheck;
import com.modern.common.type.ReturnType;
import com.modern.membership.config.ConfigMembership;
import com.modern.membership.dto.model.UserInfoVO;
import com.modern.membership.dto.req.*;
import com.modern.membership.dto.res.SessionRes;
import com.modern.membership.service.ManagerService;
import com.modern.membership.service.MembershipService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;


/**
 *  멤버쉽 Rest Controller class
 *
 *  사용자를 제어하기 위한 REST API가 정의 되어 있다.
 *
 *
 */
@RestController
@RequestMapping(value="/membership" )
public class MembershipController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ConfigMembership configMembership;

	@Autowired
	MembershipService membershipService;

	@Autowired
    FileService fileService;

	@Autowired
    ConfigFile configFile;

	@Autowired
	ManagerService managerService;

	/**
	 * 회원가입
	 * @param req
	 * @return
	 */
	@ApiOperation(value = "회원가입")
	@PostMapping(value = "/signUp")
	public ResponseHandler<?> signUp(@Valid @RequestBody(required = false) SignupReq req) {
		ResponseHandler<?> result = membershipService.signUp(req);
		return result;
	}

	/**
	 * 회원가입
	 * @param req
	 * @return
	 */
	@ApiOperation(value = "회원탈퇴")
	@PostMapping(value = "/withdraw")
	@SessionCheck
	public ResponseHandler<?> withdraw(@Valid @RequestBody(required = false) WithdrawReq req) {
		ResponseHandler<?> result = membershipService.withdraw(req);
		return result;
	}

	/**
	 * 유저 존재 확인
	 * @param req
	 * @return
	 */
	@ApiOperation(value = "유저 존재 확인")
	@GetMapping(value = "/duple")
	public ResponseHandler<?> checkExistUser(UserInfoReq req) {
		ResponseHandler<?> result = new ResponseHandler<>();
		boolean userCheck = membershipService.checkExistInUser(req);
		if(userCheck){
			result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_USER_EXIST_NG);
		}else{
			result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_USER_NO_EXIST_NG);
		}
		return result;
	}

	/**
	 * 코등 비교 후 인증 처리
	 * @param req
	 * @return
	 */
	@ApiOperation(value = "코드 비교 후 인증 처리")
	@PutMapping(value = "/code")
	public ResponseHandler<?> codeCheck(@RequestBody CodeCheckReq req){
		ResponseHandler<?> result = membershipService.codeCheck(req);
		return result;
	}

    /**
     * 코등 비교 후 인증 처리
     * @param resend
     * @return
     */
    @ApiOperation(value = "코드 재전송")
    @PutMapping(value = "/code/resend")
    public ResponseHandler<?> codeResend(@Valid @RequestBody EmailResendReq resend){
        ResponseHandler<?> result = membershipService.codeResend(resend);
        return result;
    }

	/**
	 * 비밀번호 찾기
	 * @param req
	 * @return
	 */
	@ApiOperation(value="비밀번호 찾기")
	@GetMapping(value = "/find/pwd")
	public ResponseHandler<?> findPwd(@Valid FindPwdReq req) {
		ResponseHandler<?> result = membershipService.findPwd(req);
		return result;
	}

	/**
	 * 비밀번호 찾기 이후 비밀번호 변경
	 * @param req
	 * @return
	 */
	@ApiOperation(value="비밀번호 찾기 이후 비밀번호 변경")
    @PutMapping(value="/find/pwd/verify")
    public ResponseHandler<?> chgPwdByVerifyKey(@Valid @RequestBody(required=false) ChangePasswordByCodeReq req) {
        ResponseHandler<?> result = membershipService.changePwdByVerifyKey(req);
        return result;
    }

	/**
	 * 비밀번호 찾기 이후 비밀번호 변경
	 * @param req
	 * @return
	 */
	@ApiOperation(value="비밀번호 찾기 이후 비밀번호 변경")
	@PutMapping(value="/myinfo/pwd/update")
	@SessionCheck
	public ResponseHandler<?> UpdateMyPwd(@Valid @RequestBody(required=false) ChangePasswordReq req) {
		ResponseHandler<?> result = membershipService.UpdateMyPwd(req);
		return result;
	}

	/**
	 * 이메일 인증 메일 발송
	 * @param req
	 * @return
	 */
//	@ApiOperation(value="이메일 인증 메일 발송")
//	@PostMapping(value = "/send/verifyKey")
//	@SessionCheck
//	public ResponseHandler<?> sendEmailVerifySession(CommonReq req) {
//		ResponseHandler<?> result = membershipService.sendEmailVerifySession(req);
//		return result;
//	}

	/**
	 * 이메일 인증 메일 재발송
	 * @param req
	 * @return
	 */
//	@ApiOperation(value="이메일 재발송")
//	@GetMapping(value = "/send/resend")
//	public ResponseHandler<?> sendEmailResend(EmailResendReq req) {
//		ResponseHandler<?> result = membershipService.resendMail(req);
//		return result;
//	}

	/**
	 * 이메일 인증
	 * @param req
	 * @return
	 */
//	@ApiOperation(value="이메일 인증")
//	@PostMapping(value = "/email/certify")
//	public ResponseHandler<?> emailCertify(@Valid @RequestBody(required=false) EmailVerify req) {
//		ResponseHandler<?> result = membershipService.emailCertify(req);
//		return result;
//	}

	/**
	 * 마이페이지 정보 조회
	 * @param req
	 * @return
	 */
	@ApiOperation(value="마이페이지 정보 조회")
	@GetMapping(value = "/getMyInfo")
	@SessionCheck
	public ResponseHandler<UserInfoVO> getMyInfo(@Valid MyPageReq req) {
		ResponseHandler<UserInfoVO> result = membershipService.getMyInfo(req);
		return result;
	}

	/**
	 * 마이페이지 정보 조회
	 * @param req
	 * @return
	 */
	@ApiOperation(value="마이페이지 정보 조회")
	@GetMapping(value = "/myInfo/check")
	@SessionCheck
	public ResponseHandler<?> myInfoCheck(@Valid MyInfoCheckReq req) {
		ResponseHandler<?> result = membershipService.myInfoCheck(req);
		return result;
	}

	/**
	 * 마이페이지 정보 업데이트
	 * @param req
	 * @return
	 */
	@ApiOperation(value="마이페이지정보 업데이트")
	@PutMapping(value = "/putMyInfo")
	@SessionCheck
	public ResponseHandler<?> putMyInfo(@Valid @RequestBody(required=false) UpdateUserInfoReq req) {
		ResponseHandler<?> result = membershipService.putMyInfo(req);
		return result;
	}

	/**
	 * 마이페이지 정보 업데이트
	 * @param req
	 * @return
	 */
	@ApiOperation(value="마이페이지 프로모션 업데이트")
	@PutMapping(value = "/promotion/modify")
	@SessionCheck
	public ResponseHandler<?> promoUpdate(@Valid @RequestBody(required=false) PromoModifyReq req) {
		ResponseHandler<?> result = membershipService.promoUpdate(req);
		return result;
	}

	/**
	 *
	 * @param req
	 * @return
	 */
//	@ApiOperation(value="유저정보 업데이트")
//	@PostMapping(value = "/myPage/user")
//	@SessionCheck
//	public ResponseHandler<?> UpdateMyPageUser(@Valid @RequestBody(required=false) UpdateUserInfoReq req) {
//		ResponseHandler<?> result = membershipService.UpdateUser(req);
//		return result;
//	}

	/**
	 * 프로필 업데이트
	 * @param req
	 * @return
	 */
//	@ApiOperation(value="프로필 업데이트")
//	@PostMapping(value = "/myPage/profile")
//    @SessionCheck
//	public ResponseHandler<?> UpdateMyPageProfile(@Valid @RequestBody(required=false) ProfileReq req) {
//		ResponseHandler<?> result = membershipService.UpdateProfile(req);
//		return result;
//	}

	/**
	 * 비밀번호 변경
	 * @param req
	 * @return
	 */
//	@ApiOperation(value="비밀번호 변경")
//	@PostMapping(value = "/updateMyPwd")
//	@SessionCheck
//	public ResponseHandler<?> UpdateMyPwd(@Valid @RequestBody(required=false) ChangePasswordReq req) {
//		ResponseHandler<?> result = membershipService.UpdateMyPwd(req);
//		return result;
//	}

	/**
	 * 세션 정보 조회
	 * @param request
	 * @param response
	 * @return
	 */
	@ApiOperation(value="세션 정보 조회")
	@GetMapping(value = "/session")
	public ResponseHandler<SessionRes> getSessionInfo(HttpServletRequest request, HttpServletResponse response) {
		ResponseHandler<SessionRes> result = membershipService.getSessionInfo(request, response);
		return result;
	}

	/**
	 * 유저 시그널 조회
	 * @param req
	 * @return res
	 */
//	@ApiOperation(value="유저 시그널 조회")
//	@GetMapping(value = "/check/mySignal")
//	public ResponseHandler<UserSignalRes> checkMySignal(UserSignalReq req){
//		ResponseHandler<UserSignalRes> result = membershipService.selectUserSignal(req);
//		return result;
//	}
}


