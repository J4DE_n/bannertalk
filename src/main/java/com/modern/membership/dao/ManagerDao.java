package com.modern.membership.dao;

import com.modern.admin.manager.dto.model.ManagerListVO;
import com.modern.admin.manager.dto.req.ManagerListReq;
import com.modern.membership.dto.model.ManagerInfoVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ManagerDao
{

    void insertManagerInfo(ManagerInfoVO managerInfoVO);
    ManagerInfoVO selectManagerInfo(ManagerInfoVO req);

    List<ManagerListVO> selectManager(ManagerListReq req);
    int selectManagerCnt(ManagerListReq req);

    void updateManagerDelYn(ManagerInfoVO managerInfoVO);
}
