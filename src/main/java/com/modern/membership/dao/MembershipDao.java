package com.modern.membership.dao;

import com.modern.common.dto.req.CommonReq;
import com.modern.membership.dto.model.UserInfoVO;
import com.modern.membership.dto.req.*;
import com.modern.membership.dto.res.MyInfoCheckRes;
import com.modern.membership.dto.res.MyInfoRes;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface MembershipDao
{
    UserInfoVO selectUserInfo(UserInfoReq req);
    UserInfoVO selectUser(UserInfoReq req);

    UserInfoVO selectSessionInfo(UserInfoReq req);

    void insertUser(SignupReq req);
    void insertProfile(SignupReq req);
    void updateUser(UserInfoVO req);
    void updateProfile(ProfileReq req);

//    int selectUserSignalCnt(UserSignalReq req);
//
//    List<UserInfoVO> selectReciptSignalUserList(UserSignalListReq search);
//
//    String getRecommendYn(CommonReq req);

    MyInfoRes selectCodeCheck(CodeCheckReq req);

    void updateCertificateYn(CodeCheckReq req);

    void updateCertificateCode(EmailResendReq resend);

    void updateUserLoginInfo(UserInfoVO updVO);

    MyInfoCheckRes selectExistUser(MyInfoCheckReq req);

}
