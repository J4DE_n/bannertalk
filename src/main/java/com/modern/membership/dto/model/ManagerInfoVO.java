package com.modern.membership.dto.model;
import com.modern.common.dto.model.ReturnVO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

@Data
@ApiModel(value ="관리자 VO")
public class ManagerInfoVO extends ReturnVO implements Serializable {
    private Integer userSeq;
    private String userId;
    private String userPwd;
    private int auth;
    private String certificateYn;
    private String verifyCode;
    private String verifyCodeExpirationDt;
    private String verifyPwdKey;
    private String pendingYn;
    private String pendingReason;
    private Integer loginCnt;
    private String loginLasttime;

    private String userNm;
    private String engNm;
    private String tel;
    private String birthday;
    private String gender;
    private String kakaoId;
    private String intro;
    private String greeting;
    private Integer fileGrpSeq;

    private MultipartFile file;
    private String filePath;
    private String orgFileNm;
    private String fileSeq;


    private Integer sessionSeq;
    private String delYn;
}