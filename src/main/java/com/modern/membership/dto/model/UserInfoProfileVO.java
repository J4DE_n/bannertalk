package com.modern.membership.dto.model;

import lombok.Data;

@Data
public class UserInfoProfileVO {

    private Integer userSeq;
    private String nickName;
    private String userName;
    private String dutyPosition;
    private Integer fileGrpSeq;
    private String gender;
    private String tel;
    private Integer birthday;
    private String childrenYn;
    private String department;
    private String introduction;
    private String homepageLink;
    private String newsletterYn;
    private Integer updId;
    private String updDt;

}