package com.modern.membership.dto.model;

import lombok.Data;

@Data
public class UserInfoVO {

    /*  TB_USER */
    private Integer userSeq;  // 유저 번호
    private String userId; //  userId
    private String userPwd; // 비밀번호
    private Integer auth; // 권한
    private String certificateYn; // 인증여부
    private String verifyCode; // 인증번호
    private String verifyCodeExpirationDt; // 인증번호만료시간
    private String verifyPwdKey; //  비밀번호 변경인증키
    private String pendingYn; // 정지 여부
    private String pendingReason; // 정지 일시
    private String resignYn; // 탈퇴 여부
    private String resignReason; // 탈퇴 이유
    private Integer loginCnt; // 로그인 횟수
    private String loginLastTime; // 마지막 로그인 일
    private String userNm;
    private String tel;
    private String birthday;
    private String gender;
    private String kakaoIdExistYn;
    private String kakaoId;
    private String termsAgreeYn;
    private String personalAgreeYn;
    private String etcAgreeYn;
    private String addrSelfYn;
    private String addr;
    private String addrMajor;
    private String addrMinor;
    private String fileGrpSeq;
    private String delYn; // 삭제 여부
    private Integer creId; // 작성자
    private String creDt; // 작성 시간
    private Integer updId; // 작성자
    private String updDt; // 작성 시간
    private boolean isUpdLastTime = false; // 마지막 로그인 일시 등록 여부

}