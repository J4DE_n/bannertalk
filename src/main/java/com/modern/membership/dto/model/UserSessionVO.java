package com.modern.membership.dto.model;

import lombok.Data;

@Data
public class UserSessionVO {
    private String userSeq;
    private String userId;
    private String userNm;
    private Integer auth;
}