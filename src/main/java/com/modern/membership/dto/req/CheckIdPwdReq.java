package com.modern.membership.dto.req;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "회원정보")
public class CheckIdPwdReq implements Serializable {

    @ApiParam(value = "아이디")
    private String userId;

    @ApiParam(value = "비밀번호")
    private String userPwd;

}
