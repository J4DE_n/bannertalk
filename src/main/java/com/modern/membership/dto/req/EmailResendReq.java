package com.modern.membership.dto.req;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "이메일 발송")
public class EmailResendReq implements Serializable {

    @ApiParam(value = "회원 아이디")
    private String userId;

    @ApiParam(value = "인증 코드")
    private String verifyCode;

    @ApiParam(value = "코드 유효 시간")
    private String verifyCodeExpirationDt;

}
