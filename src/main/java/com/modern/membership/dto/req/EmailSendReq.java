package com.modern.membership.dto.req;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "이메일 발송")
public class EmailSendReq implements Serializable {

    @ApiParam(value = "회원번호", required = true)
    private Integer userSeq;

    @ApiParam(value = "아이디", required = true)
    private String userId;

    @ApiParam(value = "인증 코드", required = true)
    private String verifyCode;

    @ApiParam(value = "인증 링크", required = true)
    private String verifyCodeLink;

}
