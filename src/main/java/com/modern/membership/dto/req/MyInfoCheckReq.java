package com.modern.membership.dto.req;
import com.modern.common.dto.req.CommonReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "회원가입 파라미터")
public class MyInfoCheckReq extends CommonReq implements Serializable {

    @ApiParam(value = "아이디", required = true)
    private String userId;

    @ApiParam(value = "비밀번호", required = true)
    private String userPwd;
}
