package com.modern.membership.dto.req;
import com.modern.common.dto.req.CommonReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "회원가입 파라미터")
public class ProfileReq extends CommonReq implements Serializable {

    @ApiParam(value = "아이디")
    private String userId;

    @ApiParam(value = "비밀번호")
    private String userPwd;

    @ApiParam(value = "권한")
    private String auth;

    @ApiParam(value = "인증키")
    private String verifyCode;

    @ApiParam(value = "인증키유효시간")
    private String verifyCodeExpirationDt;

    @ApiParam(value = "인증비밀번호키")
    private String verifyPwdKey;

    @ApiParam(value = "인증여부")
    private String certificateYn;

    @ApiParam(value = "삭제여부")
    private String delYn;

    @ApiParam(value = "수정자 아이디")
    private Integer updId;

    @ApiParam(value = "회원번호")
    private Integer userSeq;

    @ApiParam(value = "정지여부")
    private String pendingYn;

    @ApiParam(value = "정지일")
    private String pendingDt;

    @ApiParam(value = "정지기간")
    private String pendingPeriod;

    @ApiParam(value = "이름")
    private String userNm;

    @ApiParam(value = "생년월일")
    private String birthday;

    @ApiParam(value = "성별")
    private String gender;

    @ApiParam(value = "카카오톡아이디사용여부")
    private String kakaoIdExistYn;

    @ApiParam(value = "카카오톡아이디")
    private String kakaoId;

    @ApiParam(value = "약관동의")
    private String termsAgreeYn;

    @ApiParam(value = "개인정보 동의")
    private String personalAgreeYn;

    @ApiParam(value = "선택동의")
    private String etcAgreeYn;

    @ApiParam(value = "주소집적입력여부")
    private String addrSelfYn;

    @ApiParam(value = "주소")
    private String addr;

    @ApiParam(value = "광역시도")
    private Integer addrMajor;

    @ApiParam(value = "시군구")
    private Integer addrMinor;

    @ApiParam(value = "첨부파일그룹번호")
    private Integer fileGrpSeq;

    @ApiParam(value = "전화번호")
    private String tel;

    @ApiParam(value = "수정일")
    private String updDt;
}
