package com.modern.membership.dto.req;
import com.modern.common.dto.req.CommonReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "프로모션 이메일 설정 여부")
public class PromoModifyReq extends CommonReq implements Serializable {

    @ApiParam(value = "아이디", required = true)
    private String etcAgreeYn;

}
