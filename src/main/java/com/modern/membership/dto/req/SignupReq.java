package com.modern.membership.dto.req;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel(value = "회원가입 파라미터")
public class SignupReq implements Serializable {

    @ApiParam(value = "회원 시퀀스", hidden = true)
    @Ignore
    private Integer userSeq;

    @ApiParam(value = "아이디", required = true)
    @NotNull
    private String userId;

    @ApiParam(value = "비밀번호", required = true)
    @NotNull
    private String userPwd;

    @ApiParam(value = "권한")
    private int auth;

    @ApiParam(value = "인증여부")
    private String certificateYn;

    @ApiParam(value = "인증코드")
    private String verifyCode;

    @ApiParam(value = "인증코드 입력시간")
    private String verifyCodeExpirationDt;

    @ApiParam(value = "이름")
    private String userNm;

    @ApiParam(value = "전화번호")
    private String tel;

    @ApiParam(value = "약관동의")
    private String termsAgreeYn;

    @ApiParam(value = "개인정보 동의")
    private String personalAgreeYn;

    @ApiParam(value = "선택 동의")
    private String etcAgreeYn;

    @ApiParam(value = "삭제여부")
    private String dleYn;

    @ApiParam(value = "삭제여부")
    private Integer creId;

    @ApiParam(value = "삭제여부")
    private String creDt;

    @ApiParam(value = "삭제여부")
    private Integer updId;

    @ApiParam(value = "삭제여부")
    private String updDt;


}
