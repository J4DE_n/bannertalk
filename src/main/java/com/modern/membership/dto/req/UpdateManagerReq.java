package com.modern.membership.dto.req;
import com.modern.common.dto.req.CommonReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
@ApiModel(value ="관리자 수정")
public class UpdateManagerReq extends CommonReq {
    @ApiParam(value = "회원번호", required = true)
    private Integer userSeq;
    @ApiParam(value = "아이디")
    private String userId;
    @ApiParam(value = "이름")
    private String userNm;
    @ApiParam(value = "영어이름")
    private String engNm;
    @ApiParam(value = "전화번호")
    private String tel;
    @ApiParam(value = "생일")
    private String birthday;
    @ApiParam(value = "성별")
    private String gender;
    @ApiParam(value = "카카오아이디")
    private String kakaoId;
    @ApiParam(value = "소개")
    private String intro;
    @ApiParam(value = "인사말")
    private String greeting;
    @ApiParam(value = "프로필 이미지 물리 파일")
    MultipartFile[] files;
    @ApiParam(value = "프로필 이미지")
    private String fileGrpSeq;
    @ApiParam(value = "브랜딩1")
    private String branding1;
    @ApiParam(value = "브랜딩2")
    private String branding2;
    @ApiParam(value = "브랜딩3")
    private String branding3;
    @ApiParam(value = "주요이력")
    private String career;
}