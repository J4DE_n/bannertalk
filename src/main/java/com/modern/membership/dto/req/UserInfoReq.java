package com.modern.membership.dto.req;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "회원정보")
public class UserInfoReq implements Serializable {

    @ApiParam(value = "아이디")
    private String userId;

    @ApiParam(value = "회원번호")
    private Integer userSeq;

    @ApiParam(value = "권한")
    private Integer auth;

    @ApiParam(value = "관리자 권한")
    private Boolean isManagerAuth = false;

    @ApiParam(value = "인증키")
    private String verifyCode;

    @ApiParam(value = "인증키")
    private String verifyCodeExpirationDt;

    @ApiParam(value = "삭제여부")
    private String delYn;

    @ApiParam(value = "등록자")
    private String creId;

    @ApiParam(value = "등록일")
    private String creDt;

    @ApiParam(value = "수정자")
    private String updId;

    @ApiParam(value = "수정일")
    private String updDt;

}
