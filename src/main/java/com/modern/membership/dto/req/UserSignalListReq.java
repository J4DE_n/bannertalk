package com.modern.membership.dto.req;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="유저 시그널 조회")
public class UserSignalListReq implements Serializable {

    @ApiParam(value = "유저 시퀀스")
    private int userSeq;

    @ApiParam(value = "유저 아이디")
    private String userId;

    @ApiParam(value = "유저 클래스")
    private String primaryClassCd;

}
