package com.modern.membership.dto.req;
import com.modern.common.dto.req.CommonReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "회원가입 파라미터")
public class WithdrawReq extends CommonReq implements Serializable {

    @ApiParam(value = "비밀번호")
    private String userPwd;

    @ApiParam(value = "삭제여부")
    private String delYn;

}
