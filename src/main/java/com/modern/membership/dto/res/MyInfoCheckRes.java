package com.modern.membership.dto.res;

import lombok.Data;

@Data
public class MyInfoCheckRes {
    private String userId;
    private String userPwd;
}

