package com.modern.membership.dto.res;

import lombok.Data;

@Data
public class MyInfoRes {
    private String userId;
    private String verifyCode;
    private String verifyCodeExpirationDt;
    private String certificateYn;
}

