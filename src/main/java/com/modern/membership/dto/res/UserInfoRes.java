package com.modern.membership.dto.res;

import com.modern.membership.dto.model.UserInfoVO;
import lombok.Data;

import java.util.List;

@Data
public class UserInfoRes {
    private UserInfoVO userInfo;

    private List<UserInfoVO> reciptUserList;
}

