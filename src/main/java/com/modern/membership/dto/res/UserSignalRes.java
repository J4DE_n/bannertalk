package com.modern.membership.dto.res;

import com.modern.membership.dto.req.UserSignalReq;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "유저 시그널 목록 조회 정보")
public class UserSignalRes {

    private int totalCnt;
    private boolean isEndPage = false;

    public void setIsEndPage(UserSignalReq req, int totalCnt) {
        int endPage = (int) Math.ceil(totalCnt / (double) req.getCntPerPage());
        if(endPage == req.getCurrentPage()){
            this.isEndPage = true;
        }
    }

}
