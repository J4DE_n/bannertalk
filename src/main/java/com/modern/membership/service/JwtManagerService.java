package com.modern.membership.service;

import com.modern.common.config.ConfigCommon;
import com.modern.common.util.ComEncDecUtil;
import com.modern.common.util.ComStaticUtil;
import com.modern.common.util.CommonUtil;
import com.modern.common.util.CookieUtil;
import com.modern.membership.config.ConfigMembership;
import com.modern.membership.dao.ManagerDao;
import com.modern.membership.dao.MemberTokenDao;
import com.modern.membership.dto.model.ManagerInfoVO;
import com.modern.membership.dto.model.UserSessionVO;
import com.modern.membership.dto.model.UserTokenVO;
import com.modern.membership.dto.req.InsertMemberTokenReq;
import com.modern.membership.dto.res.SessionRes;
import io.jsonwebtoken.ExpiredJwtException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service
public class JwtManagerService {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	ConfigMembership configMembership;

	@Autowired
	ManagerDao managerDao;

	@Autowired
	MemberTokenDao memberTokenDao;

	@Autowired
	ConfigCommon configCommon;

	@Autowired
	JwtService jwtService;

	/**
	 * AccessToken Parse 회원 정보 조회
	 * @param accessToken
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public SessionRes getManagerJwtParse(String accessToken, HttpServletRequest request, HttpServletResponse response) {
		SessionRes res = null;
		String refreshToken = "";

		try {
			if(StringUtils.isNotEmpty(accessToken)) {
				String[] array = accessToken.split("[.]");
				refreshToken = ComEncDecUtil.getDecrypted(array[1], configCommon.getAes128Key());
			}
			res = jwtService.getNewJwtParse(accessToken);

		} catch (ExpiredJwtException e) {

			String forever = CookieUtil.getCookie(request, "MTFO");
			if(CommonUtil.isNotEmpty(forever) && "Y".equals(forever)) {
				//새로운 토큰 발급
				try {
					String newAccesstoken = getAccessTokenToRefresh(refreshToken, response);
					res = jwtService.getNewJwtParse(newAccesstoken);
				} catch (Exception ex) {
					logger.error("getJwtParse[Exception][getAccessTokenToRefresh]", ex);
				}
			} else {
				this.removeManagerSession(response);
			}
		} catch (Exception e) {
			logger.error("getJwtParse[Exception]", e);
		}

		return res;
	}

	/**
	 * AccessToken 관리자 쿠키 및 DB저장
	 * @param jwtString
	 * @param response
	 * @param expire
	 * @param userSeq
	 * @throws Exception
	 */
	public String setManagerJwtCookie(String jwtString, HttpServletResponse response, int expire, int userSeq) {
		String newAccessToken = "";

		try {
			if(StringUtils.isNotEmpty(jwtString)) {
				String[] array = jwtString.split("[.]");
				String th = array[0];
				String tpl = array[1];
				String encTpl = ComEncDecUtil.getEncrypted(tpl, configCommon.getAes128Key());
				String tsign = array[2];

				this.setSession(response, th, encTpl, tsign, expire);

				newAccessToken = th + "." + encTpl + "." + tsign;

				//토큰 저장
				InsertMemberTokenReq tokenReq = new InsertMemberTokenReq();
				tokenReq.setUserSeq(userSeq);
				tokenReq.setRefreshToken(tpl);
				tokenReq.setCreId(userSeq);
				memberTokenDao.insertUserToken(tokenReq);
			}
		} catch(Exception e) {
			logger.error("setManagerJwtCookie[Exception]", e);
		}

		return newAccessToken;
	}

	/**
	 * 리프레쉬 토큰으로 AccessToken 조회
	 * @param refreshToken
	 * @return
	 * @throws Exception
	 */
	public String getAccessTokenToRefresh(String refreshToken, HttpServletResponse response) throws Exception {
		String newAccessToken = "";

		if(StringUtils.isNotEmpty(refreshToken)) {
			UserTokenVO userTokenVO= memberTokenDao.selectMemberToken(refreshToken);

			ManagerInfoVO req = new ManagerInfoVO();
			req.setUserSeq(userTokenVO.getUserSeq());
			ManagerInfoVO infoVO = managerDao.selectManagerInfo(req); //관리자

			UserSessionVO sessionVO = new UserSessionVO();
			BeanUtils.copyProperties(sessionVO, infoVO);
			newAccessToken = jwtService.createJwtBuilder(sessionVO);

			//쿠키저장 및 토큰 저장
			int expire = configMembership.getCookie().getExpire();
			newAccessToken = this.setManagerJwtCookie(newAccessToken, response, expire, userTokenVO.getUserSeq());
		}

		return newAccessToken;
	}

	/**
	 * 세션 삭제
	 * @param response
	 */
	public void removeManagerSession(HttpServletResponse response) {
		CookieUtil.removeCookie(response, "MTFO", ComStaticUtil.requestDomain);
		CookieUtil.removeCookie(response, "MTH", ComStaticUtil.requestDomain);
		CookieUtil.removeCookie(response, "MTPL", ComStaticUtil.requestDomain);
		CookieUtil.removeCookie(response, "MTSIGN", ComStaticUtil.requestDomain);
	}

	public void setSession(HttpServletResponse response, String th, String encTpl, String tsign, int expire) {
		CookieUtil.setCookie(response, "MTH", th, ComStaticUtil.requestDomain, expire);
		CookieUtil.setCookie(response, "MTPL", encTpl, ComStaticUtil.requestDomain, expire);
		CookieUtil.setCookie(response, "MTSIGN", tsign, ComStaticUtil.requestDomain, expire);
	}

	/* accesstoken */
	public String getAcessToken(HttpServletRequest request) {
		String th = CookieUtil.getCookie(request, "MTH");
		String tpl = CookieUtil.getCookie(request, "MTPL");
		String tsign = CookieUtil.getCookie(request, "MTSIGN");
		return  th + "." + tpl + "." + tsign;
	}

}
