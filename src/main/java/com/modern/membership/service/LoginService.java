package com.modern.membership.service;

import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.password.PasswordHandler;
import com.modern.common.type.ReturnType;
import com.modern.common.util.ComStaticUtil;
import com.modern.common.util.CommonUtil;
import com.modern.common.util.CookieUtil;
import com.modern.membership.config.ConfigMembership;
import com.modern.membership.dao.ManagerDao;
import com.modern.membership.dao.MembershipDao;
import com.modern.membership.dto.model.ManagerInfoVO;
import com.modern.membership.dto.model.UserInfoVO;
import com.modern.membership.dto.model.UserSessionVO;
import com.modern.membership.dto.req.LoginReq;
import com.modern.membership.dto.req.UserInfoReq;
import com.modern.membership.dto.res.SessionRes;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Service
public class LoginService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    MembershipDao membershipDao;

    @Autowired
    ManagerDao managerDao;

    @Autowired
    MembershipService membershipService;

    @Autowired
    ConfigMembership configMembership;

    @Autowired
    PasswordHandler passwordHandler;

    @Autowired
    JwtService jwtService;

    @Autowired
    JwtManagerService jwtManagerService;

    public ReturnType updateOauth(HttpServletRequest request, HttpServletResponse response) {
        try {

            //1. 현재 세션정보 조회
            ResponseHandler<SessionRes> sessionInfo = membershipService.getSessionInfo(request, response);
            int userSeq = sessionInfo.getData().getUserSeq();
            //2. 자동저장 여부 쿠키 조회
            String forever = CookieUtil.getCookie(request,"TFO" );

            //3. 유저정보 조회
            UserInfoReq infoReq = new UserInfoReq();
            infoReq.setUserSeq(userSeq);
            UserInfoVO userInfoVO = membershipDao.selectSessionInfo(infoReq);
            if (CommonUtil.isNotEmpty(userInfoVO)) {
                //4. 쿠키 삭제
                jwtService.removeSession(response);
                int expire = 0;
                if((CommonUtil.isNotEmpty(forever) && "Y".equals(forever))) {
                    expire = configMembership.getCookie().getExpire();
                    CookieUtil.setCookie(response, "TFO", forever, ComStaticUtil.requestDomain, expire);
                } else {
                    expire = -1;
                }

                //5. 쿠키 다시 굽기
                /*Map<String, Object> map = new HashMap<>();
                map.put("userSeq", userInfoVO.getUserSeq());
                map.put("userId", userInfoVO.getUserId());
                //map.put("userName", userInfoVO.getUserName());
                map.put("authority", userInfoVO.getAuth());*/
                UserSessionVO sessionVO = new UserSessionVO();
                BeanUtils.copyProperties(sessionVO, userInfoVO);
                String jwtString = jwtService.createJwtBuilder(sessionVO);

                //쿠키 저장
                jwtService.setJwtCookie(jwtString, response, expire, userInfoVO.getUserSeq());
                return ReturnType.RTN_TYPE_OK;
            } else {
                return ReturnType.RTN_TYPE_MEMBERSSHIP_USER_EXIST_NG;
            }
        } catch(Exception e) {
            logger.error("updateOauth [Exception]", e);
            return ReturnType.RTN_TYPE_NG;
        }
    }

    public ResponseHandler<SessionRes> loginOauth(LoginReq req, HttpServletResponse response) {
        ResponseHandler<SessionRes> result = new ResponseHandler();
        SessionRes sessionRes = new SessionRes();
        try {

            /* 사용자 정보 조회 */
            UserInfoReq userReq = new UserInfoReq();
            userReq.setUserId(req.getUserId());
            userReq.setAuth(configMembership.getSelectAuthorityUserNo());
            userReq.setIsManagerAuth(false);
            UserInfoVO userInfoVO = membershipDao.selectSessionInfo(userReq);

            if(ObjectUtils.isEmpty(userInfoVO)){
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_USER_NO_EXIST_NG);
                return result;
            }

            /* 비밀번호 체크 */
            String pwd = req.getUserPwd();
            String encPassword = userInfoVO.getUserPwd();
            if(!passwordHandler.matches(pwd, encPassword)) {
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_PASSWORD_ENC_NG);
                return result;
            }

            /* 인증여부 */
            if(!"Y".equals(userInfoVO.getCertificateYn())) {
                sessionRes.setUserId(userInfoVO.getUserId());
                result.setData(sessionRes);
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_EMAIL_UNVERIFY);
                return result;
            }

            UserSessionVO sessionVO = new UserSessionVO();
            sessionVO.setUserSeq(String.valueOf(userInfoVO.getUserSeq()));
            sessionVO.setUserId(userInfoVO.getUserId());
            sessionVO.setUserNm(userInfoVO.getUserNm());
            sessionVO.setAuth(userInfoVO.getAuth());

            String jwtString = jwtService.createJwtBuilder(sessionVO);

            int expire = 0;
            if((CommonUtil.isNotEmpty(req.getForever()) && "Y".equals(req.getForever()))) {
                expire = configMembership.getCookie().getExpire();
                CookieUtil.setCookie(response, "TFO", req.getForever(), ComStaticUtil.requestDomain, expire);
            } else {
                expire = -1;
            }

            //쿠키 저장
            jwtService.setJwtCookie(jwtString, response, expire, userInfoVO.getUserSeq());

            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch(Exception e) {
            logger.error("loginOauth[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            return result;
        }

        return result;
    }

    public ResponseHandler logout(HttpServletResponse response) {
        ResponseHandler result = new ResponseHandler();
        try {

            jwtService.removeSession(response);

            result.setReturnCode(ReturnType.RTN_TYPE_OK);

        } catch(Exception e) {
            logger.error("logout[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            return result;
        }

        return result;
    }

    public ResponseHandler adminLogout(HttpServletResponse response) {
        ResponseHandler result = new ResponseHandler();
        try {

            jwtManagerService.removeManagerSession(response);

            result.setReturnCode(ReturnType.RTN_TYPE_OK);

        } catch(Exception e) {
            logger.error("logout[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            return result;
        }

        return result;
    }

    /* 관리자 로그인 */
    public ResponseHandler adminLoginOauth(@Valid LoginReq req, HttpServletResponse response) {
        ResponseHandler result = new ResponseHandler();
        try {

            //1. 관리자 정보 조회
            ManagerInfoVO managerInfoVO = new ManagerInfoVO();
            managerInfoVO.setUserId(req.getUserId());
            managerInfoVO = managerDao.selectManagerInfo(managerInfoVO);

            if(ObjectUtils.isEmpty(managerInfoVO)){
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_USER_EXIST_NG);
                return result;
            }

            //2. 비밀번호 체크
            String pwd = req.getUserPwd();
            String encPassword = managerInfoVO.getUserPwd();
            UserInfoVO updVO = new UserInfoVO();
            updVO.setUserSeq(managerInfoVO.getUserSeq());
            int loginCnt = 0;
            if(!passwordHandler.matches(pwd, encPassword)) {
                //2-1. 로그인 시도 횟수 업데이트
                loginCnt = managerInfoVO.getLoginCnt()+1;
                updVO.setLoginCnt(loginCnt);
                membershipDao.updateUserLoginInfo(updVO);
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_USER_EXIST_NG);
                return result;
            }

            //2-2. 로그인 시도 횟수 초기화
            if(managerInfoVO.getLoginCnt() > 0){
                updVO.setLoginCnt(loginCnt);
                membershipDao.updateUserLoginInfo(updVO);
            }

            //3. 제한여부 확인 (인증, 펜딩)
            if(!"Y".equals(managerInfoVO.getCertificateYn())) {
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_EMAIL_UNVERIFY);
                return result;
            }
            if(!"N".equals(managerInfoVO.getPendingYn())) {
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_USER_ID_PENDING_NG);
                return result;
            }

            // 3. 권한 체크
            int auth = managerInfoVO.getAuth();
            if( auth <= configMembership.getSelectAuthorityContentsAdminNo() ){
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_AUTHORITY_NG);
                return result;
            }

            UserSessionVO sessionVO = new UserSessionVO();
            BeanUtils.copyProperties(sessionVO, managerInfoVO);

            String jwtString = jwtService.createJwtBuilder(sessionVO);

            int expire = 0;
            if((CommonUtil.isNotEmpty(req.getForever()) && "Y".equals(req.getForever()))) {
                expire = configMembership.getCookie().getExpire();
                CookieUtil.setCookie(response, "MTFO", req.getForever(), ComStaticUtil.requestDomain, expire);
            } else {
                expire = -1;
            }

            //쿠키 저장
            jwtManagerService.setManagerJwtCookie(jwtString, response, expire, managerInfoVO.getUserSeq());

            //4.로그인 성공시 마지막 로그인일시 업데이트
            updVO.setUpdLastTime(true);
            membershipDao.updateUserLoginInfo(updVO);

            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch(Exception e) {
            logger.error("loginOauth[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            return result;
        }

        return result;
    }

    /*public ResponseHandler adminLoginOauth(@Valid LoginReq req, HttpServletResponse response, HttpSession session) {
        ResponseHandler result = new ResponseHandler();
        try {

            UserInfoReq userReq = new UserInfoReq();
            userReq.setUserId(req.getUserId());
            UserInfoVO userInfoVO = membershipDao.selectUserInfo(userReq);

            if(ObjectUtils.isEmpty(userInfoVO)){
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_USER_NO_EXIST_NG);
                return result;
            }

            String pwd = req.getUserPwd();
            String encPassword = userInfoVO.getUserPwd();
            if(!passwordHandler.matches(pwd, encPassword)) {
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_PASSWORD_ENC_NG);
                return result;
            }

            int auth = userInfoVO.getAuth();

            Map<String, Object> map = new HashMap<>();
            map.put("userSeq", userInfoVO.getUserSeq());
            map.put("userId", userInfoVO.getUserId());
            //map.put("userName", userInfoVO.getUserName());
            map.put("authority", auth );

            if( auth <= configMembership.getSelectAuthorityContentsAdminNo() ){
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_AUTHORITY_NG);
                return result;
            }

            String jwtString = jwtService.createJwtBuilder(map);

            int expire = 0;
            if((CommonUtil.isNotEmpty(req.getForever()) && "Y".equals(req.getForever()))) {
                expire = configMembership.getCookie().getExpire();
                CookieUtil.setCookie(response, "TFO", req.getForever(), ComStaticUtil.requestDomain, expire);
            } else {
                expire = -1;
            }

            //쿠키 저장
            jwtService.setJwtCookie(jwtString, response, expire, userInfoVO.getUserSeq());

            *//* 관리자 로그인시 문자 보내기 위한 토큰 생성*//*
            SmsUtil.getTokenSignal();

            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch(Exception e) {
            logger.error("loginOauth[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            return result;
        }

        return result;
    }*/
}
