package com.modern.membership.service;

import com.modern.common.config.ConfigCommon;
import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.file.config.ConfigFile;
import com.modern.common.file.dto.model.ThumbnailInfo;
import com.modern.common.file.dto.req.FileDeleteReq;
import com.modern.common.file.dto.req.FileSaveReq;
import com.modern.common.file.dto.res.FileInfoRes;
import com.modern.common.file.service.FileService;
import com.modern.common.mail.MailHandler;
import com.modern.common.mail.dto.res.MailReq;
import com.modern.common.password.PasswordHandler;
import com.modern.common.type.ReturnType;
import com.modern.common.util.ComEncDecUtil;
import com.modern.common.util.CommonUtil;
import com.modern.membership.config.ConfigMembership;
import com.modern.membership.dao.ManagerDao;
import com.modern.membership.dao.MembershipDao;
import com.modern.membership.dto.model.ManagerInfoVO;
import com.modern.membership.dto.model.UserInfoVO;
import com.modern.membership.dto.req.SignupReq;
import com.modern.membership.dto.req.UserInfoReq;
import com.modern.membership.dto.res.SessionRes;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.velocity.VelocityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service
public class ManagerService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ManagerDao managerDao;

    @Autowired
    MembershipDao membershipDao;

    @Autowired
    FileService fileService;

    @Autowired
    ConfigFile configFile;

    @Autowired
    PasswordHandler passwordHandler;

    @Autowired
    ConfigCommon configCommon;

    @Autowired
    ConfigMembership configMembership;

    @Autowired
    MailHandler mailHandler;

    @Autowired
    MembershipService membershipService;

    @Autowired
    JwtManagerService jwtManagerService;

    /**
     * 관리자 정보 신규 저장
     *
     * @param
     * @return
     */
    @Transactional

    // InsertManagerReq
    public ManagerInfoVO insertManagerInfo(ManagerInfoVO req) throws Exception{
        //1. 중복아이디 체크
        UserInfoReq userInfoReq = new UserInfoReq();
        userInfoReq.setUserId(req.getUserId());
        userInfoReq.setAuth(configMembership.getSelectAuthorityUserNo());
        userInfoReq.setIsManagerAuth(true);
        boolean userCheck = membershipService.checkExistInUser(userInfoReq);
        if(userCheck){
            req.setCode(ReturnType.RTN_TYPE_MEMBERSSHIP_USER_EXIST_NG);
            return req;
        }

        //2. 회원정보 저장 (비밀번호 x)
        SignupReq userReq = new SignupReq();
        BeanUtils.copyProperties(userReq, req);
        membershipDao.insertUser(userReq);

        //3. 관리자 정보 저장
        req.setUserSeq(userReq.getUserSeq());
        return this.saveManagerInfo(req);
    }

    /**
     * 관리자 정보 업데이트
     *
     * @param
     * @return
     */
    @Transactional
    public ManagerInfoVO updateManagerInfo(ManagerInfoVO req) throws Exception {
        //1. 기존 관리자 정보 조회
        ManagerInfoVO selectvVO = new ManagerInfoVO();
        selectvVO.setUserSeq(req.getUserSeq());
        ManagerInfoVO info = managerDao.selectManagerInfo(selectvVO);

        //2. 관리자 정보 저장
        if(info != null){
            req.setFileGrpSeq(info.getFileGrpSeq());
        }

        return this.saveManagerInfo(req);
    }

    /**
     * 관리자 정보 저장
     *
     * @param
     * @return
     */
    public ManagerInfoVO saveManagerInfo(ManagerInfoVO req) throws Exception {
        //관리자 프로필 수정
        FileInfoRes profieImg = this.setProfileImg(req.getFile(), req.getSessionSeq(), req.getFileGrpSeq());

        //관리자 정보 업데이트
        if(CommonUtil.isNotEmpty(profieImg) && profieImg.getFileGrpSeq() > 0){
            //프로필 이미지 그룹 시퀀스 세팅
            req.setFileGrpSeq(profieImg.getFileGrpSeq());
        }
        managerDao.insertManagerInfo(req);
        req.setCode(ReturnType.RTN_TYPE_OK);
        return req;
    }


    /**
     * 관리자 프로필 이미지 저장
     *
     * @param
     * @return
     */
    public FileInfoRes setProfileImg(MultipartFile file, Integer sessionSeq, Integer fileGrpSeq) throws Exception {
        if(file != null && !file.isEmpty()) {
            ThumbnailInfo thumbnailInfo = new ThumbnailInfo();
            thumbnailInfo.setWidth(100);
            thumbnailInfo.setHeight(100);

            String fileNm = file.getOriginalFilename();
            int pos = fileNm.lastIndexOf( "." );
            thumbnailInfo.setExt(fileNm.substring( pos + 1 ));

            FileSaveReq fileSaveReq = new FileSaveReq();
            fileSaveReq.setCategory(configFile.getSelectCategory0());
            fileSaveReq.setIsSaveInfos(true);
            fileSaveReq.setUserSeq(sessionSeq);
            fileSaveReq.setCheckExtCategory(configFile.getExtCategory1()); //이미지 확장자 체크

            fileSaveReq.setIsThumnail(true);
            fileSaveReq.setThumnailInfo(thumbnailInfo);

            if(fileGrpSeq != null && fileGrpSeq > 0){
                fileSaveReq.setFileGrpSeq(fileGrpSeq);

                //이전 파일 삭제
                FileDeleteReq deleteReq = new FileDeleteReq();
                deleteReq.setCategory(configFile.getSelectCategory0());
                deleteReq.setFileGrpSeq(fileGrpSeq);
                fileService.deleteFiles(deleteReq);
            }
            MultipartFile[] files = {file};
            return fileService.storeFiles(files, fileSaveReq);
        }
        return null;
    }

    /**
     * 관리자 비밀번호 설정메일 발송
     *
     * @param
     * @return
     */
    public ReturnType sendMailManagerPwdSetting(UserInfoVO req) throws Exception {
        try {
            //1. 인증키 생성 및 업데이트
            String verifyPwdKey = passwordHandler.makeRandomKey(6);
            req.setVerifyPwdKey(verifyPwdKey);
            membershipDao.updateUser(req);

            String param = String.format("userId=%s&verifyKey=%s", ComEncDecUtil.getEncrypted(req.getUserId(), configCommon.getAes128Key()), ComEncDecUtil.getEncrypted(verifyPwdKey, configCommon.getAes128Key()));

            //비밀번호 재설정   URL
            String url = configMembership.getEmailUrlHostName() + configMembership.getPasswordConfirAdminmUrl() + "?" + param;

            // 임시 비밀번호를 전송한다.
            MailReq mailReq = new MailReq();
            mailReq.setMailTitle("[모던양장] 관리자 신규 비밀번호 생성 메일입니다.");
            mailReq.setToEmail(req.getUserId());
            mailReq.setVelocityPath("templates/velocity/manager/pwdSetting.vm");
            VelocityContext velocityContext = new VelocityContext();
            velocityContext.put("url", url);
            velocityContext.put("hostUrl", configMembership.getEmailUrlHostName());
            mailHandler.sendVolocityMail(mailReq, velocityContext);

            return ReturnType.RTN_TYPE_OK;
        }catch(Exception e) {
            logger.error("findPwd[Exception]", e);
            return ReturnType.RTN_TYPE_NG;
        }
    }

    /**
     * 세션 정보 조회
     * @param request
     * @param response
     * @return
     */
    public ResponseHandler<SessionRes> getSessionInfo(HttpServletRequest request, HttpServletResponse response) {
        ResponseHandler<SessionRes> result = new ResponseHandler();
        try {

            String accessToken = jwtManagerService.getAcessToken(request);
            if(CommonUtil.isEmpty(accessToken)) {
                result.setReturnCode(ReturnType.RTN_TYPE_SESSION);
                return result;
            }

            SessionRes res = jwtManagerService.getManagerJwtParse(accessToken, request, response);
            if(res == null) {
                result.setReturnCode(ReturnType.RTN_TYPE_SESSION);
                return result;
            }

            result.setReturnCode(ReturnType.RTN_TYPE_OK);
            result.setData(res);
            logger.info("getSessionInfo[result]" + result);

        } catch(Exception e) {
            logger.error("getSessionInfo[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            return result;
        }

        return result;
    }


}