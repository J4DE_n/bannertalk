package com.modern.membership.service;

import com.modern.common.VaildationCheck.ConfigValidationCheck;
import com.modern.common.config.ConfigCommon;
import com.modern.common.dto.req.CommonReq;
import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.mail.MailHandler;
import com.modern.common.mail.dto.res.MailReq;
import com.modern.common.password.PasswordHandler;
import com.modern.common.type.ReturnType;
import com.modern.common.util.*;
import com.modern.membership.config.ConfigMembership;
import com.modern.membership.dao.MembershipDao;
import com.modern.membership.dto.model.UserInfoVO;
import com.modern.membership.dto.req.*;
import com.modern.membership.dto.res.MyInfoCheckRes;
import com.modern.membership.dto.res.MyInfoRes;
import com.modern.membership.dto.res.SessionRes;
import com.modern.membership.dto.res.UserInfoRes;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.velocity.VelocityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.*;

@Service
public class MembershipService implements UserDetailsService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    MembershipDao membershipDao;

    @Autowired
    ConfigMembership configMembership;

    @Autowired
    ConfigCommon configCommon;

    @Autowired
    PasswordHandler passwordHandler;

    @Autowired
    MailHandler mailHandler;

    @Autowired
    ConfigValidationCheck configValidationCheck;

    @Autowired
    GetNowTime getNowTime;

    @Autowired
    JwtService jwtService;

    /**
     * 아이디에 대한 비밀번호 체크
     * @param req
     * @return
     * @throws Exception
     */
    public ReturnType checkIdPwd(CheckIdPwdReq req) throws Exception {
        logger.info("checkIdPwd[req]" + req);

        UserInfoReq infoReq = new UserInfoReq();
        infoReq.setUserId(req.getUserId());
        UserInfoVO userInfoVO = membershipDao.selectUserInfo(infoReq);

        if(!CommonUtil.isExist(userInfoVO)) {
            return ReturnType.RTN_TYPE_MEMBERSSHIP_USERID_NO_EXIST_NG;
        }

        if (!passwordHandler.matches(req.getUserPwd(),userInfoVO.getUserPwd())) {
            return ReturnType.RTN_TYPE_MEMBERSSHIP_PASSWORD_ENC_NG;
        }

        logger.info("checkIdPwd[result]" + userInfoVO);

        return ReturnType.RTN_TYPE_OK;
    }

    /**
     * 스프링 시큐리티 로그인 정보 저장 함수
     *
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserInfoReq infoReq = new UserInfoReq();
        infoReq.setUserId(username);
        UserInfoVO userInfoVO = membershipDao.selectUserInfo(infoReq);

        MembershipInfo member = new MembershipInfo();

        // 해당 유저가 존재 하지 않을 때
        if (!CommonUtil.isExist(userInfoVO)) {
            throw new UsernameNotFoundException("member info empty");
        }

        // 아이디 패스워드
        member.setPassword(userInfoVO.getUserPwd());
        member.setUsername(userInfoVO.getUserId());

        // Pending
        if (CommonUtil.isNotEmpty(userInfoVO.getPendingYn()) && userInfoVO.getPendingYn().equals("Y")) {
            member.setAccountNonLocked(false);
        } else {
            member.setAccountNonLocked(true);
        }

        // Spring Security 에러 처리 부분
        member.setAccountNonExpired(true);
        member.setCredentialsNonExpired(true);
        member.setCredentialsNonExpired(true);
        member.setEnabled(true);

        member.setLoginTime(getNowTime.getTimeByDate());

        // 권한 저장
        member.setAuthorities(getAuthorities(userInfoVO.getAuth()));
        member.setAuthoritiesLevel(userInfoVO.getAuth());
        member.setAuthoritiesStr(chgAuthoritiesToStr(member.getAuthoritiesLevel()));

        // 유저 시퀀스 저장
        member.setUserSeq(userInfoVO.getUserSeq());

        return member;
    }

    /**
     * 인증 키를 이용한 비밀번호 찾기
     * @param req
     * @return
     */
    public ResponseHandler<?> changePwdByVerifyKey(ChangePasswordByCodeReq req) {
        logger.info("changePwdByVerifyKey[req]" + req);
        ResponseHandler<?> result = new ResponseHandler<>();

        try {
            String userId = ComEncDecUtil.getDecrypted(req.getUserId(), configCommon.getAes128Key());
            String verifyCode = ComEncDecUtil.getDecrypted(req.getVerifyPwdKey(), configCommon.getAes128Key());

            UserInfoReq userReq = new UserInfoReq();
            userReq.setUserId(userId);
            userReq.setAuth(configMembership.getSelectAuthorityUserNo());
            userReq.setIsManagerAuth(req.getIsManagerAuth());
            UserInfoVO userInfoVO = membershipDao.selectUser(userReq);

            // 해당 유저가 없을 때
            if (userInfoVO == null) {
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_USERID_NO_EXIST_NG);
                return result;
            }

            // 인증키가 맞지 않을 때
            if(!verifyCode.equals(userInfoVO.getVerifyPwdKey())) {
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_AUTHORITY_NG);
                return result;
            }

            //  신규 패스워드 pattern 확인
            if (configValidationCheck.checkPwd(req.getNewUserPwd()) != 0) {
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_PASSWORD_PATTERN_NG);
                return result;
            }

            // 인증 키 초기 화
            UserInfoVO uivReq = new UserInfoVO();
            uivReq.setUserSeq(userInfoVO.getUserSeq());
            uivReq.setUserPwd(passwordHandler.encode(req.getNewUserPwd()));
            uivReq.setVerifyPwdKey("");
            membershipDao.updateUser(uivReq);

            result.setReturnCode(ReturnType.RTN_TYPE_OK);

            logger.info("changePwdByVerifyKey[result]" + result);
        }catch(Exception e) {
            logger.error("changePwdByVerifyKey[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            return result;
        }

        return result;
    }

    /**
     * Spring Security 암호화 함수 Override
     *
     * @return
     */
    public PasswordEncoder passwordEncoder() {
        return passwordHandler.getPasswordEncoder();
    }

    /**
     *
     * 현재 로그인한 유저의 세션 정보를 가져 온다.
     *
     * @return
     * @throws Exception
     */
    public MembershipInfo currentSessionUserInfo() throws Exception {

        if (!CommonUtil.isExist(SecurityContextHolder.getContext())) {
            return null;
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (!CommonUtil.isExist(authentication)) {
            return null;
        }

        if (authentication.getPrincipal() == null) {
            return null;
        }

        if (authentication.getPrincipal().equals("anonymousUser") ||
                (authentication.getPrincipal() == null)) {
            return null;
        }

        MembershipInfo user = (MembershipInfo) authentication.getPrincipal();

        return user;
    }



    /**
     * 회원 가입
     * @param req
     * @return
     */
    public ResponseHandler<?> signUp(SignupReq req) {
        logger.info("SignupReq[req]" + req);
        ResponseHandler<?> result = new ResponseHandler<>();

        try {
            ///////////////////////////////////////////////////////////////////////////////////////////////
            // 1. User id 검증
            ///////////////////////////////////////////////////////////////////////////////////////////////
            UserInfoReq userReq = new UserInfoReq();
            userReq.setUserId(req.getUserId());

            /* 사용자 일반 권한 설정 */
            userReq.setAuth(configMembership.getSelectAuthorityUserNo());

            if (this.checkExistInUser(userReq)) {
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_USERID_EXIST_NG);
                return result;
            }

            // 2) email의 pattern
            if (configValidationCheck.checkEmail(req.getUserId()) != 0) {
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_EMAIL_PATTERN_NG);
                return result;
            }

            logger.info("[Service][SignUp] USER ID OK");

            // 2. Password 검증 및 암호화
            String pwd = req.getUserPwd();

            // 2) Id의 pattern
            if (configValidationCheck.checkPwd(pwd) != 0) {
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_PASSWORD_PATTERN_NG);
                return result;
            }

            // 3) 암호화
            String encPassword = passwordHandler.encode(pwd);

            if (!passwordHandler.matches(pwd, encPassword)) {
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_PASSWORD_ENC_NG);
                return result;
            }
            req.setUserPwd(encPassword);

            logger.info("[Service][SignUp] USER PWD OK");
            ///////////////////////////////////////////////////////////////////////////////////////////////

            // 3. 권한 디폴트 설정
            req.setAuth(configMembership.getSelectAuthorityUserNo());

            // 3-1. 4자리 코드 생성
            String code = this.getCode(4);
            req.setVerifyCode(code);

            // 3-2. 인증 시간 설정
            String expirationDt = this.getExpirationDt();
            req.setVerifyCodeExpirationDt(expirationDt);

            // 4. TB_USER, TB_MEMBER_INFO 입력
            membershipDao.insertUser(req);
            membershipDao.insertProfile(req);

            // 5. 인증 메일 발송
            EmailSendReq emailReq = new EmailSendReq();
            emailReq.setUserSeq(req.getUserSeq());
            emailReq.setUserId(req.getUserId());
            emailReq.setVerifyCode(req.getVerifyCode());

            //인증메일 발송
            sendEmailVerify(emailReq);

            result.setReturnCode(ReturnType.RTN_TYPE_OK);

            logger.info("SignupReq[result]" + result);

        } catch (Exception e) {
            logger.error("signUp[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            return result;
        }

        return result;
    }

    /**
     * 인증 메일 발송
     *
     * @return
     * @throws Exception
     */
    public ResponseHandler sendEmailVerify(EmailSendReq req) {

        logger.info("sendEmailVerify[req]" + req);
        ResponseHandler result = new ResponseHandler();

        try {
            //String emailVerify = CommonUtil.getRandomEnNumber(16);
            String userId = req.getUserId();
            String url = ComStaticUtil.requestUrl+ configMembership.getEmailVerifyUrl() + "?userId=" + userId;
            req.setVerifyCodeLink(url);

            String fromEmail = configCommon.getMail().getAdminaddress();
            String code = req.getVerifyCode();
            String subject = "[인증코드: "+code+"] 인증 후 바로 모던양장을 사용하실 수 있습니다.";
            String contents = VelocityUtil.getEmailVerifyContents(req, ComStaticUtil.requestUrl);
            mailHandler.sendEmail(fromEmail, userId, subject, contents);

            result.setReturnCode(ReturnType.RTN_TYPE_OK);
            logger.info("sendEmailVerify[result]" + result);

        } catch(Exception e) {
            logger.error("sendEmailVerify[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);

        }

        return result;

    }

    /**
     * 인증 메일 발송
     *
     * @return
     * @throws Exception
     */
    public ResponseHandler sendEmailVerifySession(CommonReq req) {

        logger.info("sendEmailVerifySession[req]" + req);
        ResponseHandler result = new ResponseHandler();

        try {
            EmailSendReq emailReq = new EmailSendReq();

            String emailVerify = CommonUtil.getRandomEnNumber(16);
            String url = ComStaticUtil.requestUrl + configMembership.getEmailVerifyUrl() + "?userId=" + req.getUserName() + "&verifyKey=" + emailVerify;
            //emailReq.setEmailVerifyUrl(url);

            String fromEmail = configCommon.getMail().getAdminaddress();
            String toEmail = req.getUserName();
            String subject = "[모던양장] 이메일 인증";
            String contents = VelocityUtil.getEmailVerifyContents(emailReq, ComStaticUtil.requestUrl);
            mailHandler.sendEmail(fromEmail, toEmail, subject, contents);

            UserInfoVO updReq = new UserInfoVO();
            updReq.setUserSeq(req.getUserSeq());
            updReq.setVerifyCode(emailVerify);
            updReq.setVerifyCodeExpirationDt(configMembership.getEmailVerifyEndTime());
            membershipDao.updateUser(updReq);

            result.setReturnCode(ReturnType.RTN_TYPE_OK);

            logger.info("sendEmailVerifySession[result]" + result);

        } catch(Exception e) {
            logger.error("sendEmailVerifySession[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);

        }

        return result;

    }

    /**
     * 이메일 인증 처리
     *
     * @return
     * @throws Exception
     */
    public ResponseHandler emailCertify(EmailVerify req) {

        logger.info("emailCertify[req]" + req);
        ResponseHandler result = new ResponseHandler();

        try {
            UserInfoReq userReq = new UserInfoReq();
            userReq.setUserId(req.getUserId());
            userReq.setVerifyCode(req.getVerifyCode());

            UserInfoVO userInfo = membershipDao.selectUser(userReq);
            if(userInfo == null) {
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_EMAIL_VERIFY_NG);
                return result;
            }

            UserInfoVO updReq = new UserInfoVO();
            updReq.setUserSeq(userInfo.getUserSeq());
            updReq.setCertificateYn("Y");
            membershipDao.updateUser(updReq);

            result.setReturnCode(ReturnType.RTN_TYPE_OK);
            logger.info("emailCertify[result]" + result);

        } catch(Exception e) {
            logger.error("emailCertify[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }

        return result;

    }

    public ResponseHandler<?> myInfoCheck(@Valid MyInfoCheckReq req) {
        ResponseHandler result = new ResponseHandler();

        try {
            logger.info("myInfoCheck[req]" + req);
            //req.
            MyInfoCheckRes res = membershipDao.selectExistUser(req);

            if(!passwordHandler.matches(req.getUserPwd(), res.getUserPwd())) {
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_PASSWORD_ENC_NG);
                return result;
            } else { // 확인된 경우
                result.setReturnCode(ReturnType.RTN_TYPE_OK);
            }


            logger.info("myPage[result]" + result);
        } catch(Exception e) {
            logger.error("myPage[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }

        return result;
    }

    public ResponseHandler<?> UpdateUser(UserInfoVO req) {
        ResponseHandler result = new ResponseHandler();

        try {
            logger.info("UpdateUser[req]" + req);
            membershipDao.updateUser(req);

            result.setReturnCode(ReturnType.RTN_TYPE_OK);
            logger.info("UpdateUser[result]" + result);
        } catch(Exception e) {
            logger.error("UpdateUser[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }

        return result;
    }

    public ResponseHandler<?> UpdateProfile(@Valid ProfileReq req) {
        ResponseHandler result = new ResponseHandler();

        try {
            logger.info("UpdateProfile[req]" + req);

            membershipDao.updateProfile(req);

            result.setReturnCode(ReturnType.RTN_TYPE_OK);
            logger.info("UpdateProfile[result]" + result);
        } catch(Exception e) {
            logger.error("UpdateProfile[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }

        return result;
    }

    public ResponseHandler<?> UpdateMyPwd(@Valid ChangePasswordReq req) {
        ResponseHandler result = new ResponseHandler();

        try {
            logger.info("UpdateMyPwd[req]" + req);

            UserInfoVO updReq = new UserInfoVO();
            updReq.setUserSeq(req.getUserSeq());
            updReq.setUserPwd(passwordHandler.encode(req.getNewUserPwd()));

            membershipDao.updateUser(updReq);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
            logger.info("UpdateMyPwd[result]" + result);
        } catch(Exception e) {
            logger.error("UpdateMyPwd[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }

        return result;
    }

    /**
     * 비밀번호 찾기
     *
     * @param req
     * @return
     * @throws Exception
     */
    public ResponseHandler<?> findPwd(FindPwdReq req) {
        ResponseHandler result = new ResponseHandler();
        try {
            logger.info("findPwd[req]" + req);

            // DB에서 유저 정보를 읽어 온다.
            UserInfoReq userReq = new UserInfoReq();
            userReq.setUserId(req.getUserId());
            UserInfoVO userInfoVO = membershipDao.selectUser(userReq);
            UserInfoVO userInfoDetail = membershipDao.selectUserInfo(userReq);
            // 해당 유저가 존재 하지 않을 때
            if (!CommonUtil.isExist(userInfoVO)) {
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_ID_EXIST_NG);
                return result;
            }
            String name = userInfoDetail.getUserNm();
            String userId = req.getUserId();
            int userSeq = userInfoVO.getUserSeq();
            //String email = userInfoVO.getEmail();

            String verifyPwdKey = passwordHandler.makeRandomKey(6);

            UserInfoVO uivReq = new UserInfoVO();
            uivReq.setUserSeq(userInfoVO.getUserSeq());
            uivReq.setVerifyPwdKey(verifyPwdKey);

            membershipDao.updateUser(uivReq);

            //////////////////////////////////////////////////////////////////////////////////

            String param = String.format("userId=%s&verifyKey=%s", ComEncDecUtil.getEncrypted(userId, configCommon.getAes128Key()), ComEncDecUtil.getEncrypted(verifyPwdKey, configCommon.getAes128Key()));

            //비밀번호 재설정 URL
            String url = ComStaticUtil.requestUrl + configMembership.getPasswordConfirmUrl() + "?" + param;
            // 임시 비밀번호를 전송한다.
            MailReq mailReq = new MailReq();
            mailReq.setMailTitle("비밀번호를 재설정하세요.");
            mailReq.setToEmail(userId);
            mailReq.setVelocityPath("templates/velocity/pw_reset.vm");
            VelocityContext velocityContext = new VelocityContext();
            velocityContext.put("url", url);
            velocityContext.put("name", name);
            velocityContext.put("domain", ComStaticUtil.requestUrl);
            mailHandler.sendVolocityMail(mailReq, velocityContext);

        }catch(Exception e) {
            logger.error("findPwd[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }

        return result;
    }

    /**
     * 유저 존재 확인
     * @param req
     * @return
     */
    public boolean checkExistInUser(UserInfoReq req) {

        // user 데이터에서 찾기
        UserInfoVO resMemberData = membershipDao.selectUser(req);

        if (CommonUtil.isNotEmpty(resMemberData)) {
            return true;
        }

        return false;

    }


    /**
     *  인증 레벨을 인증 스트링으로 변환
     *
     * @param authorityLevel
     * @return
     */
    private List<String> chgAuthoritiesToStr(int authorityLevel) {

        List<String> authorities = new ArrayList<String>();

        // Check : unauthority
        if (authorityLevel == configMembership.getSelectUnauthorityUserNo()) {
            authorities.add(configMembership.getSelectUnauthorityUserStr());
        } else {

            // Check : General User authority
            if ((authorityLevel & configMembership.getSelectAuthorityUserNo()) > 0) {
                authorities.add(configMembership.getSelectAuthorityUserStr());
            }

            // Check : Contents Admin authority
            if ((authorityLevel & configMembership.getSelectAuthorityContentsAdminNo()) > 0) {
                authorities.add(configMembership.getSelectAuthorityContentsAdminStr());
            }

            // Check : General Admin authority
            if ((authorityLevel & configMembership.getSelectAuthorityAdminNo()) > 0) {
                authorities.add(configMembership.getSelectAuthorityAdminStr());
            }

            // Check : Super Admin authority
            if ((authorityLevel & configMembership.getSelectAuthoritySuperAdminNo()) > 0) {
                authorities.add(configMembership.getSelectAuthoritySuperAdminStr());
            }
        }
        return authorities;
    }

    /**
     * 사용자의 권한을 설정 한다.
     * Session context에 담길 user 권한을 String Array 형태로 저장 한다.
     *
     * @param authorityLevel : User 권한 값
     * @return authorities : 권한 Array 값.
     */
    private Collection<GrantedAuthority> getAuthorities(int authorityLevel) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

        // Check : unauthority
        if (authorityLevel == configMembership.getSelectUnauthorityUserNo()) {
            authorities.add(new SimpleGrantedAuthority(configMembership.getSelectUnauthorityUserStr()));
        } else {

            // Check : General User authority
            if ((authorityLevel & configMembership.getSelectAuthorityUserNo()) > 0) {
                authorities.add(new SimpleGrantedAuthority(configMembership.getSelectAuthorityUserStr()));
            }

            // Check : Contents Admin authority
            if ((authorityLevel & configMembership.getSelectAuthorityContentsAdminNo()) > 0) {
                authorities.add(new SimpleGrantedAuthority(configMembership.getSelectAuthorityContentsAdminStr()));
            }

            // Check : General Admin authority
            if ((authorityLevel & configMembership.getSelectAuthorityAdminNo()) > 0) {
                authorities.add(new SimpleGrantedAuthority(configMembership.getSelectAuthorityAdminStr()));
            }

            // Check : Super Admin authority
            if ((authorityLevel & configMembership.getSelectAuthoritySuperAdminNo()) > 0) {
                authorities.add(new SimpleGrantedAuthority(configMembership.getSelectAuthoritySuperAdminStr()));
            }
        }

        return authorities;
    }

    /**
     * 세션 정보 조회
     * @param request
     * @param response
     * @return
     */
    public ResponseHandler<SessionRes> getSessionInfo(HttpServletRequest request, HttpServletResponse response) {
        ResponseHandler<SessionRes> result = new ResponseHandler();
        try {

            String accessToken = CookieUtil.getAccessTokenFromCookie(request);
            if(CommonUtil.isEmpty(accessToken)) {
                result.setReturnCode(ReturnType.RTN_TYPE_SESSION);
                return result;
            }

            SessionRes res = jwtService.getJwtParse(accessToken, request, response);
            if(res == null) {
                result.setReturnCode(ReturnType.RTN_TYPE_SESSION);
                return result;
            }

            result.setReturnCode(ReturnType.RTN_TYPE_OK);
            result.setData(res);
            logger.info("getSessionInfo[result]" + result);

        } catch(Exception e) {
            logger.error("getSessionInfo[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
            return result;
        }

        return result;
    }

    /* 내정보 조회 (검증용) */
    public ResponseHandler<MyInfoRes> userInfoData(CommonReq req) {
        ResponseHandler<MyInfoRes> result = new ResponseHandler<>();
        UserInfoReq infoReq = new UserInfoReq();
        infoReq.setUserSeq(req.getUserSeq());
        UserInfoVO userInfoVO = membershipDao.selectUserInfo(infoReq);

        MyInfoRes res = new MyInfoRes();
        try {
            BeanUtils.copyProperties(res, userInfoVO);
            result.setData(res);

            if(CommonUtil.isEmpty(res)){
                result.setReturnCode(ReturnType.RTN_TYPE_SESSION);
            } else {
                result.setReturnCode(ReturnType.RTN_TYPE_OK);
            }

        } catch (Exception e) {
            logger.error("userInfoData :: [Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }
        return result;
    }

    public ResponseHandler<?> codeCheck(CodeCheckReq req) {
        ResponseHandler<?> result = new ResponseHandler<>();
        try {

            /* req 에 들은 값과 디비 값 비교 */
            MyInfoRes myInfoRes = membershipDao.selectCodeCheck(req);
            myInfoRes.setUserId(req.getUserId());
            if(myInfoRes.getVerifyCode().equals(req.getVerifyCode())){ // 코드 일치

                /* 인증기관과 현재 시간 비교 */
                SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date endDt = sdformat.parse(myInfoRes.getVerifyCodeExpirationDt());
                Date current = new Date();

                // returnValue = true 면 시간 지남 false 면 안지남
                Boolean returnValue = current.after(endDt);

                if(returnValue){ // 참 일때는 유효 시간이 지남
                    result.setReturnCode(ReturnType.RTN_TYPE_CERTIFICATE_TIMEOVER);
                } else{
                    /* CERTIFICATE_YN 코드 업데이트 */
                    membershipDao.updateCertificateYn(req);

                    /* 회원가입 완료 메일 발송 */
                    welcomeModern(myInfoRes);

                    result.setReturnCode(ReturnType.RTN_TYPE_OK);
                }

            } else { /* 입력된 코드가 일치 하지 않는 다는 리턴 코드값 리턴 해야 함 */
                result.setReturnCode(ReturnType.RTN_TYPE_CERTIFICATE_NOT_SAME); // <-- 수정 해야 함
            }

        } catch (Exception e) {
            logger.error("CodeCheckReq :: [Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }

        return result;
    }

    public ResponseHandler<?> codeResend(EmailResendReq resend) {
        ResponseHandler<?> result = new ResponseHandler<>();

        try {

            // 1. 4자리 코드 생성
            String code = this.getCode(4);
            resend.setVerifyCode(code);

            // 2. 인증 시간 설정
            String expirationDt = this.getExpirationDt();
            resend.setVerifyCodeExpirationDt(expirationDt);

            /* 코드를 업데이트 한 뒤 새로운 코드 발송 */
            membershipDao.updateCertificateCode(resend);

            // 5. 인증 메일 발송
            EmailSendReq emailReq = new EmailSendReq();
            emailReq.setUserId(resend.getUserId());
            emailReq.setVerifyCode(code);

            //인증메일 발송
            sendEmailVerify(emailReq);

            result.setReturnCode(ReturnType.RTN_TYPE_OK);

            logger.info("codeResend[result]" + result);

        } catch (Exception e) {
            logger.error("codeResend :: [Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }

        return result;
    }

    private String getCode(int digit){
        String rs = "";
        Random rnd =new Random();
        StringBuffer buf =new StringBuffer();
        for(int i=0;i<digit;i++) {
            if (rnd.nextBoolean()) {
                buf.append((char) ((int) (rnd.nextInt(26)) + 65));
            } else {
                buf.append((rnd.nextInt(10)));
            }
        }
        rs = buf.toString();
        return rs;
    }

    private String getExpirationDt(){
        String today = null;
        SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, +6);
        today = sdformat.format(cal.getTime());
        return today;
    }

    private void welcomeModern(MyInfoRes myInfoRes) throws Exception {
        EmailSendReq req = new EmailSendReq();
        String userId = myInfoRes.getUserId();
        String fromEmail = configCommon.getMail().getAdminaddress();

        String subject = "[모던양장] 웰컴 투- 모던양장!";
        String contents = VelocityUtil.getEmailWelcome(ComStaticUtil.requestUrl);

        mailHandler.sendEmail(fromEmail, userId, subject, contents);
    }

    public ResponseHandler<UserInfoVO> getMyInfo(MyPageReq req) {
        ResponseHandler<UserInfoVO> result = new ResponseHandler<>();

        try {
            logger.info("MyPageReq[req]" + req);

            UserInfoReq userInfoReq = new UserInfoReq();
            userInfoReq.setUserId(req.getUserId());
            UserInfoVO userInfoVO = membershipDao.selectUserInfo(userInfoReq);

            result.setData(userInfoVO);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);

        } catch (Exception e) {
            logger.error("= :: [Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }

        return result;
    }

    public ResponseHandler<?> putMyInfo(UpdateUserInfoReq req) {
        ResponseHandler result = new ResponseHandler();

        try {
            logger.info("UpdateUserInfoReq[req]" + req);

            ProfileReq pr = new ProfileReq();
            pr.setUserSeq(req.getUserSeq());
            pr.setUpdId(req.getUserSeq());
            BeanUtils.copyProperties(pr, req);

            membershipDao.updateProfile(pr);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            logger.error("= :: [Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }
        return result;
    }

    public ResponseHandler<?> promoUpdate(PromoModifyReq req) {
        ResponseHandler result = new ResponseHandler();

        try {
            logger.info("PromoModifyReq[req]" + req);

            ProfileReq pfReq = new ProfileReq();
            BeanUtils.copyProperties(pfReq, req);
            membershipDao.updateProfile(pfReq);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            logger.error("= :: [Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }

        return result;
    }

    public ResponseHandler<?> withdraw(WithdrawReq req) {
        ResponseHandler result = new ResponseHandler();

        try {
            logger.info("withdraw[req]" + req);

            //req.
            MyInfoCheckReq myInfoCheckReq = new MyInfoCheckReq();
            BeanUtils.copyProperties(myInfoCheckReq, req);
            MyInfoCheckRes res = membershipDao.selectExistUser(myInfoCheckReq);

            if(!passwordHandler.matches(req.getUserPwd(), res.getUserPwd())) {
                result.setReturnCode(ReturnType.RTN_TYPE_MEMBERSSHIP_PASSWORD_ENC_NG);
                return result;
            } else { // 확인된 경우 TB_USER, TB_MEMBER_INFO --> DEL_YN 'Y' 로 수정

                // TB_USER
                UserInfoVO uiVO = new UserInfoVO();
                BeanUtils.copyProperties(uiVO, req);
                uiVO.setUserPwd("");
                membershipDao.updateUser(uiVO);

                // TB_MEMBER_INFO
                ProfileReq pfReq = new ProfileReq();
                BeanUtils.copyProperties(pfReq, req);
                membershipDao.updateProfile(pfReq);

                result.setReturnCode(ReturnType.RTN_TYPE_OK);
            }

            result.setReturnCode(ReturnType.RTN_TYPE_OK);
        } catch (Exception e) {
            logger.error("= :: [Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }

        return result;
    }
}