package com.modern.portfolio.controller;

import com.modern.common.dto.res.ResponseHandler;
import com.modern.portfolio.dto.req.PortfolioReq;
import com.modern.portfolio.dto.res.PortfolioRes;
import com.modern.portfolio.service.PortfolioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


/**
 *  포토폴리오 Rest Controller class
 *
 *  포토폴리오를 제어하기 위한 REST API가 정의 되어 있다.
 *
 */
@RestController
@RequestMapping(value="/portfolio" ) //모듈명 정의
@Api(value = "PortfolioController", description = "포트폴리오 API")
public class PortfolioController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private PortfolioService portfolioService;

	/**
	 * 포토폴리오 목록
	 */
	@ApiOperation(value = "포토폴리오 목록")
	@GetMapping
	public ResponseHandler<PortfolioRes> selectPortfolio(@Valid PortfolioReq req) {
		ResponseHandler<PortfolioRes> result = portfolioService.selectPortfolio(req);
		return  result;
	}

}
