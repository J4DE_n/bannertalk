package com.modern.portfolio.dao;

import com.modern.portfolio.dto.model.PortfolioVO;
import com.modern.portfolio.dto.req.PortfolioReq;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface PortfolioDao {
    List<PortfolioVO> selectPortfolio(PortfolioReq req);
}
