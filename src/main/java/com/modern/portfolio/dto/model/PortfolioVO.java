package com.modern.portfolio.dto.model;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value ="사용자 포토폴리오 목록 VO")
public class PortfolioVO implements Serializable {
    private Integer ptfSeq;             // 포토폴리오 시퀀스
    private String title;               // 제목
    private String summary;             // 요약설명
    private String fnFilePath;         // 썸네일 파일 경로
    private String detailFilePath;     // 세부설명 파일 경로
}