package com.modern.portfolio.dto.req;

import com.modern.common.dto.req.ManagerPagingReq;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel(value ="사용자 포토폴리오 목록 요청")
public class PortfolioReq implements Serializable {
    @ApiParam(value = "노출 게시물 개수", required = true)
    private int cnt;
}