package com.modern.portfolio.dto.res;

import com.modern.portfolio.dto.model.PortfolioVO;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel(value ="포토폴리오 목록 응답")
public class PortfolioRes implements Serializable {
    private List<PortfolioVO> portfolioList;        // 포토폴리오 목록 리스트
}