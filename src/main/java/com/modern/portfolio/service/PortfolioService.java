package com.modern.portfolio.service;

import com.modern.common.dto.res.ResponseHandler;
import com.modern.common.type.ReturnType;
import com.modern.portfolio.dao.PortfolioDao;
import com.modern.portfolio.dto.model.PortfolioVO;
import com.modern.portfolio.dto.req.PortfolioReq;
import com.modern.portfolio.dto.res.PortfolioRes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PortfolioService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private PortfolioDao portfolioDao;

    /**
     * 포토폴리오 목록
     */
    public ResponseHandler<PortfolioRes> selectPortfolio(PortfolioReq req) {
        logger.info("selectPortfolio[req]" + req);
        final ResponseHandler<PortfolioRes> result = new ResponseHandler<>();

        try {
            List<PortfolioVO> list = portfolioDao.selectPortfolio(req);
            PortfolioRes res = new PortfolioRes();
            res.setPortfolioList(list);

            result.setData(res);
            result.setReturnCode(ReturnType.RTN_TYPE_OK);
            logger.info("selectPortfolioList[result]" + result);
        }catch (Exception e) {
            logger.error("selectPortfolioList[Exception]", e);
            result.setReturnCode(ReturnType.RTN_TYPE_NG);
        }
        return result;
    }
}