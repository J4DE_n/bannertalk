var seq = tms.getParameterByName("seq");
var editorImg = new Array();

/* 메시지 영역 */
var message = {
    required: {
        title: '제목을 입력해주세요',
    }
}

/* 저장 버튼 */
$(document).on("click", "#changeSave", function(e){
    boardInsertUpdate();
});

/* 수정 버튼 */
$(document).on("click", "#changeUpdate", function(e){
    boardInsertUpdate();
});

/* 삭제 버튼 */
$(document).on("click", "#changeDelete", function(e){
    boardDelete();
});

/* 취소 버튼 */
$(document).on("click", "#changeCancel", function(e){
    location.href = "/page/admin/change/list";
});

$(document).ready(function(){

    /* 썸머노트 세팅 */
    setSummernote();

    /* insert & update 구분 */
    if(tms.isNotEmpty(seq)){
        /*상세정보*/
        getDetail();
    } else {
        /* 고객 변화 등록 */
        /* 저장 버튼 O / 수정 버튼 X / 삭제 버튼 X */
        $("#changeSave").show();
        $("#changeUpdate").hide();
        $("#changeDelete").hide();

        var now = new Date();

        var year = now.getFullYear();
        var mon = (now.getMonth()+1)>9 ? ''+(now.getMonth()+1) : '0'+(now.getMonth()+1);
        var day = now.getDate()>9 ? ''+now.getDate() : '0'+now.getDate();

        var chan_val = year + '-' + mon + '-' + day;
        $("#writeDt").val(chan_val);
    }


    /* 제목 글자수 체크 */
    $("#title").keyup(function (e){
        var title = $(this).val();
        if (title.length >= 30){
            alert("최대 30자까지 입력 가능합니다.");
            $(this).val(title.substring(0, 30));
        }
    });

    /* 요약 설명 글자수 체크 */
    $("#summary").keyup(function (e){
        var title = $(this).val();
        if (title.length >= 100){
            alert("최대 100자까지 입력 가능합니다.");
            $(this).val(title.substring(0, 100));
        }
    });

    /* 태그 글자수 체크 */
    $("#tagNm").keyup(function (e){
        var title = $(this).val();
        if (title.length >= 30){
            alert("최대 30자까지 입력 가능합니다.");
            $(this).val(title.substring(0, 30));
        }
    });
});

/* 서머노트 초기화 함수 */
var setSummernote = function(){
    $('#summernote').summernote({
        minHeight:300,
        lang: 'ko-KR',
        callbacks: {
            onImageUpload: function(files, editor, welEditable) {
                sendFile(files, this, welEditable);
            }
        },
        placeholder: '고객 변화 상세내용을 입력해주세요.'
        //lang: 'ko-KR' // default: 'en-US'
    });
}

/* 게시판 상세 조회 */
function getDetail() {
    /* 저장 버튼 X */
    $("#changeSave").hide();

    var params = {
        chgSeq: seq
    };

    var options = {showLoding:true};
    tms.ajaxGetHelper('/mapi/change/item', params ,options ,function(rs){
        if(rs.code==0){
            var dt = rs.data.writeDt.split(" ");
            rs.data.writeDt = dt[0];

            tmsInput.detailMapping(rs.data);
            var displayYn = rs.data.displayYn;

            if(displayYn == 'Y'){
                $("input:checkbox[name='displayYn']").prop("checked", true);
                $("input:checkbox[name='displayYn']").val("Y");
            }else{
                $("input:checkbox[name='displayYn']").val("N");
            }

            var title = rs.data.title;

            var summary = rs.data.summary;

            var tagNm = rs.data.tagNm;

            var contents = rs.data.contents;
            /* 썸머노트 */
            var content = $('#summernote').summernote('code',contents);
            // var tempDiv = document.createElement('div');
            // tempDiv.innerHTML = content;

            $.each($(".note-editable").find("img[data-filename]"), function(index, value) {
                var nm = $(value).data("filename");
                editorImg.push(nm);
            });
        }
    });
}

/* 게시판 삽입 & 수정 */
function boardInsertUpdate(){

    if(validationCheck()){

        /* 이미지 삭제 */
        var editorDelImg = editorImg.slice();

        var contents = $('#summernote').summernote('code'); //에디터 내용 가져오기
        //var tempDiv = document.createElement('div');
        //tempDiv.innerHTML = contents;
        $.each($(".note-editable").find("img[data-filename]"), function(index, value) { //삭제된 이미지 비교 처리
            var nm = $(value).data("filename");
            var arrayIdx = editorDelImg.indexOf(nm);
            if(arrayIdx != -1){
                editorDelImg.splice(arrayIdx,1);
            }
        });

        var frm = "#changeForm";
        var form = $(frm)[0];
        var param = new FormData(form);

        var url = "/mapi/change";
        if(tms.isNotEmpty(seq)){
            //console.log("seq")
            url = "/mapi/change/edit"
            param.append("chgSeq", seq);
            param.delete("displayYn");
            param.append("displayYn", $(".displayYn").val())
        }

        param.append("editorDelImg", editorDelImg);

        tms.ajaxMulitpartHelper(url, param, null,function(rs){
            if(rs.code==0){
                tms.initInputTxt(frm);
                $('#summernote').summernote('reset');
                location.href = "/page/admin/change/list";
            }else if(rs.code == 100){
                alert("error");
            }
        });


    }
}

/* 벨류 체크 */
function validationCheck(){

    /* 초기화 */
    $("#titleCheck").text("");

    var frm = "#changeForm";
    var $errorMsg = $(".message");

    var title = $("#title").val();

    var valid = true;

    if(tms.isEmpty(title)){
        $("#titleCheck").text("제목을 입력해주세요");
        valid = false ;
    }

    // var valid = tmsValidation.validInputChk(frm, message, function(res){
    //     $errorMsg.text(res);
    //});
    return valid;
}


/* 서머노트 에디터 이미지 등록 */
var sendFile = function(files, editor, welEditable) {
    var data = new FormData();
    for (var i = files.length - 1; i >= 0; i--) {
        data.append('file', files[i]);
    }
    data.append('category', 9);
    tms.ajaxMulitpartHelper("/api/file/editor", data, null, function(res){
        var fileInfos = res.data.fileInfos;
        if(res.code == 0 && tms.isNotEmpty(fileInfos)){
            fileInfos.forEach(function(obj, idx){
                editorImg.push(obj.fileSysName);
                $(editor).summernote('insertImage', obj.fileUrl, obj.fileSysName);
            });
        }
    }, function(){
        alert("이미지 등록에 실패하였습니다\n다시 시도해주세요");
    });
}

/* 고객 변화 삭제 */
function boardDelete(){
    /* delete popup */
    layer_OPEN('.popup_visible_order');
}

/* 용어 여러개 삭제 */
function realDeleteTerm(){
    var param = [];
    param[0] = seq;

    var params = {
        changeSeqList : param
    };

    var options = {showLoding:true};
    tms.ajaxDeleteHelper('/mapi/change', JSON.stringify(params), options, function (rs) {
        if(rs.code==0){
            location.href="/page/admin/change/list";
        }else{
            alert('오류가 있습니다.');
        }
    });
}

/* 레이어 클로즈 */
function closePopup(obj_selector){
    var obj = $(obj_selector);
    obj.stop().animate({'opacity':0},300,function (){
        $(this).css({'display':'none'});
    });
    realDeleteTerm();
};
