var countPerPage = 10;

/*var getParameterByName = function(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
} --> 상세페이지에서 사용할 것!!!!!!. 경로별로 이름 다르게 설정할 때 필요하다.*/


$(document).ready(function(){

    list(1);

    /* 검색버튼 클릭 */
    $("#searchBtn").on("click", function(){
        search();
    });

    /* 페이징 필터 클릭 */
    $("#filter").on("click", function(){
        var $this = $(this);
        countPerPage = $this.data("cnt");
        search();
    });

    /* 새로고침 버튼 클릭 */
    $("#refreshBtn").on("click", function(){
        location.href="/page/admin/change/list";
    });

    /* 검색 엔터 */
    $("#searchText").on("keyup", function(e){
        if(e.keyCode == 13){
            search();
        }
    });

});

/* 등록 클릭 시 */
$(document).on('click','#changeEnroll',function(){
    location.href="/page/admin/change/detail";
});

/* 삭제 클릭 시 */
$(document).on('click','#changeDelete',function(){
    deleteTerm()
});

/* 전체 선택 */
$(document).on('click','#allSelect',function(){
    var allSelect = $("#allSelect").is(":checked");
    if(allSelect){
        $(".changeSelect").prop('checked', true);
        $('#btnDel').show();
    }else{
        $(".changeSelect").prop('checked', false);
        $('#btnDel').hide();
    }
});

$(document).on('click','[name="changeSelect"]',function () {
    $('#allSelect').prop("checked",false );
    if($('[name="changeSelect"]:checked').length > 0){
        $('#btnDel').show();
    } else {
        $('#btnDel').hide();
    }
});

/*function getFormatDate(date){
    var wYear = date.getFullYear();
    var wMonth = date.getMonth();
    wMonth = wMonth >= 10 ? wMonth : '0' + wMonth;
    var wDay = date.getDate();
    wDay = wDay >= 10 ? wDay : '0' + wDay;

    var formatting = wYear + '-' + wMonth + '-' + wDay;

    return formatting;
}*/

/* 리스트 조회 */
function list(no){
    $('#btnDel').hide();
    var colsInfo = [
        {no:1, col:'chgSeq', name:'선택', custom:function(obj){
                return "<label class=\"checkbox primary\">" +
                    '<input type="checkbox" class="changeSelect" name="changeSelect" style="vertical-align: middle;" value='+obj.chgSeq+' />' +
                    "<em></em></label>";
            }}
        , {no:2, col:'rowNum', name:'순서'}
        , {no:3, col:'displayYn', name:'게시 여부', custom:function(obj){
            var displayYn = obj.displayYn;

            if(displayYn == 'Y'){
                return "<div>게시</div>";
            }else{
                return "<div>미 게시</div>";
            }

            }}
        , {no:4, col:'title', name:'제목',custom:function(obj){
                return "<a href='/page/admin/change/detail?seq="+obj.chgSeq+"' class=\"title\">"+obj.title+"</a>";
            }}
        , {no:5, col:'creId', name:'등록자', custom:function (obj) {
                return "<div>"+obj.userNm+"("+obj.userId+")"+"</div>";
            }}
        , {no:6, col:'writeDt', name:'등록일', custom:function (obj) {
            var wDate = obj.writeDt.split(' ');
                return "<div>"+wDate[0]+"</div>";

                /* 달력 모듈 사용 시 아래꺼 사용하면 될 거 같다 */
                /*return "<div>"+obj.writeDt+"</div>";*/
            }} /*날짜 포맷팅 진행하기 찾아보고*/
    ];

    var params = {
        currentPage : no
        , cntPerPage : countPerPage
        , startDate : $("#startDate").val()
        , endDate : $("#endDate").val()
        , searchText : $("#searchText").val()
        , displayYn : $("select[name=searchDisplayYn]").val()
    };


    var options = {showLoding:true};
    tms.ajaxGetHelper('/mapi/change',params ,options ,function(rs){
        var data = rs.data;


        var totalCnt = tms.isEmpty(data)? 0 : data.totalCnt;

        var paging = {
            id: '#paging',
            pageNo: no,
            totalCnt: totalCnt,
            countPerPage: countPerPage,
            fn: 'list',
            activeClass: 'on',
            type: 'type1'
        };

        var info = {
            colsInfo: colsInfo
            ,paging : paging
        };

        if(rs.code==0){
            var list = rs.data.changeList;

            if(list==null || list.length==0){
                tableType3("#list", null, info, 6, "내용이 없습니다.");
                return ;
            }

            tableType3("#list", list, info);
            $('.table_style_02 tbody td').css({'textAlign':'center'})
        }else {
            tableType3("#list", null, info, 6, "내용이 없습니다.");
        }
        $("#list table tr td:nth-child(1)").addClass("c");
    });
}
/* 고객 변화 삭제 */
function deleteTerm(){
    layer_OPEN('.popup_delete');
}

/* 고객 변화 여러개 삭제 */
function realDeleteTerm(){
    var param = [];

    $.each( $('[name="changeSelect"]:checked') ,function (k,v) {
        param.push( $(this).val() );
    });
    var params = {
        seqList : param
    };

    var options = {showLoding:true};
    tms.ajaxDeleteHelper('/mapi/change', JSON.stringify(params), options, function (rs) {
        if(rs.code==0){
            list(1);
            $("#allSelect").removeAttr("checked");
        }else{
            alert('잠시 후 시도하여주세요');
        }
    });
}

/* 한자리가 되는 숫자에 "0"을 넣어주는 함수 */
function addzero(n){
    return n < 10 ? "0" + n: n;
}

/* 날짜 계산 */
function dateInput(month,day){

    /* 기존 값 초기화 */
    $("#startDate").val("");
    $("#endDate").val("");

    var date = new Date();
    var start = new Date(Date.parse(date)-month* 1000 * 60 * 60 * 24);
    var today = new Date(Date.parse(date)-day* 1000 * 60 * 60 * 24);

    if(month < 10){
        start.setMonth(start.getMonth()-month);
    }
    var yyyy = start.getFullYear();
    var mm = start.getMonth()+1;
    var dd = start.getDate();

    var t_yyyy = today.getFullYear();
    var t_mm = today.getMonth()+1;
    var t_dd = today.getDate();


    if(day>0){
        $("#endDate").val(yyyy+'-'+addzero(mm)+'-'+addzero(dd));
        $("#startDate").val(t_yyyy+'-'+addzero(t_mm)+'-'+addzero(t_dd));
    }else{
        $("#startDate").val(yyyy+'-'+addzero(mm)+'-'+addzero(dd));
        $("#endDate").val(t_yyyy+'-'+addzero(t_mm)+'-'+addzero(t_dd));
    }
}

/* 검색 */
function search() {
    if(tms.isNotEmpty($("#searchText").val()) && $("#searchText").val().length < 2){
        alert("2글자 이상 입력해주세요.");
        return;
    }
    list(1);
}
