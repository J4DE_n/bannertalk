/* 관리자 공통 function */
var admin = (function ($, win, doc) {
    /* 확인 팝업 */
    var openConfirmPopup = function(msg, fnAction) {

        $("#confirmPopup").html("");
        $("#confirmPopTemplate").tmpl({message: msg}).appendTo("#confirmPopup");
        layer_OPEN(".confirmPop");

        $("#confirmPopup .in_layer_box").find("a.confirm").bind("click", function(){
            if(typeof fnAction == "function"){
                fnAction();
            }
            layer_CLOSE(".confirmPop");
            $(this).unbind();
        });
    };

    /* 알림 팝업 */
    var openAlertPopup = function(msg, fnAction) {
        $("#alertPopup").html("");
        $("#alertPopTemplate").tmpl({message: msg}).appendTo("#alertPopup");
        layer_OPEN(".alertPop");

        var $btnConfirm = $("#alertPopup .in_layer_box").find("a.confirm");
        $btnConfirm.focus();
        $btnConfirm.bind("click", function(){
            if(typeof fnAction == "function"){
                fnAction();
            }
            layer_CLOSE(".alertPop");
            $(this).unbind();
        });
    };

    var message = {
        cancelMsg: "페이지에서 나가시겠습니까? <br>변경사항이 저장되지 않을 수 있습니다."
        , saveMsg: "저장되었습니다."
        , modifyMsg: "수정되었습니다."
        , delMsg: "삭제되었습니다."
        , deletedMsg: "삭제된 데이터 입니다."
        , listDelMsg: "선택하신 항목을 삭제하시겠습니까?"
        , detailDelMsg: "현재 항목을 삭제하시겠습니까?"
        , delErrMsg: "삭제 중 오류가 발생하였습니다. \n잠시 후 다시 시도해주세요."
        , saveErrMsg: "저장 중 오류가 발생하였습니다. \n잠시 후 다시 시도해주세요."
        , noSearchMsg: "검색 결과가 없습니다."
        , noDelSelectMsg: "삭제할 항목을 선택해주세요"
        , twoWordErrMsg: "2글자 이상 입력해주세요."
        , fOverSizeMsg : "첨부파일 크기를 초과했습니다."
        , fExtErrMsg : "지원하지 않는 형식입니다."
        , imgDelMsg : "이미지를 삭제하시겠습니까?"
        , imgUpErrMsg : "이미지 등록에 실패하였습니다. \n잠시 후 다시 시도해주세요."
        , defaultErrMsg : "오류가 발생하였습니다. \n잠시 후 이용 부탁드립니다."
        , noMatchIdMsg : "존재하지 않는 아이디입니다."
        , sentMailMsg : "메일이 발송 되었습니다."
        , reqEmailMsg : "이메일을 입력해주세요."
        , valEmailMsg : "잘못된 이메일 형식입니다."
        , invalidPathMsg : "잘못된 접근입니다."
        , pwdFormMsg : "비밀번호는 특수문자와 영어, 숫자를 포함한 최소 8자 이상입니다."
        , pwdNotSameMsg : "비밀번호가 일치하지 않습니다"
        , authErrPageMsg : "만료된 비밀번호 페이지입니다."
        , errChgPwdMsg : "비밀번호 설정 중 에러가 발생했습니다. 잠시 후 다시 시도해주세요."
        , newPwdMsg : "새 비밀번호를 입력해주세요."
        , newPwdChkMsg : "새 비밀번호 확인을 입력해주세요."
        , popCancelMsg : "작성 중이던 내용이 삭제됩니다.<br>정말 취소하시겠습니까?"
        , sendResetMsg : "비밀번호 재설정 메일을 보냈습니다. <br>메일을 확인해주세요."
        , newPwdSetMsg : "신규 비밀번호가 설정 되었습니다.<br>확인을 누르시면 로그인 페이지로 이동합니다."
        , chgPwdMsg : "비밀번호를 성공적으로 변경했습니다.<br>확인을 누르시면 로그인 페이지로 이동합니다."

    }

    var urlInfo = {
        managerList: "/page/admin/manager/list" //컨설턴트 목록
        , adminLogin: "/page/admin/login" // 로그인 화면
        , adminPwdFind: "/page/admin/pwdFind" // 비밀번호 찾기 화면
        , adminPwdSetting: "/page/admin/pwdSetting" // 비밀번호 재설정 화면
        , managerList : "/page/admin/manager/list"
        , managerRegister : "/page/admin/manager/register"
        , portfolioList: "/page/admin/portfolio/list"
        , portfolioRegister: "/page/admin/portfolio/register"
        , myInfo: "/page/admin/myInfo"
    }
    var managerInfo = {
        seq: 0
        ,name: ''
        ,auth: ''
    }

    var getSession = function(){
        tms.ajaxGetHelper("/session/manager", null, null, function (res) {
            if (res.code == 0) {
                //로그인 성공시 처리
                var data = res.data;
                managerInfo.seq = data.userSeq;
                managerInfo.name = data.userName;
                getAuth(data.auth);

                if(typeof sessionCallback === "function"){
                    sessionCallback();
                }
            }
        });
    }

    var getAuth = function(set){
        if((set & 0x0020) == 0x0020) {
            managerInfo.auth = code.authStr.ADMIN;
        } else if((set & 0x0010) == 0x0010) {
            managerInfo.auth = code.authStr.CONSULTANT;
        } else if((set & 0x01) == 0x01) {
            managerInfo.auth = code.authStr.USER;
        }

        if(managerInfo.auth == code.authStr.CONSULTANT){
            $("#productPayment").hide();
            $("#text").hide();
            $("#mng").hide();
            $(".authShow").hide();
        }
    }

    var getAuthStr = function(set){
        if((set & 0x0020) == 0x0020) {
            return code.authStr.ADMIN;
        } else if((set & 0x0010) == 0x0010) {
            return code.authStr.CONSULTANT;
        } else if((set & 0x01) == 0x01) {
            return code.authStr.USER;
        }
    }

    return {
        openConfirmPopup: openConfirmPopup,
        openAlertPopup: openAlertPopup,
        urlInfo: urlInfo,
        message: message,
        getSession: getSession,
        managerInfo: managerInfo,
        getAuthStr: getAuthStr
    }

}(jQuery, window, document));


$(function(){
   admin.getSession();
});