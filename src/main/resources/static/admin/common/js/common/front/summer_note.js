$(document).ready(function(){
    /* 썸머노트 세팅 */
    setSummernote();
});

/* 서머노트 초기화 함수 */
var setSummernote = function(){
    $('#summernote').summernote({
        minHeight:300,
        lang: 'ko-KR',
        callbacks: {
            onImageUpload: function(files, editor, welEditable) {
                sendFile(files, this, welEditable);
            }
        },
        placeholder: '내용을 입력해주세요.'
        //lang: 'ko-KR' // default: 'en-US'
    });
}
/* 서버노트 에디터 이미지 등록 */
var sendFile = function(files, editor, welEditable) {
    var data = new FormData();
    for (var i = files.length - 1; i >= 0; i--) {
        data.append('file', files[i]);
    }
    tms.ajaxMulitpartHelper("/api/file/editor", data, null, function(res){
        var fileInfos = res.data.fileInfos;
        if(res.code == 0 && tms.isNotEmpty(fileInfos)){
            fileInfos.forEach(function(obj, idx){
                editorImg.push(obj.fileSysName);
                $(editor).summernote('insertImage', obj.fileUrl, obj.fileSysName);
            });
        }
    }, function(){
        alert("이미지 등록에 실패하였습니다\n다시 시도해주세요");
    });
}