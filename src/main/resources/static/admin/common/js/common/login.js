$(function () {
    if(location.pathname != "/page/admin/login"){
        $(document).on('click','#header .user_area .user_box .user_layer .btn_logout',function (e) {
            e.preventDefault();
            tms.ajaxPostHelper('/logout',{},null,function (rs) {
                if(rs.code=='0'){
                    location.href='/page/admin/login';
                }
            });
        })

        tms.ajaxGetHelper('/membership/session',{},null,function (rs) {
            if(rs.code=='0'){
                var userNm = rs.data.userNm;
                if(tms.isNotEmpty(userName)){
                    $('#header .user_area .name').text(userNm);
                    $('#header .user_area .user_box .btn_user').text(userNm.substring(0,1));
                }
            }else{
                location.href='/page/admin/login';
            }
        });
    }
});