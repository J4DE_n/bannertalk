UI = {
	load: function(){
		$(document).ready(function(){			
			UI.fn_left_area();
			UI.fn_sub_main();
			UI.fn_small_layer_pop();			

			$('.date_to_date').datepicker({
				language: "ko",
				 orientation: "bottom auto"
			});

		});//ready

		$(window).load(function(){

		});//load

		$(window).resize(function(){
			
		});//resize
	},

	fn_small_layer_pop : function(){
		var user_box = $('#header .user_box');
		var user_btn = user_box.find('.btn_user');

		var setting_box = $('#header .setting_box');
		var btn_setting = setting_box.find('.btn_setting');

		user_btn.bind('click',function(){
			if(user_box.hasClass('open')){
				user_box.removeClass('open');
			}
			else{
				setting_box.removeClass('open');
				user_box.addClass('open');			
			}
			return false; 
		});		
		
		btn_setting.bind('click',function(){
			if(setting_box.hasClass('open')){
				setting_box.removeClass('open');
			}
			else{
				user_box.removeClass('open');
				setting_box.addClass('open');			
			}
			return false; 
		});

		var list_total_box = $('.list_top_area .list_total_choice_box');
		var list_btn = list_total_box.find('.button');

		list_btn.bind('click',function(){
			if(list_total_box.hasClass('open')){
				list_total_box.removeClass('open');
			}
			else{				
				list_total_box.addClass('open');			
			}
			return false; 
		});

		var mem_profile_box = $('.member_profile_box .profile_modify_box');
		var mem_profile_modify_btn = mem_profile_box.find('.btn_profile_modify');

		mem_profile_modify_btn.bind('click',function(){
			if(mem_profile_box.hasClass('open')){
				mem_profile_box.removeClass('open');
			}
			else{				
				mem_profile_box.addClass('open');			
			}
			return false; 
		});
		
		$(document).click(function(e){ //문서 body를 클릭했을때
			if(e.target.className =="user_layer"){return false}
			if(e.target.className =="setting_layer"){return false}
			if(e.target.className =="list_total_layer"){return false}
			if(e.target.className =="modify_layer"){return false}
			user_box.removeClass('open');
			setting_box.removeClass('open');
			list_total_box.removeClass('open');
			mem_profile_box.removeClass('open');
		});
	},
	
	fn_left_area : function(){
		var wraper = $('#wrap');
		var btn_fixed = $('#left_area .btn_menu_fixed');

		btn_fixed.bind('click',function(){
			if(wraper.hasClass('fixed_screen')){
				wraper.removeClass('fixed_screen');
			}
			else{
				wraper.addClass('fixed_screen');			
			}
		});

		var left_menu = $('#left_area .menu_list').children();
		var depth2 = $('#left_area .depth2_wrap');
		var btn_depth1 = $('#left_area .btn_depth1');

		btn_depth1.bind('click',function(){
			var p_obj = $(this).parent();
			var this_depth2 = p_obj.find('.depth2_wrap');

			if(p_obj.hasClass('on')){
				p_obj.removeClass('on');
				this_depth2.stop().slideUp(300);
			}else {
				left_menu.removeClass('on');
				p_obj.addClass('on');
				depth2.slideUp(300);
				this_depth2.stop().slideDown(300);
			}
		});
	},

	fn_sub_main : function (){
		var content_wrap_obj = $('.content_inner');
		var conts_top_area_obj = content_wrap_obj.find('.conts_top_area');
		var content = $('#contents');
		var search_opener = content_wrap_obj.find('.search_area_opener a');
		var search_condition_box = content_wrap_obj.find('.search_condition_box');

		if(search_condition_box.length == 0){ return; }


		search_opener.bind('click',function(){
			if(content_wrap_obj.hasClass('search_open')){
				content_wrap_obj.removeClass('search_open');
			}else {
				content_wrap_obj.addClass('search_open');
			}
		});

		var movement = function (){
			var offset_top = content.offset().top - 104;

			if (offset_top <= 0){
				if(!content_wrap_obj.hasClass('fixed')){
					conts_top_area_obj.css({'height': conts_top_area_obj.innerHeight()});
					content_wrap_obj.addClass('fixed');
				}								
			}else {
				if(content_wrap_obj.hasClass('fixed')){
					conts_top_area_obj.css({'height': 'auto'});
					content_wrap_obj.removeClass('fixed');
					content_wrap_obj.removeClass('search_open');
				}
			}
		};

		content_wrap_obj.scroll(function(){
			movement();
		});
	},

	tab_active : function (){
		var tab_obj = $('.tab_wrap');
		var tab_depth1 = tab_obj.find('> ul > li');
		var tab_depth2 = tab_obj.find('> ul > li > ul > li');
		var tab_depth1_btn = tab_obj.find('> ul > li > a');
		var tab_depth2_btn = tab_obj.find('> ul > li > ul > li > a');

		tab_depth1_btn.on('click',function(){
			tab_depth1.removeClass('on');
			tab_depth2.removeClass('on');
			$(this).parent().addClass('on');
		});	
		
		tab_depth2_btn.on('click',function(){
			tab_depth2.removeClass('on');
			$(this).parent().addClass('on');
		});
	}
}

function jsChange(obj){
	$(obj).parent().parent().find("input[name=fname]").val($(obj).val());
}

//레이어 오픈 
var layer_OPEN = function (obj_selector){
	var obj = $(obj_selector);
	obj.css({'display':'block','opacity':0});
	obj.stop().animate({'opacity':1},500);
};

//레이어 클로즈
var layer_CLOSE = function (obj_selector){
	var obj = $(obj_selector);	
	obj.stop().animate({'opacity':0},500,function (){
		$(this).css({'display':'none'});	
	});
};


UI.load();
