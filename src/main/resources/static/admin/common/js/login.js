$(function() {
    $("#userPwd").keydown(function (key) {
        if(key.keyCode == 13){
            clickedLogin();
        }
    });

    /* 로그인 버튼 클릭 */
    $("#btnLogin").on("click", function(){
        clickedLogin();
    });
});


function clickedLogin(){

    /* 벨리데이션 필수체크 */
    var message = {
        required: {
            userId: "이메일을 입력해주세요.",
            userPwd: "비밀번호를 입력해주세요."
        },
        validation: {
            password: {
                1: '비밀번호는 최소 8자 이상입니다.'
                , 2:'비밀번호는 특수문자와 영어, 숫자를 포함한 최소 8자 이상입니다.'
                , 3:'비밀번호는 특수문자와 영어, 숫자를 포함한 최소 8자 이상입니다.'
            },
            email: {
                1: '잘못된 이메일 형식입니다.'
                , 2: '잘못된 이메일 형식입니다.'
            }
        }
    };

    var $errorBox =$(".error_box");
    var valid = tmsValidation.validInputChk("#frm", message, function(errorMsg){
        if(tms.isNotEmpty(errorMsg)){
            $errorBox.show().text(errorMsg);
        } else {
            $errorBox.hide().text("");
        }
    });

    if(!valid){
        return false;
    }

    /* 로그인 액션 */
    var forever = 'N';

    var params = {
        userId: $("#userId").val()
        , userPwd: $("#userPwd").val()
        , forever: forever
    };
    tms.ajaxPostHelper("/admin/login", JSON.stringify(params), null, function(result){
        console.log(result);
        if(result.code == 0) {
            location.href='/page/admin/portfolio/list';
        } else if(result.code == 226){
            $errorBox.show().text("이메일 인증이 필요합니다.");
        } else if(result.code == 218){
            $errorBox.show().text("접근 할 수 없습니다.");
        } else {
            $errorBox.show().text("로그인 정보가 올바르지 않습니다.\n다시한번 확인해주세요");
        }
    }, function(e){
        $errorBox.show().text("로그인 정보가 올바르지 않습니다.\n다시한번 확인해주세요");
    });

}