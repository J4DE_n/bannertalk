var countPerPage = 10;
var managerUserSeq = 0;
$(document).ready(function(){
    getComCode();

    /* 검색버튼 클릭 */
    $("#searchBtn").on("click", function(){
        search();
    });

    /* 새로고침 버튼 클릭 */
    $("#refreshBtn").on("click", function(){
        location.href = admin.urlInfo.managerList;
    });

    /* 검색 엔터 */
    $("#searchText").on("keyup", function(e){
        if(e.keyCode == 13){
            search();
        }
    });

    /* 삭제 */
    $("#btnDel a").on("click", function(){
        fnDelList();
    });

    /* 등록 클릭 시 */
    $(document).on('click','#managerEnroll',function(){
        location.href= admin.urlInfo.managerRegister;
    });

    /* 체크박스 컨트롤 */
    $(document).on('click','#allSelect',function() {
        tms.checkboxAllControl("#allSelect", "[name=subSelect]")
        showDelBtn();
    });

    $(document).on('click','[name="subSelect"]',function () {
        tms.checkboxControl("#allSelect", "[name=subSelect]");
        showDelBtn();
    });

    /* 매칭 회원 조회 */
    $(document).on("click", ".matchUser", function(){
        managerUserSeq = $(this).data("seq");
        getMatchinguser(1);
    });

});

var showDelBtn = function() {
    if($('[name="subSelect"]:checked').length == 0){
        $("#btnDel").hide();
    } else {
        $('#btnDel').show();
    }
};

/* 리스트 조회 */
function list(no){
    $('#btnDel').hide();
    var colsInfo = [
        {no:1, col:'userSeq', name:'선택', custom:function(obj){
                return "<label class=\"checkbox primary\">" +
                    '<input type="checkbox" name="subSelect" style="vertical-align: middle;" value='+obj.userSeq+' />' +
                    "<em></em></label>";
        }}
        , {no:2, col:'rowNum', name:'순서'}
        , {no:3, col:'auth', name:'권한', custom:function(obj){
            var auth = obj.auth;
            if(admin.getAuthStr(auth) === code.authStr.CONSULTANT){
                return "<div>컨설턴트</div>";
            }else{
                return "<div>관리자</div>";
            }
        }}
        , {no:4, col:'pendingYn', name:'활동여부',custom:function(obj){
            var pendingYn = obj.pendingYn;
            if(pendingYn == 'Y'){
                return "<div class='star danger'>제한</div>";
            }else{
                return "<div>활동</div>";
            }
        }}
        , {no:5, col:'userId', name:'아이디(이메일)', custom:function (obj) {
                return "<a href='"+admin.urlInfo.managerRegister+"?seq="+obj.userSeq+"' class=\"title\"><u>"+obj.userId+"</u></a>";
        }}
        , {no:6, col:'userNm', name:'이름', custom:function (obj) {
                return "<a href='"+admin.urlInfo.managerRegister+"?seq="+obj.userSeq+"' class=\"title\"><u>"+obj.userNm+"</u></a>";
            }}
        , {no:7, col:'gender', name:'성별'}
        , {no:8, col:'birthday', name:'생년월일'}
        , {no:9, col:'tel', name:'휴대폰 번호'}
        , {no:10, col:'matchingCnt', name:'매칭인원', custom: function (obj){
            return "<a href='#self' class='matchUser' data-seq='"+obj.userSeq+"' >"+obj.matchingCnt+"</a>";
        }}
        , {no:11, col:'creDt', name:'가입일', custom: function (obj){
            return tms.isNotEmpty(obj.creDt)? moment(obj.creDt).format('YYYY-MM-DD HH:mm:ss') : '';
        }}
        , {no:12, col:'loginLasttime', name:'최근 접속일', custom: function (obj){
            return tms.isNotEmpty(obj.loginLasttime)? moment(obj.loginLasttime).format('YYYY-MM-DD HH:mm:ss') : '';
        }}
    ];

    var params = {
        currentPage : no
        , cntPerPage : countPerPage
        , searchText : $("#searchText").val()
        , auth : $("select[name=searchAuth]").val()
        , gender : $("select[name=searchGender]").val()
    };

    var options = {showLoding:true};
    tms.ajaxGetHelper('/mapi/manager',params ,options ,function(rs){
        var data = rs.data;
        var totalCnt = tms.isEmpty(data) ? 0 : data.totalCnt;
        $("#totalCnt").text(tms.setComma(totalCnt));
        var paging = {
            id: '#paging',
            pageNo: no,
            totalCnt: totalCnt,
            countPerPage: countPerPage,
            fn: 'list',
            activeClass: 'on',
            type: 'type1'
        };

        var info = {
            colsInfo: colsInfo
            ,paging : paging
        };

        if(rs.code==0){
            var list = rs.data.managerList;
            if(list==null || list.length==0){
                tableType3("#list", null, info, 12, "내용이 없습니다.");
                return ;
            }
            tableType3("#list", list, info);
            $('.table_style_02 tbody td').css({'textAlign':'center'})
        }else {
            tableType3("#list", null, info, 12, "내용이 없습니다.");
        }
        $("#list table tr td:nth-child(1)").addClass("c");
    });
}


function fnDelList(){
    if($('[name="subSelect"]:checked').length == 0){
        admin.openAlertPopup(admin.message.noDelSelectMsg);
        return;
    }

    admin.openConfirmPopup(admin.message.listDelMsg,function(){
        var param = [];
        $.each( $('[name="subSelect"]:checked') ,function (k,v) {
            param.push($(this).val());
        });
        var params = {
            userSeqList :param
        };

        var options = {showLoding:true};
        tms.ajaxDeleteHelper('/mapi/manager', JSON.stringify(params), options, function (rs) {
            if(rs.code==0){
                admin.openAlertPopup(admin.message.delMsg, function(){
                    list(1);
                    $("#allSelect").prop("checked", false);
                });
            }else{
                alert(admin.message.defaultErrMsg);
            }
        });
    });
}

/* 검색 */
function search() {
    if(tms.isNotEmpty($("#searchText").val()) && $("#searchText").val().length < 2){
        admin.openAlertPopup("2글자 이상 입력해주세요.");
        return;
    }
    list(1);
}

var getComCode = function(callback){
    common.getComCode("GENDER", function(res){
        if(res.code === 0){
            var data = res.data;
            var genderDom = "";
            var codeList = data.comCodeList;
            for(var item in codeList){
                if(codeList[item].mstCd == "GENDER") { //성별
                    genderDom += '<option value="'+codeList[item].dtlCd+'">'+codeList[item].dtlNm+'</option>';
                }
            }
            $("[name=searchGender]").append(genderDom);

            if(typeof callback === "function"){
                callback(res);
            }
        }
    });
}

var getMatchinguser = function(no){
    var colsInfo = [
        {no:1, col:'matchDt', name:'매칭일', custom: function (obj){
            return tms.isNotEmpty(obj.matchDt)? moment(obj.matchDt).format('YYYY-MM-DD HH:mm:ss') : '';
        }}
        , {no:2, col:'userId', name:'아이디'}
        , {no:3, col:'userNm', name:'이름'}
        , {no:4, col:'pendingYn', name:'상세보기',custom:function(obj){
            return "<a href='"+admin.urlInfo.memberInfo+"?seq="+obj.userSeq+"' class='button tiny'>상세보기</a>"
        }}
    ];

    var params = {
        currentPage : no
        , cntPerPage : countPerPage
        , managerUserSeq: managerUserSeq
    };
    tms.ajaxGetHelper("/mapi/manager/matching/user", params, null, function(rs){
        var data = rs.data;
        var totalCnt = data.totalCnt;
        var paging = {
            id: '#rPaging',
            pageNo: no,
            totalCnt: totalCnt,
            countPerPage: countPerPage,
            fn: 'getMatchinguser',
            activeClass: 'on',
            type: 'type1'
        };

        var info = {
            colsInfo: colsInfo
            ,paging : paging
        };

        if(rs.code==0){
            var list = rs.data.list;
            tableType3("#matchingList", list, info);
        }else {
            tableType3("#matchingList", null, info, 4, "매칭된 회원이 없습니다.");
        }

        layer_OPEN('#matchingPop');
    });
}