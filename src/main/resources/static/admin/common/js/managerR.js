var seq = tms.getParameterByName("seq");
var message = {
    required: {
        userId: '아이디를 입력해주세요.',
        auth: '권한을 선택해주세요',
        title: '제목을 입력해주세요',
        userNm: '이름을 입력해주세요',
        gender: '성별을 선택해주세요',
        birthday: '생년월일을 입력해주세요',
        tel: '휴대폰번호를 입력해주세요',
        kakaoId: '카카오 아이디를 입력해주세요'
    },
    validation: {
        email: {
            2: '@를 포함하여 이메일 형식으로 입력해주세요.'
        },
        tel: {
            2: '-를 제외한 정확한 번호를 입력해주세요.'
        }
    }
}


var pagePath = "";

var addrArr = new Array();
$(function(){
    /*페이지 경로 및 설명 설정*/
    $("#RExpShow").append("새로운 컨설턴트를 등록할 수 있습니다.");
    $("#DExpShow").append("컨설턴트 상세 정보를 조회 및 수정하거나 삭제할 수 있습니다.");
    $("#RNameShow").append("컨설턴트 등록");
    $("#DNameShow").append("컨설턴트 상세");

    if(tms.isNotEmpty(seq)){
        detailCtrl();

        $(".editNone").hide();
        $(".edit").show();
        $("[name=userId]").attr("disabled", true);
        getDetail();
    } else {
        registerCtrl();
        $(".managerDelete").hide();

        //가능서비스, 가능대상, 성별조회
        getComCode();
    }

    //포맷 컨트롤
    tms.setFormatCntrol();

    /* 등록 */
    $(".managerSave").on("click", function(){
        fnSave();
    });

    /*목록*/
    $(".managerCancel").on("click", function(){
        location.href = admin.urlInfo.managerList;
    });

    /* 삭제 */
    $(".managerDelete").on("click", function(){
        fnDel();
    });
});


/* 저장 */
var fnSave = function(){
    var frm = "#frm";

    var requiredChk = tmsValidation.requiredAllChk(frm, message); //필수체크
    var validChk = tmsValidation.validInputAllChk(frm, message); //형식체크

    $("[data-required], [data-valid]").parents(".input_title_wraper").removeClass("invalid").find(".invalid_txt").hide();
    for(var i in requiredChk){
        $("[data-required='"+requiredChk[i].code+"']").parents(".input_title_wraper").addClass("invalid").find(".invalid_txt").text(requiredChk[i].msg).show();
    }

    for(var i in validChk){
        $("[data-valid='"+validChk[i].code+"']").parents(".input_title_wraper").addClass("invalid").find(".invalid_txt").text(validChk[i].msg).show();
    }

    if(requiredChk.length > 0 || validChk.length > 0){
        return;
    }

    var form = $(frm)[0];
    var param = new FormData(form);
    var uploadFile = $("input[name=uploadFile]");
    var file = uploadFile[0].files;
    if(file.length > 0 && file[0]){
        param.append('file', file[0]);
    }

    var url = "/mapi/manager";
    if(tms.isNotEmpty(seq)){ //수정
        url = "/mapi/manager/edit";
        param.append('userSeq', seq);

        //제한
        if(tms.isEmpty(param.get("pendingYn"))){
            param.append('pendingYn', 'N');
        }
    }

    tms.ajaxMulitpartHelper(url, param, null,function(rs){
        var code = rs.code;
        if(code === 0){
            admin.openAlertPopup(admin.message.saveMsg , function(){
                location.href = admin.urlInfo.managerList;
            });
        } else if(code === 200) {
            $("input[name=userId]").parents(".input_title_wraper").addClass("invalid").find(".invalid_txt").text("등록된 이메일 입니다.").show().end().focus();
        } else if(code == 100){
            alert("저장중 오류가 발생하였습니다.\n잠시후 다시 시도해주세요");

        }
    });
}

/* 공통코드 조회 */
var getComCode = function(callback){
    common.getComCode("RANGE_SERVICE, RANGE_TARGET, GENDER", function(res){
        if(res.code === 0){
            var data = res.data;
            var serviceDom = "";
            var tartgetDom = "";
            var genderDom = "";
            var codeList = data.comCodeList;
            for(var item in codeList){
                if(codeList[item].mstCd == "RANGE_SERVICE"){ //서비스
                    serviceDom += '<label class="checkbox primary"><input type="checkbox" name="serviceChk" value="'+codeList[item].dtlCd+'" /><em></em><span class="object" style="width: 200px">'+codeList[item].dtlNm+'</span></label>';
                } else if(codeList[item].mstCd == "RANGE_TARGET"){ //대상
                    tartgetDom += '<label class="checkbox primary"><input type="checkbox" name="targetChk" value="'+codeList[item].dtlCd+'" /><em></em><span class="object" style="width: 200px">'+codeList[item].dtlNm+'</span></label>';
                } else if(codeList[item].mstCd == "GENDER") { //성별
                    genderDom += '<label class="radio primary mr35"><input type="radio" name="gender" data-mapping="gender" data-required="gender" value="'+codeList[item].dtlCd+'" /><em></em> <span>'+codeList[item].dtlNm+'</span></label>';
                }
            }

            $("#targetChkBox").append(tartgetDom);
            $("#serviceChkBox").append(serviceDom);
            $("#genderDom").prepend(genderDom);

            if(typeof callback === "function"){
                callback(res);
            }
        }
    });
}

/* 삭제 */
var fnDel = function(){
    admin.openConfirmPopup("컨설턴트를<br />삭제하시겠습니까?",function(){
        tms.ajaxDeleteHelper("/mapi/manager", JSON.stringify({userSeqList: [Number(seq)]}), null, function(res){
            if(res.code === 0){
                admin.openAlertPopup("컨설턴트가 삭제되었습니다.", function(){
                    location.href = admin.urlInfo.managerList;
                });
            } else {
                alert("삭제중 오류가 발생하였습니다.\n잠시후 다시 시도해주세요.");
            }
        });
    });
}

/* 상세조회*/
var getDetail = function(){
    tms.ajaxGetHelper("/mapi/manager/item", {userSeq: seq}, null, function(res){
        var data = res.data;
        //가능서비스, 가능대상, 성별 조회

        getComCode(function(){
            if(res.code === 0){
                if(tms.isNotEmpty(data.fileSeq)){
                    data.fileInfo = "등록된 사진명 : <a href='/api/file/download?fileSeq="+data.fileSeq+"&category=0'>"+data.orgFileNm+"</a>";
                }
                tmsInput.detailMapping(data);

                if(tms.isNotEmpty(data.language)){
                    var langStrArr = data.language.split(',');
                    for(var i in langStrArr){
                        $('#language').tagEditor('addTag', langStrArr[i]);
                    }
                }

                //활동지역
                var addrStr = data.region;
                if(tms.isNotEmpty(addrStr)){
                    var addrStrArr = addrStr.split(',');
                    for(var i in addrStrArr){
                        addrArr.push(addrStrArr[i]);
                    }

                    var addrList = data.addrList;
                    for(var i in addrList){
                        $("#addrArea").append("<a href='#self' class='button' data-seq='"+addrList[i].minorSeq+"'><em>"+addrList[i].addrMajor+" "+addrList[i].addrMinor+"</em><img class='address_select ml10' src='/admin/images/common/ico_tab_close.png' /></a>");
                    }
                }

                //가능대상
                var rangeTargetStr = data.rangeTarget;
                if(tms.isNotEmpty(rangeTargetStr)){
                    var targetArr = rangeTargetStr.split(',');
                    for(var i in targetArr){
                        $("input[name=targetChk][value='"+targetArr[i]+"']").prop("checked", true);
                    }
                }
                //가능 서비스
                var rangeServiceStr = data.rangeService;
                if(tms.isNotEmpty(rangeServiceStr)){
                    var serviceArr = rangeServiceStr.split(',');
                    for(var i in serviceArr){
                        $("input[name=serviceChk][value='"+serviceArr[i]+"']").prop("checked", true);
                    }
                }
            } else if(res.code === 102){
                alert("삭제된 데이터 입니다.");
                location.href = admin.urlInfo.managerList;
            }
        });
    });
}

var detailCtrl = function () {
    /*페이지 명 및 설명 제어*/
    $("#RExpShow").hide();
    $("#DExpShow").show();
    $("#RNameShow").hide();
    $("#DNameShow").show();

    /*페이지 경로 제어*/
    pagePath += "컨설턴트 상세";
    $("#pagePath").append(pagePath);
    $(".managerSave").text("수정");
}

var registerCtrl = function () {
    /*페이지 명 및 설명 제어*/
    $("#RExpShow").show();
    $("#DExpShow").hide();
    $("#RNameShow").show();
    $("#DNameShow").hide();

    /*페이지 경로 제어*/
    pagePath += "컨설턴트 등록";
    $("#pagePath").append(pagePath);
}