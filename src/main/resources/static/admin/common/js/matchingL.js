var matching = (function ($, win, doc) {
    var list = [];
    var getList = function(callback){
        var colsInfo = [
            {no:1, col:'userId', name:'아이디'}
            , {no:2, col:'userNm', name:'이름'}
            , {no:3, col:'gender', name:'성별',custom:function(obj){
                return common.getComCodeNm(codeArr, "GENDER", obj.gender);
            }}
            , {no:4, col:'region', name:'활동지역',custom:function(obj){
                var region = obj.region;
                if(tms.isNotEmpty(region)){
                    var strArr = new Array();
                    region = region.split(',');
                    for(var i in region){
                        strArr.push(common.getAddrNmBySeq(siArr, gunguArr, region[i]));
                    }
                    return strArr.toString();
                } else {
                    return "";
                }
            }}
            , {no:5, col:'rangeTarget', name:'가능대상', custom:function(obj){
                var rangeTarget = obj.rangeTarget;
                if(tms.isNotEmpty(rangeTarget)){
                    var strArr = new Array();
                    rangeTarget = rangeTarget.split(',');
                    for(var i in rangeTarget){
                        strArr.push(common.getComCodeNm(codeArr, "RANGE_TARGET", rangeTarget[i]));
                    }
                    return strArr.toString();
                } else {
                    return "";
                }
            }}
            , {no:6, col:'rangeService', name:'가능서비스', custom:function(obj){
                var rangeService = obj.rangeService
                if(tms.isNotEmpty(rangeService)){
                    var strArr = new Array();
                    rangeService = rangeService.split(',');
                    for(var i in rangeService){
                        strArr.push(common.getComCodeNm(codeArr, "RANGE_SERVICE", rangeService[i]));
                    }
                    return strArr.toString();
                } else {
                    return "";
                }
            }}
            , {no:7, col:'matchYn', name:'매칭',custom:function(obj){
                var ischeck = (obj.matchYn == 'Y')? 'checked' : '';
                return '<label class="slide_toggle primary"><input type="checkbox" '+ ischeck +' value="'+obj.userSeq+'" onclick="matching.matchingControl(this)" /><em></em></label>'
            }}
        ];

        var params = {
            userSeq: seq
        };
        var options = {showLoding:true};
        tms.ajaxGetHelper("/mapi/manager/matching", params, options, function(rs){
            var data = rs.data;
            var totalCnt = 0;
            var info = {
                colsInfo: colsInfo
            };

            if(rs.code==0){
                list = data.list;
                totalCnt = list.length;
                tableType3("#list", list, info);
            }else {
                tableType3("#list", null, info, 7, "조회된 컨설턴트가 없습니다.");
            }

            $("#totalCnt").text(tms.setComma(totalCnt));

            if(typeof callback == "function"){
                callback(rs);
            }
        });
    }

    var matchingControl = function(obj){
        var $this = $(obj);
        if($this.is(":checked")){ //매칭
            tms.ajaxPostHelper("/mapi/member/matching/consultant", JSON.stringify({managerUserSeq: $this.val(), userSeq:seq}), null, function(){
                list.forEach(function (item, index, obj) {
                    if(item.userSeq == $this.val()){
                        item.matchYn = "Y";
                    }
                });
            });
        } else { //삭제
            tms.ajaxDeleteHelper("/mapi/member/matching/consultant", JSON.stringify({managerUserSeq: $this.val(), userSeq:seq}), null, function(){
                list.forEach(function (item, index, obj) {
                    if(item.userSeq == $this.val()){
                        item.matchYn = "N";
                    }
                });
            });
        }

    }

    var searchOption = {
        gender: '',
        addr: [],
        target: [],
        service: [],
        matchYn: ''
    }
    var search = function(options){
        options = options || {};
        for (var prop in searchOption)  {
            options[prop] = typeof options[prop] !== 'undefined' ? options[prop] : searchOption[prop];
        }
        var searchList = list.filter(function(obj){
            var region = obj.region;
            for(var i in options){
                var resultFlag = true;
                if(tms.isNotEmpty(options[i])){
                    if(i == "gender"){
                        if(obj.gender != options[i]){
                            resultFlag = false;
                            break;
                        }
                    }

                    if(i == "addr"){
                        if(tms.isNotEmpty(region)){ //시도
                            var regionArr = region.split(',');
                            if(intersect(regionArr, options[i]).length == 0){
                                resultFlag = false;
                                break;
                            }
                        } else {
                            resultFlag = false;
                        }
                    }

                    if(i == "target"){
                        if(tms.isNotEmpty(obj.rangeTarget)){
                            if(obj.rangeTarget.indexOf(options[i]) == -1){
                                resultFlag = false;
                                break;
                            }
                        } else {
                            resultFlag = false;
                            break;
                        }
                    }

                    if(i == "service"){
                        if(tms.isNotEmpty(obj.rangeService)){
                            if(obj.rangeService.indexOf(options[i]) == -1){
                                resultFlag = false;
                                break;
                            }
                        } else {
                            resultFlag = false;
                            break;
                        }
                    }

                    if(i == "matchYn"){
                        if(obj.matchYn != options[i]){
                            resultFlag = false;
                            break;
                        }
                    }
                }
            }
            return resultFlag;
        });

        var colsInfo = [
            {no:1, col:'userId', name:'아이디'}
            , {no:2, col:'userNm', name:'이름'}
            , {no:3, col:'gender', name:'성별',custom:function(obj){
                return common.getComCodeNm(codeArr, "GENDER", obj.gender);
            }}
            , {no:4, col:'region', name:'활동지역',custom:function(obj){
                var region = obj.region;
                if(tms.isNotEmpty(region)){
                    var strArr = new Array();
                    region = region.split(',');
                    for(var i in region){
                        strArr.push(common.getAddrNmBySeq(siArr, gunguArr, region[i]));
                    }
                    return strArr.toString();
                } else {
                    return "";
                }
            }}
            , {no:5, col:'rangeTarget', name:'가능대상', custom:function(obj){
                var rangeTarget = obj.rangeTarget;
                if(tms.isNotEmpty(rangeTarget)){
                    var strArr = new Array();
                    rangeTarget = rangeTarget.split(',');
                    for(var i in rangeTarget){
                        strArr.push(common.getComCodeNm(codeArr, "RANGE_TARGET", rangeTarget[i]));
                    }
                    return strArr.toString();
                } else {
                    return "";
                }
            }}
            , {no:6, col:'rangeService', name:'가능서비스', custom:function(obj){
                var rangeService = obj.rangeService
                if(tms.isNotEmpty(rangeService)){
                    var strArr = new Array();
                    rangeService = rangeService.split(',');
                    for(var i in rangeService){
                        strArr.push(common.getComCodeNm(codeArr, "RANGE_SERVICE", rangeService[i]));
                    }
                    return strArr.toString();
                } else {
                    return "";
                }
            }}
            , {no:7, col:'matchYn', name:'매칭',custom:function(obj){
                var ischeck = (obj.matchYn == 'Y')? 'checked' : '';
                return '<label class="slide_toggle primary"><input type="checkbox" '+ ischeck +' value="'+obj.userSeq+'" onclick="matching.matchingControl(this)" /><em></em></label>'
            }}
        ];

        if(searchList.length == 0){
            tableType3("#list", null, searchList, 7, "조회된 컨설턴트가 없습니다.");
        } else {
            tableType3("#list", searchList, {colsInfo: colsInfo});
        }

        $("#totalCnt").text(tms.setComma(searchList.length));
    }

    function intersect(a, b) {
        var tmp={}, res=[];
        for(var i=0;i<a.length;i++) tmp[a[i]]=1;
        for(var i=0;i<b.length;i++) if(tmp[b[i]]) res.push(b[i]);
        return res;
    }

    return {
        getList: getList,
        matchingControl: matchingControl,
        search: search
    }

}(jQuery, window, document));