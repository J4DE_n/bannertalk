var countPerPage = 10;

$(document).ready(function(){

    list(1);

    /* 검색버튼 클릭 */
    $("#searchBtn").on("click", function(){
        search();
    });  /*2번 항목*/

    /* 새로고침 버튼 클릭 */
    $("#refreshBtn").on("click", function(){
        location.href="/page/admin/member/list";
    });  /*3번 항목*/

    /* 검색 엔터 */
    $("#searchText").on("keyup", function(e){
        if(e.keyCode == 13){
            search();
        }
    });   /*4번 항목*/

});

/* 등록 클릭 시 */
$(document).on('click','#memberEnroll',function(){
    location.href="/page/admin/member/detail";
});

/* 삭제 클릭 시 */
$(document).on('click','#memberDelete',function(){
    deleteTerm()
});

/* 전체 선택 */
$(document).on('click','#allSelect',function(){
    var allSelect = $("#allSelect").is(":checked");
    if(allSelect){
        $(".memberSelect").prop('checked', true);
        $('#btnDel').show();
    }else{
        $(".memberSelect").prop('checked', false);
        $('#btnDel').hide();
    }
});

$(document).on('click','[name="memberSelect"]',function () {
    $('#allSelect').prop("checked",false );
    if($('[name="memberSelect"]:checked').length > 0){
        $('#btnDel').show();
    } else {
        $('#btnDel').hide();
    }
});

/* 리스트 조회 */
function list(no){
    $('#btnDel').hide();
    var colsInfo = [
        /*{no:1, col:'chgSeq', name:'선택', custom:function(obj){
                return "<label class=\"checkbox primary\">" +
                    '<input type="checkbox" class="managerSelect" name="managerSelect" style="vertical-align: middle;" value='+obj.chgSeq+' />' +
                    "<em></em></label>";
            }}
        , {no:2, col:'rowNum', name:'순서', custom:function(obj){
                return "<span>"+obj.rowNum+"</span>";
            }}
        , {no:3, col:'displayYn', name:'게시 여부',custom:function(obj){
                return "<span>"+obj.displayYn+"</span>";
            }}
        , {no:4, col:'title', name:'제목',custom:function(obj){
                return "<a href='/page/admin/manager/detail?seq="+obj.chgSeq+"' class=\"title\">"+obj.title+"</a>";
            }}
        , {no:5, col:'creId', name:'등록자',custom:function(obj){
                return "<div>"+obj.creId+"</div>";

                /!*등록자, 이메일 형식 맞춰야함*!/

            }}
        , {no:6, col:'writeDt', name:'등록일',custom:function(obj){
                return "<span>"+obj.writeDt+"</span>";
            }}*/

        {no:1, col:'USER_SEQ', name:'선택', custom:function(obj){
                return "<label class=\"checkbox primary\">" +
                    '<input type="checkbox" class="memberSelect" name="memberSelect" style="vertical-align: middle;" value='+obj.userSeq+' />' +
                    "<em></em></label>";
            }}
        , {no:2, col:'rowNum', name:'순서'}
        , {no:3, col:'userId', name:'아이디',custom:function(obj){
                return "<a href='/page/admin/member/detail?seq="+obj.userSeq+"' class=\"userId\">"+obj.userId+"</a>";
            }}
        , {no:4, col:'userNm', name:'이름', custom:function (obj) {
                return "<div>"+obj.userNm+"</div>";
            }}
        , {no:5, col:'kakaoId', name:'카카오 아이디'}
        , {no:6, col:'birthday', name:'생년월일'}
        , {no:7, col:'birthday', name:'지역'}
        , {no:8, col:'gender', name:'성별'}
        , {no:9, col:'creDt', name:'회원가입일'}
    ];

    var params = {
        currentPage : no
        , cntPerPage : 10
        , startDt : $("#startDate").val()
        , endDt : $("#endDate").val()
    };

    /* 검색조건 설정 */
    params.searchText = $("#searchText").val();
    params.gender = $("select[name=gender]").val();
    /*지역 검색조건 넣어야한다.*/

    var options = {showLoding:true};
    tms.ajaxGetHelper('/mapi/member',params ,options ,function(rs){
        var data = rs.data;
        var totalCnt = tms.isEmpty(data)? 0 : data.totalCnt;

        var paging = {
            id: '#paging',
            pageNo: no,
            totalCnt: totalCnt,
            countPerPage: countPerPage,
            fn: 'list',
            activeClass: 'on',
            type: 'type1'
        };

        /*var paging = {
            id: '#paging',
            pageNo: no,
            totalCnt: rs.data.cnt,
            countPerPage: 10,
            fn: 'list',
            activeClass: 'current',
            type: 'type1'
        };*/

        var info = {
            colsInfo: colsInfo
            ,paging : paging
        };

        if(rs.code==0){
            var list = rs.data.memberList;

            if(list==null || list.length==0){
                tableType3("#list", null, info, 7, "내용이 없습니다.");
                return ;
            }

            tableType3("#list", list, info);
            $('.table_style_02 tbody td').css({'textAlign':'center'})
        }else {
            tableType3("#list", null, info, 7, "내용이 없습니다.");
        }
    });
}
/* 사용자 삭제 */
function deleteTerm(){
    var selCnt = $("input[name=memberSelect]:checked").length;

    if (selCnt == 0) {
        $('.modal .title').text('선택한 게시물이 없습니다.');
        layer_OPEN('.modal');
        return;
    }else{
        /* delete popup */
        layer_OPEN('.popup_delete');
    }
}

/* 사용자 여러개 삭제 */
function realDeleteTerm(){
    var param = [];

    $.each( $('[name="memberSelect"]:checked') ,function (k,v) {
        param.push( $(this).val() );
    });
    var params = {
        userSeqList :param
    };

    var options = {showLoding:true};
    tms.ajaxDeleteHelper('/mapi/member', JSON.stringify(params), options, function (rs) {
        if(rs.code==0){
            list(1);
            $("#allSelect").removeAttr("checked");
        }else{
            alert('잠시 후 시도하여주세요');
        }
    });



}

/* 레이어 클로즈 */
function closePopup(obj_selector){
    var obj = $(obj_selector);
    obj.stop().animate({'opacity':0},300,function (){
        $(this).css({'display':'none'});
    });
    realDeleteTerm();
};


/* 한자리가 되는 숫자에 "0"을 넣어주는 함수 */
function addzero(n){
    return n < 10 ? "0" + n: n;
}

/* 날짜 계산 */
function dateInput(month,day){

    /* 기존 값 초기화 */
    $("#startDate").val("");
    $("#endDate").val("");

    var date = new Date();
    var start = new Date(Date.parse(date)-month* 1000 * 60 * 60 * 24);
    var today = new Date(Date.parse(date)-day* 1000 * 60 * 60 * 24);

    if(month < 10){
        start.setMonth(start.getMonth()-month);
    }
    var yyyy = start.getFullYear();
    var mm = start.getMonth()+1;
    var dd = start.getDate();

    var t_yyyy = today.getFullYear();
    var t_mm = today.getMonth()+1;
    var t_dd = today.getDate();


    if(day>0){
        $("#endDate").val(yyyy+'-'+addzero(mm)+'-'+addzero(dd));
        $("#startDate").val(t_yyyy+'-'+addzero(t_mm)+'-'+addzero(t_dd));
    }else{
        $("#startDate").val(yyyy+'-'+addzero(mm)+'-'+addzero(dd));
        $("#endDate").val(t_yyyy+'-'+addzero(t_mm)+'-'+addzero(t_dd));
    }
}

/* 검색 */
function search() {
    if(tms.isNotEmpty($("#searchText").val()) && $("#searchText").val().length < 2){
        alert("2글자 이상 입력해주세요.");
        return;
    }
    list(1);
}

/*/!* 리스트 페이지 상태 변경 *!/
function updateNoticeShowYn(seq, displayYn){
    var params = {
        chgSeq : seq
        , displayYn : displayYn
    }
    boardUpdate(params);
} 필요없을거같은데*/

/*
/!* 이벤트 노출 여부만 리스트페이지에서 수정 *!/
function boardUpdate(params){
    tms.ajaxPostHelper('/mapi/notice/update', JSON.stringify(params), null,function(rs){
        if(rs.code == 100){
            alert("잠시후 시도하여 주세요");
        }
    });
} 필요없을거같은데*/
