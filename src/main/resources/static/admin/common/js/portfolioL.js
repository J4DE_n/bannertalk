var countPerPage = 10;

$(function(){
    //list(1);
});

/***********************************
            Event Controll
 ***********************************/
/* 새로고침 버튼 클릭 */
$(document).on("click", "#refreshBtn",function(e){
    location.href=admin.urlInfo.portfolioList;
});

/* 날짜토글 버튼 클릭 */
$(document).on("click", "input[name=text_toggle1]", function(e){
    dateInput($(this).data("month"), $(this).data("day"));
});

/* 검색버튼 클릭 */
$(document).on("click", "#searchBtn", function(e){
    search();
});

/* Enter키 누른 경우 검색버튼 */
$(document).on("keyup", "#searchText", function(e){
    if(e.keyCode == 13){
        search();
    }
});

/* 체크박스 컨트롤 */
$(document).on('click','#allSelect',function() {
    tms.checkboxAllControl("#allSelect", "[name=subSelect]")
    showDelBtn();
});
$(document).on('click','[name="subSelect"]',function () {
    tms.checkboxControl("#allSelect", "[name=subSelect]");
    showDelBtn();
});

/* 삭제 */
$(document).on("click", "#btnDel", function(){
    fnDelList();
});

/* 등록 클릭 시 */
$(document).on('click','#register',function(){
    location.href=admin.urlInfo.portfolioRegister;
});



/***********************************
            Function List
 ***********************************/
/* 검색 */
function search() {
    if(tms.isNotEmpty($("#searchText").val()) && $("#searchText").val().length < 2){
        alert(admin.message.twoWordErrMsg);
        return;
    }
    list(1);
}

/* 리스트 조회 */
function list(no){
    // Table Column Define

    var colsInfo = [
        {no:1, col:'ptfSeq', name:'선택', custom:function(obj){
            return '<label class="checkbox primary">'
                + '<input type="checkbox" name="subSelect" style="vertical-align: middle;" value='+obj.ptfSeq+' /><em></em>'
                + '</label>'
        }}
        , {no:2, col:'rowNum', name:'순서'}
        , {no:3, col:'displayYn', name:'노출 여부', custom:function(obj){
            var displayYn = obj.displayYn;
            if(displayYn == 'Y')
                return "<div>노출</div>";
            else
                return "<div>노출 안함</div>";
        }}
        , {no:4, col:'title', name:'제목',custom:function(obj){
            return "<a href='"+admin.urlInfo.portfolioRegister+"?ptfSeq="+obj.ptfSeq+"' class=\"title\"><u>"+obj.title+"</u></a>";
        }}
        , {no:5, col:'creId', name:'등록자', custom:function (obj) {
            return "<div>"+obj.userNm+"<br>("+obj.userId+")"+"</div>";
        }}
        , {no:6, col:'writeDt', name:'등록일', custom:function (obj) {
            return tms.isNotEmpty(obj.writeDt)? moment(obj.writeDt).format('YYYY-MM-DD') : '';
        }}
    ];

    // Parameter For Search
    var params = {
        currentPage : no
        , cntPerPage : countPerPage
        , startDate : $("#startDate").val()
        , endDate : $("#endDate").val()
        , searchText : $("#searchText").val()
        , displayYn : $("select[name=searchDisplayYn]").val()
    };

    // Table Content Setting
    var options = {showLoding:true};
    tms.ajaxGetHelper('/mapi/portfolio', params, options,function(rs){
        var data = rs.data;

        // Number of Search Results
        var totalCnt = tms.isEmpty(data)? 0 : data.totalCnt;
        $("#totalCnt").text(tms.setComma(totalCnt));

        // Paging Setting
        var paging = {
            id: '#paging',
            pageNo: no,
            totalCnt: totalCnt,
            countPerPage: countPerPage,
            fn: 'list',
            activeClass: 'on',
            type: 'type1'
        };

        // List Info
        var info = {
            colsInfo: colsInfo
            ,paging : paging
        };

        if(rs.code==0){
            var list = rs.data.portfolioList;

            if(list == null || list.length == 0){
                tableType3("#list", null, info, 6, admin.message.noSearchMsg);
                return ;
            }

            tableType3("#list", list, info);
            $('.table_style_02 tbody td').css({'textAlign':'center'})
        }else {
            // 검색결과 오류 -> 서버오류 변경 필요
            tableType3("#list", null, info, 6, admin.message.noSearchMsg);
        }
        
        $("#list table tr td:nth-child(1)").addClass("c");
    });
}

/* 삭제 */
function fnDelList(){
    // 체크박스 선택이 안 된 경우
    if($('[name="subSelect"]:checked').length == 0){
        admin.openAlertPopup(admin.message.noDelSelectMsg);
        return;
    }

    admin.openConfirmPopup(admin.message.listDelMsg,function(){
        // 체크박스 항목 가져오기
        var choose = [];
        $.each( $('[name="subSelect"]:checked') ,function (k,v) {
            choose.push($(this).val());
        });

        var params = {
            ptfSeqList :choose
        };

        var options = {showLoding:true};
        tms.ajaxDeleteHelper('/mapi/portfolio', JSON.stringify(params), options, function (rs) {
            if(rs.code==0){
                admin.openAlertPopup(admin.message.delMsg, function(){
                    list(1);
                    $("#allSelect").removeAttr("checked");
                });
            }else{
                alert(admin.message.delErrMsg);
            }
        });
    });
}

/* 삭제 버튼 노출 */
var showDelBtn = function(){
    if($('[name="subSelect"]:checked').length == 0){
        $("#btnDel").hide();
    } else {
        $('#btnDel').show();
    }
};

/* 한자리가 되는 숫자에 "0"을 넣어주는 함수 */
function addzero(n){
    return n < 10 ? "0" + n: n;
}

/* 날짜 계산 */
function dateInput(month, day){
    /* 기존 값 초기화 */
    $("#startDate").val("");
    $("#endDate").val("");

    var date = new Date();
    var today = new Date(Date.parse(date));

    if(month > 0){
        for(var i = 0; i < month; i++){
            if(today.getMonth()-i < 0){
                day += (new Date(today.getFullYear() - 1, today.getMonth()-i + 12, 0)).getDate()
            }else{
                day += (new Date(today.getFullYear(), today.getMonth()-i, 0)).getDate()
            }
        }
    }

    var start = new Date(Date.parse(date) - day* 1000 * 60 * 60 * 24);

    if(day>0){
        $("#startDate").val(start.getFullYear()+'-'+addzero(start.getMonth()+1)+'-'+addzero(start.getDate()));
        $("#endDate").val(today.getFullYear()+'-'+addzero(today.getMonth()+1)+'-'+addzero(today.getDate()));
    }
}