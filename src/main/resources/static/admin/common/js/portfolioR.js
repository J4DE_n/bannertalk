var seq = tms.getParameterByName("ptfSeq");

/* 메시지 영역 */
var message = {
    required: {
        writeDt: '등록일을 입력해주세요'
        ,title: '제목을 입력해주세요'
        ,summary: '요약설명을 입력해주세요.'
    }
}

$(function(){
    var setting = {
        pagePath : "포토폴리오 등록"
        , pageComment : "새로운 포토폴리오를 등록할 수 있습니다."
        , saveBtnName : "등록"
        , delBtnShow : false,
    }

    if(tms.isNotEmpty(seq)){
        setting.pagePath = "포토폴리오 수정";
        setting.saveBtnName = "수정";
        setting.delBtnShow  = true;
        $("input[name=tnFileDelYn]").val("N");
        $("input[name=detailFileDelYn]").val("N");

        getDetail();            // 상세내용 조회
    } else {
        setWriteDt();           // 등록일 초기 설정
    }

    // 화면 셋팅
    displaySetting(setting);
});

/***********************************
            Event Controll
 ***********************************/
/* 저장 버튼 */
$(document).on("click", ".save", function(e){
    fnSave();
});

/* 삭제 버튼 */
$(document).on("click", ".delete", function(e){
    fnDel();
});

/* 취소 버튼 */
$(document).on("click", ".cancels", function(e){
    fnCancel();
});

/* 첨부 파일 컨트롤 */
$(document).on("change", "input[name=tnFile]", function(e){
    fileChk($(this));
});
$(document).on("change", "input[name=detailFile]", function(e){
    fileChk($(this));
});

/* 유효성 체크 해제 */
$(document).on("blur", "#writeDt", function(e){
    if(! tms.isEmpty($(this).val())){
        $(this).next().hide();
        $(this).parents(".input_title_wraper").removeClass("invalid");
    }
});

$(document).on("blur", "#title", function(e){
    if(! tms.isEmpty($(this).val())){
        $(this).next().hide();
        $(this).parents(".input_title_wraper").removeClass("invalid");
    }
});

$(document).on("blur", "#summary", function(e){
    if(! tms.isEmpty($(this).val())){
        $(this).next().hide();
        $(this).parents(".input_title_wraper").removeClass("invalid");
    }
});

$(document).on("blur", "#tnFile", function(e){
    if(! tms.isEmpty($(this).val())){
        $(this).next().hide();
        $(this).parents(".input_title_wraper").removeClass("invalid");
    }
});

$(document).on("blur", "#detailFile", function(e){
    if(! tms.isEmpty($(this).val())){
        $(this).next().hide();
        $(this).parents(".input_title_wraper").removeClass("invalid");
    }
});

/***********************************
            Function List
 ***********************************/
/* 등록일 초기 설정 */
var setWriteDt = function(){
    var now = new Date();
    var year = now.getFullYear();
    var mon = (now.getMonth()+1)>9 ? ''+(now.getMonth()+1) : '0'+(now.getMonth()+1);
    var day = now.getDate()>9 ? ''+now.getDate() : '0'+now.getDate();
    var chan_val = year + '-' + mon + '-' + day;
    $("#writeDt").val(chan_val);
}

/* 상세 조회 */
var getDetail = function(){
    var options = {showLoding:true};
    tms.ajaxGetHelper('/mapi/portfolio/item',  {ptfSeq: seq} ,options ,function(res){
        if(res.code === 0){
            var data = res.data;

            /* 등록일 설정 */
            var dt = data.writeDt.split(" ");
            data.writeDt = dt[0];

            /* 노출 여부 설정 */
            if(data.displayYn == "Y"){
                $(".displayYn").attr("checked", true);
            }else{
                $(".displayYn").attr("checked", false);
            }

            /* 파일 이름 설정 */
            if(tms.isNotEmpty(data.tnFile)){
                data.tnFileNm = data.tnFile.orgFileNm;
                $("[data-mapping='tnFileNm']").closest(".input_title_wraper").show();
            }
            if(tms.isNotEmpty(data.detailFile)){
                data.detailFileNm = data.detailFile.orgFileNm;
                $("[data-mapping='detailFileNm']").closest(".input_title_wraper").show();
            }

            /* 상세 정보 맵핑 */
            tmsInput.detailMapping(data);

            /* 첨부파일 다운로드 설정 */
            if(tms.isNotEmpty(data.tnFileGrpSeq)){
                $("#tnFileDown").attr("href", "/api/file/download?fileSeq="+res.data.tnFileGrpSeq+"&category=1");
            }
            if(tms.isNotEmpty(data.detailFileGrpSeq)){
                $("#detailFileDown").attr("href", "/api/file/download?fileSeq="+res.data.detailFileGrpSeq+"&category=1");
            }
        }
    });
}

/* 포토폴리오 등록 & 수정 */
var fnSave = function(){
    var frm = "#form";
    var form = $(frm)[0];
    var param = new FormData(form);

    if($("input[name=tnFileDelYn]").val() != "N" && $("input[name=tnFile]").get(0).files.length < 1){
        message.required["tnFile"] = "메인 썸네일 이미지를 등록해주세요.";
        $("#tnFile").attr("data-required", "tnFile")
    }

    if($("input[name=detailFileDelYn]").val() != "N" && $("input[name=detailFile]").get(0).files.length < 1){
        message.required["detailFile"] = "상세설명 이미지를 등록해주세요.";
        $("#detailFile").attr("data-required", "detailFile")
    }

    var requiredChk = tmsValidation.requiredAllChk(frm, message);   // 필수 체크 항목
    $("[data-required], [data-valid]").parents(".input_title_wraper").removeClass("invalid").find(".invalid_txt").hide();
    for(var i in requiredChk){
        $("[data-required='"+requiredChk[i].code+"']").parents(".input_title_wraper").addClass("invalid").find(".invalid_txt").text(requiredChk[i].msg).show();
    }

    if(requiredChk.length > 0){
        $("[data-required='"+requiredChk[0].code+"']").focus();
        return;
    }

    // 노출 여부
    if($("input:checkbox[name='displayYn']").is(":checked")) {
        param.set("displayYn", "Y");
    }else{
        param.set("displayYn", "N");
    }

    var url = "/mapi/portfolio";
    if(tms.isNotEmpty(seq)){
        url = "/mapi/portfolio/edit";
        param.append("ptfSeq", seq);
    }

    tms.ajaxMulitpartHelper(url, param, null,function(rs){
        if(rs.code==0){
            tms.initInputTxt(frm);
            location.href = admin.urlInfo.portfolioList;
        }else if(rs.code == 100){
            alert("error");
        }
    });
}

/* 포토폴리오 삭제 */
function fnDel(){
    admin.openConfirmPopup(admin.message.detailDelMsg,function(){
        tms.ajaxDeleteHelper("/mapi/portfolio", JSON.stringify({ptfSeqList: [Number(seq)]}), null, function(res){
            if(res.code === 0){
                admin.openAlertPopup(admin.message.delMsg, function(){
                    location.href = admin.urlInfo.portfolioList
                });
            } else {
                alert(admin.message.delErrMsg);
            }
        });
    });
}

/* 취소 버튼 */
var fnCancel = function () {
    admin.openConfirmPopup(admin.message.cancelMsg, function () {
        location.href = admin.urlInfo.portfolioList;
    })
}

/* 파일 확장자 & 크기 체크 */
var fileChk = function(uploadFile){
    var tempFile = uploadFile[0].files;

    var chkSize = tms.checkFileSize(uploadFile[0], 10);
    var chkExt = tms.checkFileExt(tempFile[0].name);

    if(!chkSize){
        alert(admin.message.fOverSizeMsg);
        tms.initInputTxt(".file");
        return;
    }
    if(!chkExt){
        alert(admin.message.fExtErrMsg);
        tms.initInputTxt(".file");
        return;
    }
}

/* 파일 삭제 */
function delFile(obj) {
    admin.openConfirmPopup(admin.message.imgDelMsg, function(){
        if(obj.name == "tnFileDel"){
            $("#form input[name='tnFileDelYn']").val("Y");
            $(obj).parent().remove();
        }else if(obj.name == "detailFileDel"){
            $("#form input[name='detailFileDelYn']").val("Y");
            $(obj).parent().remove();
        }
    });
}

var displaySetting = function(setting){
    $("#pagePath").text(setting.pagePath);
    $("#reg_title").text(setting.pagePath);
    $("#reg_comment").text("");

    $(".save").text(setting.saveBtnName);
    if(setting.delBtnShow){
        $(".delete").show();
    }else{
        $(".delete").hide();
    }
}




