var code = (function ($, win, doc) {
    var  authStr = {
        CONSULTANT: 'CONSULTANT',
        ADMIN: 'ADMIN',
        USER: 'USER'
    };

    var codeMst = {
        PAYMENT_METHOD: "PAYMENT_METHOD",
        PAYMENT_STATE: "PAYMENT_STATE",
        REFUND_STATUS: "REFUND_STATUS"
    };

    return {
        authStr: authStr,
        codeMst: codeMst
    }

}(jQuery, window, document));