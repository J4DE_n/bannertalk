/* 공통function */
var common = (function ($, win, doc) {

    /* 공통코드 호출 */
    var getComCode = function(mstCd, callback){
        tms.ajaxGetHelper("/api/com/code", {mstCdArr: mstCd}, null, function(res){
            if(typeof callback == "function"){
                callback(res);
            }
        });
    };

    /* 주소 조회 */
    var getAddr = function(addrSeq, callback){
        tms.ajaxGetHelper("/api/com/addr", {parentAddrSeq: addrSeq}, null, function(res){
            if(typeof callback == "function"){
                callback(res);
            }
        });
    };

    /* 주소 조회 */
    var getAddrList = function(addrSeqList, callback){
        tms.ajaxGetHelper("/api/com/addr/item", {parentAddrSeq: addrSeqList}, null, function(res){
            if(typeof callback == "function"){
                callback(res);
            }
        });
    };

    /* 주소 조회 (타입으로 조회 SI, GUNGU) */
    var getAddrByType = function(addrType, callback){
        tms.ajaxGetHelper("/api/com/addr", {addrType: addrType}, null, function(res){
            if(typeof callback == "function"){
                callback(res);
            }
        });
    };


    /* 시도 명 매핑*/
    var getAddrNmBySeq = function(sidoArr, gunguArr, seq){
        var dtlObj = gunguArr.filter(function(obj){
            return obj['addrSeq'] == seq;
        })[0];

        var parentObj = sidoArr.filter(function(obj){
            return obj['addrSeq'] == dtlObj['parentAddrSeq'];
        })[0];
        return parentObj['item'] + ' ' +dtlObj['item'];
    }

    /* 공통코드명 매핑 */
    var getComCodeNm  = function(codeArr, mstCd, dtlCd){
        try {
            return codeArr.filter(function(obj){
                return obj['mstCd'] === mstCd && obj['dtlCd'] === dtlCd;
            })[0]['dtlNm'];
        } catch(e) {
            return "";
        }
    }

    var getFrontHostInfo = function() {
        var url = location.href;
        var info = "";
        if(url.indexOf("local")) {
            info = "http://localhost:8080";
        } else if(url.indexOf("local")) {
            info = "http://localhost:8080";
        } else {
            info = "http://localhost:8080";
        }

        return info;
    }


    return {
        getComCode: getComCode,
        getAddr: getAddr,
        getAddrList : getAddrList,
        getAddrByType: getAddrByType,
        getAddrNmBySeq: getAddrNmBySeq,
        getComCodeNm: getComCodeNm,
        getFrontHostInfo: getFrontHostInfo

    }

}(jQuery, window, document));

$(document).on("click","#header", function(){
    sessionStorage.clear();
});