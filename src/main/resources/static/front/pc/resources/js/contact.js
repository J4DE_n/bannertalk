$(function(){
    $('input, textarea').placeholder({customClass:'my-placeholder'});

    todosMap('37.515392', '127.020730', 'gmap');
});

function fnCategoryChk(){
    var category = "";
    $('input:checkbox[name="categoryChk"]').each(function(){
        if(this.checked){
            category = category+this.value+",";
        }
    });
    category = category.substring(0,(category.length-1));
    document.contactForm.category.value = category;
}


//구글 지도 스크립트
function todosMap(gpsX, gpsY, id){
    var myLatlng = new google.maps.LatLng(gpsX, gpsY);
    var myOptions = {
        zoom: 17,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    var map = new google.maps.Map(document.getElementById(id), myOptions);
    var marker;
    var size_x = 40;
    var size_y = 40;
    //var image = new google.maps.MarkerImage( '/front/images/common/todos_pointer.png');
    new google.maps.Size(size_x, size_y),
        '',
        '',
        new google.maps.Size(size_x, size_y);
    var latlng = new google.maps.LatLng(gpsX,gpsY);
    marker = new google.maps.Marker({
        position: latlng,
        map: map,
        //icon: image,
        animation: google.maps.Animation.BOUNCE,
        //animation: google.maps.Animation.DROP,
        title: '여기는 투도스 입니다.'
    });

    var content = "투도스 ";

    //google.maps.event.addListener(marker, 'click', function() {
    var infowindow = new google.maps.InfoWindow(
        { content:  false,
            size: new google.maps.Size(100,200)
        })
    //infowindow.open(map, marker); 말풍선 영
    //});

}