var userId = tms.getParameterByName("userId");
$(function() {

    /* 이메일 링크 받아 넘어왔을시 초기 내용 세팅 */
    $("#joinStep3 .sub_join .inner_box .txt_mail span").text(userId);

});

$(document).on("click", "#goCertificate", function(){
    $(".txt_alert").hide();
    var code = $("#code1").val()+$("#code2").val()+$("#code3").val()+$("#code4").val();
    var params = {
        userId : userId
        , verifyCode : code
    };
    var option = {showLoding: true};
    tms.ajaxPutHelper("/membership/code", JSON.stringify(params), option, function(result){

        if(result.code == 0){ // 성공시

            $("#popup_all #popup_01").show();
            $("#popup_all #popup_01").css("opacity", "unset");
            $("#popup_all #popup_01 .in_layer_box .mb45").text("회원가입이 완료되었습니다.");

            $("#popup_all #popup_01 button").removeAttr("onclick");
            $("#popup_all #popup_01 button").attr("onclick", "goStep4()");

        } else if(result.code == 500) { // 유효시간 끝남

            $("#popup_all #popup_01").show();
            $("#popup_all #popup_01").css("opacity", "unset");
            $("#popup_all #popup_01 .in_layer_box .mb45").text("인증코드 유효시간이 지났습니다.");

        } else if(result.code == 501) { // 코드가 안 같음
            $(".txt_alert").show();
        } else { // Internal Server Error
            alert("잠시후 시도 하여 주시길 바랍니다.");
        }

    })

});

/* 이메일 재발송 버튼 눌렀을 때 */
$(document).on("click", "#emailResend", function(){

    /* 재발송시 새로 코드 생성 하고 발송 하고 유효시간도 초기화 한다. */
    var params = { userId : userId};
    var option = { showLoding: true };
    tms.ajaxPutHelper("/membership/code/resend", JSON.stringify(params), option, function(result){
        if(result.code == 0){
            $("#popup_all #popup_01").show();
            $("#popup_all #popup_01").css("opacity", "unset");
            $("#popup_all #popup_01 .in_layer_box .mb45").text(userId+" 이메일로 인증 코드를 발송했습니다.");
        }else{
            alert("잠시후 시도하여 주세요");
        }
    })
});

function goStep4(){
    $("#popup_all #popup_01").hide();
    $("#joinStep3").hide();
    $("#joinStep4").show();

}