/* json */
var jsonData = {};

/* step2 validation */
var message = {
    required: {
        userId: "이메일을 입력해주세요",
        userPwd: "비밀번호를 입력해 주세요",
        userPwdChk: "비밀번호 확인을 입력해주세요.",
        tel: "핸드폰 번호를 입력해주세요",
        userName:"이름을 입력해주세요"
    },
    validation: {
        email: {
            1: '잘못된 이메일 형식입니다.'
            , 2: '잘못된 이메일 형식입니다.'
        },
        password: {
            1: '비밀번호는 특수문자와 영어, 숫자를 포함한 최소 8자 이상입니다.'
            , 2:'비밀번호는 특수문자와 영어, 숫자를 포함한 최소 8자 이상입니다.'
            , 3:'비밀번호는 특수문자와 영어, 숫자를 포함한 최소 8자 이상입니다.'
        },
        tel: {
            1: '문자/특수문자 제외 숫자 11자리 번호를 입력해주세요.',
            2: '문자/특수문자 제외 숫자 11자리 번호를 입력해주세요.'
        }
    }
};

$(function() {

    /* 회원가입 step1 */
    $("#agreeYn1").on("click", function(){

        if($('input:checkbox[id="chk_01"]').is(":checked")){
            jsonData.termsAgreeYn = 'Y';
        }
        if($('input:checkbox[id="chk_02"]').is(":checked")){
            jsonData.personalAgreeYn = 'Y'
        }
        if($('input:checkbox[id="chk_03"]').is(":checked")){
            jsonData.etcAgreeYn = 'Y'
        }


        /* step1 닫고 step2 열기 */
        $("#joinStep1").hide();
        $("#joinStep2").show();
    });

});

/* 회원가입 step2 - 가입하기 2단계 다음 버튼 클릭 시 */
function agreeYn2(){

    /* 벨리데이션 체크 해야 함 - 체크 값을 true or false 값으로 리턴 */
    var parentNode = "#joinStep2";

    var valid = tmsValidation.validInputChk(parentNode, message, function(res){

        for(var key in message.required) {
            if(res == message.required[key]){
                if(key == 'userName'){
                    $(".userName_error").show().text(res);
                }else if(key == 'tel'){
                    $(".phone_error").show().text(res);
                }else if(key == 'userPwd'){
                    $(".pwd_error").show().text(res);
                }else if(key == 'userPwdChk'){
                    $(".pwd_check_error").show().text(res);
                }else if(key == 'userId'){
                    $(".email_error").show().text(res);
                }
            }
        }

        for(var key in message.validation) {
            if(res == message.validation[key][1]){
                if(key == 'email'){
                    $(".email_error").show().text(res);
                }else if(key == 'password'){
                    $(".pwd_error").show().text(res);
                }else if(key == 'tel'){
                    $(".phone_error").show().text(res);
                }
            }
        }

    });

    if(!valid){
        return false;
    }

    duple(function(result){
        if(result.code == 200){
            /* 아이디가 존재 할 때 */
            $(".email_error").show().text("이미 사용된 Email 입니다.");
        } else{

            /* validation check 통과 */

            /* jsonData setting */
            jsonData.userId = $("#it_id").val();
            jsonData.userPwd = $("#it_pw_01").val();
            jsonData.tel = $("#it_phone").val();
            jsonData.userNm = $("#it_name").val();


            /* 페이지 값 세팅 */
            $("#joinStep3 .sub_join .txt_mail span").text(jsonData.userId);

            /* 메일 발송 api 호출 */
            var option = {showLoding: true};
            tms.ajaxPostHelper("/membership/signUp", JSON.stringify(jsonData), option, function(result){
                if(result.code == 0){ // 메일 발송 성공

                    /* step2 --> step3 */
                    $("#joinStep2").hide();
                    $("#joinStep3").show();
                    $("#joinStep3 .sub_join .inner_box .txt_mail span").text(jsonData.userId);
                }
            });

        }
    });

}

function duple(callback){
    params = {
        userId:$("input[name='userId']").val()
    };
    tms.ajaxGetHelper("/membership/duple", params, null, function(result){
        if(typeof callback == "function"){
            callback(result);
        }
    });
}

$(document).on("click", "#goCertificate", function(){
    $(".txt_alert").hide();
    var code = $("#code1").val()+$("#code2").val()+$("#code3").val()+$("#code4").val();
    var params = {
        userId : jsonData.userId
        , verifyCode : code
    };
    var option = {showLoding: true};
    tms.ajaxPutHelper("/membership/code", JSON.stringify(params), option, function(result){

        if(result.code == 0){ // 성공시
            $("#popup_all #popup_01").show();
            $("#popup_all #popup_01").css("opacity", "unset");
            $("#popup_all #popup_01 .in_layer_box .mb45").text("회원가입이 완료되었습니다.");

            $("#popup_all #popup_01 button").removeAttr("onclick");
            $("#popup_all #popup_01 button").attr("onclick", "goStep4()");

        } else if(result.code == 500) { // 유효시간 끝남
            $("#popup_all #popup_01").show();
            $("#popup_all #popup_01").css("opacity", "unset");
            $("#popup_all #popup_01 .in_layer_box .mb45").text("인증코드 유효시간이 지났습니다.");
        } else if(result.code == 501) { // 코드가 안 같음
            $(".txt_alert").show();
        } else { // Internal Server Error
            alert("잠시후 시도 하여 주시길 바랍니다.");
        }

    })

});

/* 이메일 재발송 버튼 눌렀을 때 */
$(document).on("click", "#emailResend", function(){

    /* 재발송시 새로 코드 생성 하고 발송 하고 유효시간도 초기화 한다. */
    var params = { userId : jsonData.userId};

    var option = { showLoding: true };
    tms.ajaxPutHelper("/membership/code/resend", JSON.stringify(params), option, function(result){
        if(result.code == 0){
            $("#popup_all #popup_01").show();
            $("#popup_all #popup_01").css("opacity", "unset");
            $("#popup_all #popup_01 .in_layer_box .mb45").text(jsonData.userId+" 이메일로 인증 코드를 발송했습니다.");
        }else{
            alert("잠시후 시도하여 주세요");
        }
    })
});

function goStep4(){
    $("#popup_all #popup_01").hide();
    $("#joinStep3").hide();
    $("#joinStep4").show();

}