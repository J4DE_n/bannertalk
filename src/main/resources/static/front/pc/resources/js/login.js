/* json */
var jsonData = {};

$(function() {


    $("#it_id, #it_pw").on("keyup", function(e){
        if(e.keyCode == 13){
            loginAction();
        }
    });

    /* 로그인 버튼 클릭 */
    $("#loginBtn").on("click", function(){
        loginAction();
    });
});

var loginAction = function(){
    /* 벨리데이션 필수체크 */
    var message = {
        required: {
            userId: "이메일을 입력해주세요.",
            userPwd: "비밀번호를 입력해주세요."
        },
        validation: {
            password: {
                1: '비밀번호는 최소 8자 이상입니다.'
                , 2:'비밀번호는 특수문자와 영어, 숫자를 포함한 최소 8자 이상입니다.'
                , 3:'비밀번호는 특수문자와 영어, 숫자를 포함한 최소 8자 이상입니다.'
            },
            email: {
                1: '잘못된 이메일 형식입니다.'
                , 2: '잘못된 이메일 형식입니다.'
            }
        }
    };
    email = $("#userId").val();
    var $errorBox =$(".error_box");
    var valid = tmsValidation.validInputChk("#frm", message, function(errorMsg){
        if(tms.isNotEmpty(errorMsg)){
            $errorBox.show().text(errorMsg);
        } else {
            $errorBox.hide().text("");
        }
    });

    if(!valid){
        return false;
    }

    var params = {
        userId: $("#it_id").val()
        , userPwd: $("#it_pw").val()
    };

    tms.ajaxPostHelper("/login", JSON.stringify(params), {showLoding: true}, function(result){
        if(result.code == 0) {
            location.href='/';
        } else if(result.code == 226){
            var userId = result.data.userId;
            /* 이메일 미인증 처리 */
            $(".login-box").replaceWith($("#noEmailBox").show());
            $(".bg1").removeClass("bg1");

        } else {
            $errorBox.show().text("이메일 또는 비밀번호가 잘못되었습니다.");
        }

    }, function(e){
        $errorBox.show().text("이메일 또는 비밀번호가 잘못되었습니다.");
    });
};