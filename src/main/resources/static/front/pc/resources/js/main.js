$(function(){
    if(isMobile()){
        $(".project").click(function(){
            $(".project overlay").addClass("mobileProject");
        });
    }
    list($("#portfolios"));
});


var list = function(obj){
    tms.ajaxGetHelper('/portfolio', {cnt : 10}, null,function(res) {
        var list = res.data.portfolioList;
        let  cnt = -1;
        for(let i=0; i < list.length; i++){
            if(i % 5 == 0){
                obj.append('<div class="row"></div>');   // 0~4, 5~9, 10~14, 15~19 ....
                cnt ++;
            }

            switch (i%10) {
                case 1:
                case 5:
                    obj.children().eq(cnt).append('<div class="col-sm-6"><div class="row"></div></div>');
                    break;
                case 3:
                case 7:
                    obj.children().eq(cnt).children().eq(0).append('<div class="row"></div>');
                    break;
            }
        }

        cnt = -1;
        for(let i = 0; i < list.length; i++){
            switch(i % 10){
                // 0 : [0, 0, 0]
                case 0:
                    cnt++;
                    obj.children().eq(cnt).prepend($(ptfShow(list[i], "big-content")));
                    break;

                // 1, 2 : [0, 1, 0]
                case 1:
                case 2:
                    obj.children().eq(cnt).find(".row:eq(0)").append($(ptfShow(list[i], "small-content")));
                    break;

                // 3, 4 : [0, 1, 1]
                case 3:
                case 4:
                    obj.children().eq(cnt).find(".row:eq(1)").append($(ptfShow(list[i], "small-content")));
                    break;

                // 5, 6 : [1, 0, 0]
                case 5:
                    cnt++;
                case 6:
                    obj.children().eq(cnt).find(".row:eq(0)").append($(ptfShow(list[i], "small-content")));
                    break;

                // 7, 8 : [1, 0, 1]
                case 7:
                case 8:
                    obj.children().eq(cnt).find(".row:eq(1)").append($(ptfShow(list[i], "small-content")));
                    break;

                // 9 : [1, 1, 0]
                case 9:
                    obj.children().eq(cnt).append($(ptfShow(list[i], "big-content")));
                    break;
            }
        }
    });


}

var ptfShow= function(obj, content){
    return '<div class="col-sm-6">'+'<div class="project-content">'+'<div class=\"project '+content+'\">'
        + '<img class="img-responsive" src="'+obj.fnFilePath+'" alt="'+obj.title+'" />'
        + '<a class="pop_link" href="'+obj.detailFilePath+'">'
        + '<div class="overlay">'+'<i class="fa fa-arrow-circle-o-right"></i>'
        + '<h3>'+ obj.title +'</h3>'+'<h4>'+ obj.summary+'</h4>'
        + '</div>'+'</a>'+'</div>'+'</div>'+'</div>';
}


// esc클릭시 화면 닫기
$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        fnClosePop();
    }
});

$(document).on('click', '.pop_link', function(e){
    e.preventDefault();

    $(".lpContent img").attr("src", $(this).attr("href"));
    loPopOpen();
});

//팝업 오픈
function loPopOpen(){
    var $href = $(this).attr('href');
    var marL = ($('#lpPopView').outerWidth()/2);
    var marT = ($('#lpPopView').outerHeight()/2);
    $('body').append('<a href="#none" onclick="fnClosePop()"><div class="totalDimmBg"></div></a>');
    $('#lpPopView').fadeIn('fast');
    //$($href).fadeIn();
    //$('html, body').animate({scrollTop:400}, 'slow');
    $('#lpPopView').css({'margin-left':-marL})
    $('.totalDimmBg').height($('body').height());

    $("body").css({"overflow-y":"hidden"});

    return false;
}

//팝업 닫기
function fnClosePop(){
    //$($href).fadeIn();
    $("body").css({"overflow-y":"auto"});
    $('#lpPopView').hide();
    $('.totalDimmBg').remove();
    $('#lpPopView').css({'margin-left':'', 'margin-top':''});
    $(".lpContent img").attr("src","");
    $('#lpPopView').animate({scrollTop:0}, 500);
}