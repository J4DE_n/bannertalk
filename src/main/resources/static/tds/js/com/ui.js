UI = {
	load: function(){
		$(document).ready(function(){
			UI.fn_gnb();
			UI.fn_user();
			UI.fn_main();
			UI.fn_parallax();
			UI.fn_join();
			UI.fn_pageTab();
			UI.fn_faq();
			UI.fn_order();

		});//ready


		$(window).load(function(){

		});//load
	},

	fn_gnb : function(){
		var header = $('#header');

		$(window).scroll(function(){
			sTop = $(window).scrollTop();
			if (sTop > 0){
				header.addClass('scroll');
			}else{
				header.removeClass('scroll');
			}
		});
	},

	fn_main : function(){
		if($('#main_contents').length == 0){return;}
		$('#wrap').addClass('main');
	},

	fn_user : function(){
		var btn_user = $('#header .btn_user');
		var user_layer = $('#header .layer_user');
		var user_flag = 1;

		btn_user.on('click',function(){
			if (user_flag){
				user_layer.fadeIn();
				user_flag = 0;
			}else{
				user_layer.fadeOut();
				user_flag = 1;
			}
		});

	},

	fn_parallax : function(){
		$('#main_contents .section_02').parallax({
			imageSrc:'/front/pc/images/main/sec_02_bg.png',
			speed:'0.8',
		});
		$('#main_contents .section_03').parallax({
			imageSrc:'/front/pc/images/main/sec_03_bg.png',
			speed:'0.5',
		});
		$('#main_contents .section_09 .inner_box').parallax({
			imageSrc:'/front/pc/images/main/sec_09_bg.png',
			speed:'0.8',
		});
		$('#main_contents .section_10 .img').parallax({
			imageSrc:'/front/pc/images/main/sec_10_img_01.png',
			speed:'0.8',
			zIndex:'1',
		});
		$('#main_contents .section_11 .cont_01 .img').parallax({
			imageSrc:'/front/pc/images/main/sec_11_img_01.png',
			speed:'0.8',
			zIndex:'1',
		});
		$('#main_contents .section_11 .cont_02 .img').parallax({
			imageSrc:'/front/pc/images/main/sec_11_img_02.png',
			speed:'0.8',
			zIndex:'1',
		});
		$('#main_contents .section_11 .cont_03 .img').parallax({
			imageSrc:'/front/pc/images/main/sec_11_img_03.png',
			speed:'0.8',
			zIndex:'1',
		});
		$('#main_contents .section_11 .cont_04 .img').parallax({
			imageSrc:'/front/pc/images/main/sec_11_img_04.png',
			speed:'0.8',
			zIndex:'1',
		});
	},

	fn_join : function(){
		if($('.sub_join').length == 0){return;}

		agree_chk = function(){
			if ($('#chk_01').prop('checked')&&$('#chk_02').prop('checked')){
				$(".btn_agree").addClass('on');
			}else{
				$(".btn_agree").removeClass('on');
			}
		};

		$('#chk_all').on('click',function(){
			if ($('#chk_all').prop('checked')){
				$("input[name=chk]").prop("checked",true);
			}else{
				$("input[name=chk]").prop("checked",false);
			}
			agree_chk();
		});

		$('#chk_01,#chk_02').on('click',function(){
			agree_chk();
		});

		var btn_tab = $('.privacy_box .btn_tab').children();
		var btn_cont = $('.privacy_box .tab_cont').children();
		
		btn_tab.each(function(i){this.num = i});

		btn_tab.on('click',function(){
			btn_tab.removeClass('on');
			btn_tab.eq(this.num).addClass('on');
			btn_cont.removeClass('on');
			btn_cont.eq(this.num).addClass('on');
		});
	},

	fn_pageTab : function(){
		if($('.page_tab').length == 0){return;}
		$(window).scroll(function(){
			sTop = $(window).scrollTop();
			if (sTop > 200){
				$('#wrap').addClass('tab_fixed');
			}else{
				$('#wrap').removeClass('tab_fixed');
			}
		});
	},
	fn_faq : function(){
		if($('.sub_faq').length == 0){return;}

		var faq_a = $('.faq_list .q button');

		faq_a.on('click',function(){
			$(this).parent().parent().siblings().removeClass('open');
			$(this).parent().parent().toggleClass('open');
		});
	},

	fn_order : function(){
		if($('.sub_order').length == 0){return;}

		var info_wrap_top = $('.info_wrap').offset().top;
		var payment_box = $('.payment_box');
		var agree_box_top = $('.agree_box').offset().top;

		$(window).scroll(function(){
			sTop = $(window).scrollTop();
			if (sTop >= (info_wrap_top - 90) && sTop <= (agree_box_top)){
				payment_box.stop().animate({'top': (sTop - info_wrap_top + 170) },0);
			}
		});

		agree_chk = function(){
			if ($('#chk_04').prop('checked')&&$('#chk_05').prop('checked')&&$('#chk_06').prop('checked')){
				$(".btn_agree").addClass('on');
			}else{
				$(".btn_agree").removeClass('on');
			}
		};

		$('#chk_all').on('click',function(){
			if ($('#chk_all').prop('checked')){
				$("input[name=chk]").prop("checked",true);
			}else{
				$("input[name=chk]").prop("checked",false);
			}
			agree_chk();
		});

		$('#chk_04,#chk_05,#chk_06').on('click',function(){
			agree_chk();
		});

	}

}

//레이어 오픈 
var layer_OPEN = function (obj_selector){
	var obj = $(obj_selector);
	obj.css({'display':'block','opacity':0});
	obj.stop().animate({'opacity':1},500);
};

//레이어 클로즈
var layer_CLOSE = function (obj_selector){
	var obj = $(obj_selector);	
	obj.stop().animate({'opacity':0},500,function (){
		$(this).css({'display':'none'});	
	});
};


UI.load();
