/*
package com.tms.tms.membership.controller;

import com.tms.tms.board.config.ConfigBoard;
import com.tms.tms.board.service.BoardService;
import com.tms.tms.common.type.ReturnType;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

public class BoardServiceTest {

    @Autowired
    private ConfigBoard configBoard ;

    @Autowired
    BoardService boardService;

    @Test
    public void checkWrite() {
        Map writeData = new HashMap();
        ReturnType returnType;

        writeData.put(configBoard.getMapTitleVar(),"title");
        writeData.put(configBoard.getMapContentVar(),"123456498431231456484213211346546545646461651616161615611");
        writeData.put(configBoard.getMapUserIdVar(),"userId");
        writeData.put(configBoard.getMapUserNameVar(),"userName");
        writeData.put(configBoard.getMapUserEmailVar(),"userEmail");

        try{
            returnType = boardService.write(writeData);
            assert(returnType == ReturnType.RTN_TYPE_OK);
        }
        catch (Exception e)
        {
            assert(false);
        }
    }
}
*/