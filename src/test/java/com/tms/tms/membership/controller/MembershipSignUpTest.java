/*
package com.hobien.front.membership.controller;

//import com.tms.tms.common.type.ReturnType;
//import com.tms.tms.membership.config.ConfigMembership;
import com.tms.tms.membership.service.MembershipService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
//import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.ResponseEntity.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
*/
/**
 *  Spring runner
 *  ApplicationContext 를 로딩 할 수 있다.
 *  @Autowired를 beans을 가질 수 있다.
 *  rule을 통해 mockito의 기능을 사용할 수 있다.
 *
 */
/*
@RunWith(SpringRunner.class)
public class MembershipSignUpTest {

    // rule을 통해 mockito 기능을 가져다 사용 할 수 있다.
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Autowired
    private ConfigMembership configMembership ;

    @Autowired
    MembershipService membershipService;

    @Test
    public void testSignUp() {

        Map MembershipData = new HashMap();
        ReturnType returnType;

        MembershipData.put(configMembership.getMapUserIdVar(),"agundle");
        MembershipData.put(configMembership.getMapUserPwdVar(),"DBdnstjs@3");
        MembershipData.put(configMembership.getMapUserNameVar(),"dbdb");
        MembershipData.put(configMembership.getMapEmailVar(),"raymond@todos.co.kr");

        try{
            returnType = membershipService.signUp(MembershipData);
            assert(returnType == ReturnType.RTN_TYPE_OK);
        }
        catch (Exception e)
        {
            assert(false);
        }
    }
}
*/

/**
 * Mockito runner
 * mocks & spies를 사용할 수 있다.
 *
  */
//@RunWith(MockitoJUnitRunner.class)
//@SpringBootTest
//

/*
@WebMvcTest(MembershipController.class)
public class MembershipSignUpTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ConfigMembership configMembership ;

    @MockBean
    MembershipService membershipService;

    @Test
    public void testSignUp() throws Exception {
        Map tempMap = new HashMap();

        given(membershipService.signUp(tempMap)).willReturn(ReturnType.RTN_TYPE_OK);
        mvc.perform(post("/membership/signUp").contentType(MediaType.APPLICATION_JSON))
        //    .andExpect(jsonPath)
    }
}
*/



