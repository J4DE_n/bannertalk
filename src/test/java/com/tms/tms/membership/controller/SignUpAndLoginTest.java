//package com.tms.tms.membership.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
/*
@RunWith(SpringRunner.class)
@SpringBootTest
public class SignUpAndLoginTest {
    private WebDriver driver;

    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver", "src/test/driver/chromedriver.exe");
        driver = new ChromeDriver();
    }
    @After
    public void tearDown() {
        //driver.quit();
    }

    @Test
    public void test() throws InterruptedException {

        int i = 0;
        long pageLoadTimeout = 2000;

        for(i = 0; i<10; i++) {

            driver.get("http://localhost:8080/membership/signUp");

            // Page 로드 timeout
            synchronized (driver) {
                driver.wait(pageLoadTimeout);
            }

            driver.findElement(By.id("userName")).sendKeys("user"+i);
            driver.findElement(By.id("userPwd")).sendKeys("DBdbstjs@3");
            driver.findElement(By.id("userRealName")).sendKeys("name"+i);
            //driver.findElement(By.id("email")).sendKeys("raymond@todos.co.kr");

            driver.findElement(By.id("submit")).click();

            // Page 로드 timeout
            synchronized (driver) {
                driver.wait(pageLoadTimeout);
            }

            // 경고창의 수락을 누른다.
            Alert alert = driver.switchTo().alert();
            alert.accept();

            driver.get("http://localhost:8080/membership/login");

            // Page 로드 timeout
            synchronized (driver) {
                driver.wait(pageLoadTimeout);
            }

            driver.findElement(By.name("userId")).sendKeys("user"+i);
            driver.findElement(By.name("userPwd")).sendKeys("DBdbstjs@3");
            driver.findElement(By.name("submit")).click();

            // Page 로드 timeout
            synchronized (driver) {
                driver.wait(pageLoadTimeout);
            }
        }
    }

}
*/